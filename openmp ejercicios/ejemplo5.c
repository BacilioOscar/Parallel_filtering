/*
Parallel and Distributed Computing Class
OpenMP

Practice 5 : Vector * Matrix
Name       : 
*/

#include <stdio.h>
#include <omp.h>
#define SIZE 100


int main ()
{
  // Counters
  int i, j;  
  // Working matrix and vectors
  float matrix[SIZE*SIZE], vector[SIZE], result[SIZE];

  // Initializing Matrix and Vector
  printf("\nInitializing Matrix [%d][%d] and Vector[%d] ...\n", SIZE,SIZE,SIZE);
  for (i=0; i<SIZE; i++){
    vector[i] = i + 1.0;
  }

  for (i=0; i < SIZE; i++) {
    for (j=0; j < SIZE; j++) {
      matrix[i*SIZE+j] = i * 2.0;
    }
  }

  printf("\nStarting Multiplication Vector * Matrix ...\n");
  double start = omp_get_wtime();

  #pragma omp parallel for
  	for (i=0; i < SIZE; i++){
  		double sum=0;
  		for (j=0;j< SIZE; j++){
  			sum+=matrix[i*SIZE+j] * vector[j];
  			}
  			result[i]=sum;
  			}
  double end = omp_get_wtime();

	for (i=0; i < SIZE; i++)
		printf("\t result=%f",result[i]);
  printf("\nMultiplication Vector * Matrix has FINISHED\n");
  printf("\nExecution Time = %f\n",end - start);
  return 0; 
}

