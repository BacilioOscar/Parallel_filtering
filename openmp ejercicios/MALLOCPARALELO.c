#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#define tam 100

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char *argv[]) {

	int **a; //declaramos arreglos bidimensionales
	int **b;
	int **c; //matriz del resultado
	
	a=(int *)malloc(tam*sizeof(int *)); //numero de renglones tama�o de columas por enteros
	b=(int *)malloc(tam*sizeof(int *));
	c=(int *)malloc(tam*sizeof(int *));
	
	if(a==NULL || b==NULL || c==NULL){
		printf("Error memoria dinamica esta llena");
	}
	else{
	int i;
	for (i=0;i<tam;i++){
		
		a[i]=(int *)malloc(tam*sizeof(int *));  //numero de columnas
		b[i]=(int *)malloc(tam*sizeof(int *));
		c[i]=(int *)malloc(tam*sizeof(int *));
	}
	
	int num=0;
	int num2=0;
	int contador=1;
	
	for(num=0;num<tam;++num){
		for(num2=0;num2<tam;++num2){
			
			a[num][num2]=contador;
			b[num][num2]=contador;
			
			contador++;			
		}
}

	
	//===================================================================================
	
	printf("\n EL RESULTADO DE LA MULTIPLICACION DE MATRICES A X B, ES LA MATRIZ C \n\n");
	
	
	
	int a1,b1,d1;	
	int aux=0;
	
float start = omp_get_wtime();
#pragma omp parallel num_threads(2)
#pragma omp for schedule(static) private (a1,d1,b1) reduction(+:aux)
	for(a1=0;a1<tam;++a1){
		for(d1=0;d1<tam;d1++){
			aux=0;
			for(b1=0;b1<tam;++b1){
			
				aux+=a[a1][b1]*b[b1][d1];
			}
			c[a1][d1]=aux;
		}
	}
float end = omp_get_wtime();
printf("TIEMPO DE EJECUCION %f",end-start);
}

    free(a); //liberar memoria del malloc
	free(b);
	
	//Opcional imprimir resultado
	/*	for(num=0;num<tam;++num){
		for(num2=0;num2<tam;++num2){
			
		printf("%d ",c[num][num2]);
			
		}
		printf("\n");
	}
	*/
	
	return 0;
}
