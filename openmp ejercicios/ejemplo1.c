/*                                                                                                                   
Parallel and Distributed Computing Class                                                                             
OpenMP                                                                                                               
                                                                                                                     
Practice 1 : First example with OpenMP
Name       :                                                                                             
*/

#include <omp.h>
#include <stdio.h>
int main(){

  int procs = omp_get_num_procs();
  printf("# of cores= %d\n",procs);
  #pragma omp parallel
  {
  	int core=omp_get_thread_num(); 
  	if((core==0)){	  
    printf("\nHola mundo\n");
	}
	printf("%d\n",core);
  }
  printf("\nLa version instalada de OpenMP es: %d\n\n",_OPENMP);
  return 0;
}

