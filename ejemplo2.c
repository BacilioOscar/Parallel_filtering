#include <omp.h>
#include <stdio.h>
int main() {
#pragma omp parallel
#pragma omp sections
{
 #pragma omp section
 printf("Task A: %d\n", omp_get_thread_num());
 #pragma omp section
 printf("Task B: %d\n", omp_get_thread_num());
 #pragma omp section
 printf("Task C: %d\n", omp_get_thread_num());
}
}
