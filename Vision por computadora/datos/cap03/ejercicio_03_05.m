% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 3.5: Transformaciones geom�tricas

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3.7.2 Transformaciones elementales
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
% Transformaciones elementales:
A = [1 2 3; 4 5 6; 7 8 9];

[m,n] = size(A);

% Rotaci�n
% la rotaci�n se realiza con respecto al centro de coordenadas
theta = pi/4; % pi/6 = 30�
h = 0;
for i=-(m-2):1:(m-2)
    for j=-(n-2):1:(n-2)
       h = h + 1;
       x = round(cos(theta)*i - sin(theta)*j);
       y = round(sin(theta)*i + cos(theta)*j);
       rotacion.coordenadas.i(h) = i;
       rotacion.coordenadas.j(h) = j;
       rotacion.coordenadas.x(h) = x;
       rotacion.coordenadas.y(h) = y;
    end
end

disp('Coordenadas obtenidas (i,j):'); 
disp(rotacion.coordenadas.i);
disp(rotacion.coordenadas.j);

disp('Coordenadas obtenidas (x,y): ');
disp(rotacion.coordenadas.x);
disp(rotacion.coordenadas.y);
