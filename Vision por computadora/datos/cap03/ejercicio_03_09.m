% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 3.9: Transformaciones geom�tricas

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3.7.2 Transformaciones elementales
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
% Registro entre imagenes
A = [1 2 3; 4 5 6; 7 8 9];

[m,n] = size(A);


% Transformada af�n: ejercicio 3.8
theta = pi/6;
Sx = 2;
Sy = 1.5;
id = 2.0;
jd = 0.5;
a11 = Sx*cos(theta); 
a12 = -sin(theta);
a10 = id;
a20 = jd;
a21 = sin(theta);
a22 = Sy*cos(theta);
h = 0;
for i=-(m-2):1:(m-2)
    for j=-(n-2):1:(n-2)
       h = h + 1;
       x = round(a11*i + a12*j + a10);
       y = round(a21*i + a22*j + a20);
       afin.coordenadas.i(h) = i;
       afin.coordenadas.j(h) = j;
       afin.coordenadas.x(h) = x;
       afin.coordenadas.y(h) = y;
    end
end

C = [1 -1 -1; 1 0 -1; 1 1 -1; 1 -1 0; 1 0 0; 1 1 0; 1 -1 1; 1 0 1; 1 1 1];

bx = (afin.coordenadas.x)';
by = (afin.coordenadas.y)';

[U,S,V] = svd(C);

%calculo de los coeficientes de la transformada af�n
ax = V*pinv(S)*U'*bx;
ay = V*pinv(S)*U'*by;

disp('Coeficientes ax :'); disp(ax'); 
disp('Coeficientes ay :'); disp(ay'); 
