% EJERCICIOS RESUELTOS DE VISIÓN POR COMPUTADOR
% Autores: Gonzalo Pajares y Jesús Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 3.10: Transformaciones geométricas

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3.7.3 Deformaciones
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;

A = [1 2 3; 4 5 6; 7 8 9];

[m,n] = size(A);

% Deformación
h = 0;
for i=-(m-2):1:(m-2)
    for j=-(n-2):1:(n-2)
       h = h + 1;
       x = round(i);
       y = round(j + 0.5*(sin(60*x/30)+1));
       deformacion.coordenadas.i(h) = i;
       deformacion.coordenadas.j(h) = j;
       deformacion.coordenadas.x(h) = x;
       deformacion.coordenadas.y(h) = y;
    end
end

disp('Coordenadas originales (i,j):'); 
disp(deformacion.coordenadas.i);
disp(deformacion.coordenadas.j);

disp('Coordenadas de la deformación  (x,y): ');
disp(deformacion.coordenadas.x);
disp(deformacion.coordenadas.y);
