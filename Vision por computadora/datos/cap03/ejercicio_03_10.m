% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 3.10: Transformaciones geom�tricas

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3.7.2 Transformaciones elementales
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;

A = [1 2 3; 4 5 6; 7 8 9];

[m,n] = size(A);

% Inclinaci�n
a2 = 1.5; 
h = 0;
for i=-(m-2):1:(m-2)
    for j=-(n-2):1:(n-2)
       h = h + 1;
       x = round(i + a2*j);
       y = round(j);
       inclinacion.coordenadas.i(h) = i;
       inclinacion.coordenadas.j(h) = j;
       inclinacion.coordenadas.x(h) = x;
       inclinacion.coordenadas.y(h) = y;
    end
end

% Perspectiva
a3 = 2.8; 
h = 0;
for i=-(m-2):1:(m-2)
    for j=-(n-2):1:(n-2)
       h = h + 1;
       x = round(a3*i*j);
       y = round(j);
       perspectiva.coordenadas.i(h) = i;
       perspectiva.coordenadas.j(h) = j;
       perspectiva.coordenadas.x(h) = x;
       perspectiva.coordenadas.y(h) = y;
    end
end

disp('Coordenadas obtenidas en inclinaci�n (i,j):'); 
disp(inclinacion.coordenadas.i);
disp(inclinacion.coordenadas.j);

disp('Coordenadas obtenidas en inclinaci�n (x,y): ');
disp(inclinacion.coordenadas.x);
disp(inclinacion.coordenadas.y);

disp('Coordenadas obtenidas en perspectiva (x,y): ');
disp(perspectiva.coordenadas.x);
disp(perspectiva.coordenadas.y);
