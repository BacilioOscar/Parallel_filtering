% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 3.4: Transformaciones geom�tricas

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3.7.2 Transformaciones elementales
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;

A = [1 2 3; 4 5 6; 7 8 9];

[m,n] = size(A);

% Traslaci�n
dx = 2; dy = 1;
h = 0;
for i=0:1:m-1
    for j=0:1:n-1
       h = h + 1;
       x = i + dx;
       y = j + dy;
       traslacion.coordenadas.i(h) = i;
       traslacion.coordenadas.j(h) = j;
       traslacion.coordenadas.x(h) = x;
       traslacion.coordenadas.y(h) = y;
    end
end

disp('Coordenadas obtenidas (i,j):'); 
disp(traslacion.coordenadas.i);
disp(traslacion.coordenadas.j);

disp('Coordenadas obtenidas (x,y): ');
disp(traslacion.coordenadas.x);
disp(traslacion.coordenadas.y);
