% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 10.3: Clasificaci�n: reconocimiento estad�stico

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 10.5.2 Clasificador param�trico Bayesiano 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; close all

%datos 2-dimensionales con 2 clases
% clase c1: 
 datos(1).X = [1,2;3,3;1,5;2,2;3,3];
% clase c2: 
datos(2).X = [6,4;6,3;7,4;8,4;8,5];

c = 2; %numero de clases 

%C�lculo de las medias
for i=1:1:c
    datos(i).m = mean(datos(i).X);
end

%C�lculo de las matrices de covarianza
for i=1:1:c
    datos(i).C = cov(datos(i).X);
end

%Medidas de separabilidad de las clases
% divergencia de las clases, coseno, Jeffries-Matusita
Divergencia = zeros(c,c);
Coseno = zeros (c,c);
Jeffries = zeros(c,c);
for i=1:1:c
    for j=1:1:c
      if i~=j
         aa = datos(i).C-datos(j).C;
         bb = inv(datos(i).C)-inv(datos(j).C);
         cc = inv(datos(i).C)+inv(datos(j).C);
         dd = datos(i).m-datos(j).m;
         Divergencia(i,j) = 0.5*trace(aa*bb)+...
                            0.5*trace(dd*cc*dd'); 
                        
         dd1 = sqrt(sum(datos(i).m.^2));
         dd2 = sqrt(sum(datos(j).m.^2));
         Coseno(i,j) = sum(datos(i).m.*datos(j).m)/(dd1*dd2);
         
         ne = exp(1); 
         dc = det(cc); 
         dci = sqrt(det(datos(i).C));
         dcj = sqrt(det(datos(j).C));
         B = (1/8)*dd*inv(cc/2)*dd'+(1/(2*log10(ne)))*log10(2*dc/(dci*dcj));
         Jeffries(i,j) = 2*(1-exp(-B));         
      end
    end
end

disp('Divergencia: '); disp(Divergencia);
disp('Coseno: '); disp(Coseno);
disp('Distancia de Jeffries: '); disp(Jeffries);

%distancias de Mahalanobis
xk = [4,5];

for i=1:1:c
    datos(i).dM = (xk-datos(i).m)*inv(datos(i).C)*(xk-datos(i).m)';
end

disp('distancia de (4,5) a la clase 1:'); disp(datos(1).dM); 
disp('distancia de (4,5) a la clase 2:'); disp(datos(2).dM); 







