% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 10.1: Clasificaci�n: reconocimiento estad�stico

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 10.5.1 Agrupamiento borroso 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% par�metros del clasificador
clear all; close all;

% numero de cl�steres
c = 2;  
m = 2; %peso exponencial
e = 1e-2; %criterio de terminaci�n

% muestras de entrenamiento
X = [1,1;1,3;1,5; 2,2;2,3; 6,3;6,4; 7,1;7,3;7,5];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%        FASE DE APRENDIZAJE     %%%%%%%%%%%%%%%%%%%%%
%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%normalizacion de datos en el rango [0,1]
MI=min(X); %minimo
MA=max(X); %maximo
X=(X-repmat(min(X),size(X,1),1))./(repmat(max(X),...
         size(X,1),1)-repmat(min(X),size(X,1),1));
     
[N,n] = size(X);
X1 = ones(N,1);

% calculo de los centros de forma aleatoria
Me = mean(X);             %valor medio de los datos (1,n)
Md = max(abs(X - ones(N,1)*Me)); %maxima distancia en el conjunto de datos
rand('state',0)
v = 2*(ones(c,1)*Md).*(rand(c,n)-0.5) + ones(c,1)*Me;

% Inicializar la matriz de partici�n fuzzy 
for j = 1 : c,
  xv = X - X1*v(j,:);
  d(:,j) = sum((xv*eye(n).*xv),2);
end;
d = (d).^(-2/(m-1));
Ui = (d ./ (sum(d,2)*ones(1,c)));

Uf = zeros(N,c);                % matriz particion
iter = 0;                      % iteraciones

% Iterar 
while  max(max(Ui-Uf)) > e
  iter = iter + 1;
  Uf = Ui;
  % Calcular centros
  Um = Uf.^m;
  sumU = sum(Um);
  v = (Um'*X)./(sumU'*ones(1,n));
  for j = 1 : c,
    xv = X - X1*v(j,:);
    d(:,j) = sum((xv*eye(n).*xv),2);
  end;
  distSalida=sqrt(d);
  J(iter) = sum(sum(Ui.*d));

  % Actulizar Ui
  d = d.^(-2/(m-1));
  Ui = (d ./ (sum(d,2)*ones(1,c)));
  c1 = 'U iteracion #'; c2 = num2str(iter); c3 = 'Centros v';
  disp([c1,c2]); disp(Ui');
  disp([c3,c2]); disp(v);
  disp('Delta'); disp(max(max(Ui-Uf)));
end
Ui;
v;
iter;
Um = Uf.^m; 
sumf = sum(Um);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%        FIN FASE DE APRENDIZAJE   %%%%%%%%%%%%%%%%%%%
%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%        FASE DE VALIDACION   %%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%% Validaci�n de la clasificaci�n %%%%%%
N = size(Ui,1); %numero de muestras
n = size(v,2);

%coeficiente de partici�n (CP)
f = Ui.^m;
CP = 1/N*sum(sum(f));
%coeficiente de entrop�a (CE)
f = Ui.*log(Ui);
CE = -1/N*sum(sum(f));
     
%resultados   
Cvalidez.CP = CP;
Cvalidez.CE = CE;   
        
%indice particion(SC)
ni = sum(Ui);                        % cardinalidad fuzzy
si = sum(distSalida.*Ui.^(m/2));     % variaci�n fuzzy 
pii=si./ni;
mask = zeros(c,n,c);                 % separaci�n de los clusteres 
for i = 1:c
     for j =1:c
         mask(j,:,i) = v(i,:);
     end
     dist(i) = sum(sum((mask(:,:,i) - v).^2));
end
s = dist;

SC = sum(pii./s);

% �ndice de separaci�n (S)
S = sum(pii)./(N*min(dist));

% �ndice de Xie y Beni (XB)
XB = sum((sum(distSalida.*Ui.^2))./(N*min(distSalida)));
    
% coeficientes    
Cvalidez.SC = SC;
Cvalidez.S = S;
Cvalidez.XB = XB;    
        
% �ndice de Dunn (DI)
[mm,etiqueta] = min(distSalida');

for i = 1:c
    index=find(etiqueta == i);
    dat{i}=X(index,:);
    meret(i)= size(dat{i},1);
end
mindistmatriz =ones(c,c)*inf;
mindistmatriz2 =ones(c,c)*inf;
        
for cntrActualClust = 1:c
    for cntrOtrosClust = (cntrActualClust+1):c
         for cntrActualPoints = 1:meret(cntrActualClust)
             dd = min(sqrt(sum([(repmat(dat{cntrActualClust}(cntrActualPoints,:),...
                 meret(cntrOtrosClust),1)-dat{cntrOtrosClust}).^2]')));
             % calcular distancias para el �ndice alternativo de Dunn 
             dd2 = min(abs(distSalida(cntrActualClust,:)-distSalida(cntrOtrosClust,:)));
                     
             if mindistmatriz(cntrActualClust,cntrOtrosClust) > dd
                mindistmatriz(cntrActualClust,cntrOtrosClust) = dd;
             end
             if mindistmatriz2(cntrActualClust,cntrOtrosClust) > dd2
                mindistmatriz2(cntrActualClust,cntrOtrosClust) = dd2;
             end
          end
       end
      end

   minimaDist = min(min(mindistmatriz));
   minimaDist2 = min(min(mindistmatriz2));
        
   maxDispersion = 0;
   for cntrActualClust = 1:c
      DispersionActual = 0;
      for cntrActualPoints1 = 1:meret(cntrActualClust)
         dd = max(sqrt(sum([(repmat(dat{cntrActualClust}(cntrActualPoints1,:),meret(cntrActualClust),1) ...
                      -dat{cntrActualClust}).^2]')));
         if DispersionActual < dd
            DispersionActual = dd;
         end
         if maxDispersion < DispersionActual
            maxDispersion = DispersionActual;
         end
      end 
   end

   DI = minimaDist/maxDispersion;
      
   % �ndice alternativo de Dunn
   ADI = minimaDist2/maxDispersion;
   %resultados
 
   Cvalidez.DI = DI;
   Cvalidez.ADI = ADI;   

   disp('Validaci�n');
   disp('CP:'); disp(Cvalidez.CP);
   disp('CE:'); disp(Cvalidez.CE);
   disp('SC:'); disp(Cvalidez.SC);
   disp('S:'); disp(Cvalidez.S);
   disp('XB:'); disp(Cvalidez.XB);
   disp('DI:'); disp(Cvalidez.DI);
   disp('ADI:'); disp(Cvalidez.ADI);
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%        FIN FASE DE VALIDACION   %%%%%%%%%%%%%%%%%%%%
%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%        FASE DE CLASIFICACION   %%%%%%%%%%%%%%%%%%%%%
%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Ejercicio 10.1: Calculo de los valores de pertenencia a cada uno de los cl�steres
AA = [3,4];

%normalizaci�n de A
AA=(AA-repmat(MI,size(AA,1),1))./(repmat(MA,...
         size(AA,1),1)-repmat(MI,size(AA,1),1));
     
% calculo de los grados de pertenencia a cada una de las clases
for i=1:1:c
  dd(i) = (1/sum((AA - v(i,:)).^2))^(2/(m-1));
end

SUMA = sum(dd);
for i=1:1:c
  mu(i) = dd(i)/SUMA;
end

%desnormalizaci�n
X = (repmat(MA,size(X,1),1) - repmat(MI,size(X,1),1)).*X + ...
        repmat(MI,size(X,1),1);

v = (repmat(MA,size(v,1),1) - repmat(MI,size(v,1),1)).*v + ...
        repmat(MI,size(v,1),1);
    
disp('Centros finales: '); disp(v);    

disp('Calculo de los grados de pertenencia:'); disp(mu);

% representaci�n de datos
[a,p]=size(X);   
if p==2
    plot(X(:,1),X(:,2),'b.',v(:,1),v(:,2),'ro');
elseif p==3
    plot3(X(:,1),X(:,2),X(:,3),'k.',v(:,1),v(:,2),v(:,3),'kd');
    hold on
    plot3(v(1,1),v(1,2),v(1,3),'gd'); 
    plot3(v(2,1),v(2,2),v(2,3),'rd'); 
    plot3(v(3,1),v(3,2),v(3,3),'bd');
    plot3(AA(1),AA(2),AA(3),'ko');
    grid on
    q = 2; % proyecci�n a la dimensi�n q (en este caso bidimensional)
    [y,vp,V,D] = Componentes_principales(X,v,q,c);
    grid on
    figure; plot(y(:,1),y(:,2),'k.')
    hold on; plot(vp(1,1),vp(1,2),'gd')
    hold on; plot(vp(2,1),vp(2,2),'rd')
    hold on; plot(vp(3,1),vp(3,2),'bd')
    
else
  %no representar nada
end







