% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 10.4: Clasificaci�n: reconocimiento estad�stico

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 10.5.2 Clasificador param�trico Bayesiano 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; close all

% datos 3-dimensionales con 3 clases
% clase c1: 
datos(1).X = [200,160,120;210,170,130;215,172,133;210,165,134;198,177,138];

% clase c2: 
datos(2).X = [90,130,60;92,138,54;87,128,66;91,134,60;85,123,55];

% clase c3: 
datos(3).X = [30,44,178;20,40,180;24,42,184;28,50,176;22,46,181];

c = 3; %numero de clases 


%C�lculo de las medias
for i=1:1:c
    datos(i).m = mean(datos(i).X);
end

%C�lculo de las matrices de covarianza
for i=1:1:c
    datos(i).C = cov(datos(i).X);
end

%Medidas de separabilidad de las clases
% divergencia de las clases, coseno, Jeffries-Matusita
Divergencia = zeros(c,c);
Coseno = zeros (c,c);
Jeffries = zeros(c,c);
for i=1:1:c
    for j=1:1:c
      if i~=j
         aa = datos(i).C-datos(j).C;
         bb = inv(datos(i).C)-inv(datos(j).C);
         cc = inv(datos(i).C)+inv(datos(j).C);
         dd = datos(i).m-datos(j).m;
         Divergencia(i,j) = 0.5*trace(aa*bb)+...
                            0.5*trace(dd*cc*dd'); 
                        
         dd1 = sqrt(sum(datos(i).m.^2));
         dd2 = sqrt(sum(datos(j).m.^2));
         Coseno(i,j) = sum(datos(i).m.*datos(j).m)/(dd1*dd2);
         
         ne = exp(1); 
         dc = det(cc); 
         dci = sqrt(det(datos(i).C));
         dcj = sqrt(det(datos(j).C));
         B = (1/8)*dd*inv(cc/2)*dd'+(1/(2*log10(ne)))*log10(2*dc/(dci*dcj));
         Jeffries(i,j) = 2*(1-exp(-B));         
      end
    end
end

disp('Divergencia: '); disp(Divergencia);
disp('Coseno: '); disp(Coseno);
disp('Distancia de Jeffries: '); disp(Jeffries);

%distancias de Mahalanobis
%%xk = [4,5];

xk = [208,170, 135]; 

for i=1:1:c
    datos(i).dM = (xk-datos(i).m)*inv(datos(i).C)*(xk-datos(i).m)';
end

disp('distancia de (208,170, 135) a la clase 1:'); disp(datos(1).dM); 
disp('distancia de (208,170, 135) a la clase 2:'); disp(datos(2).dM); 
disp('distancia de (208,170, 135) a la clase 3:'); disp(datos(3).dM); 


xk = [208,40, 60]; 

for i=1:1:c
    datos(i).dM = (xk-datos(i).m)*inv(datos(i).C)*(xk-datos(i).m)';
end

disp('distancia de (208, 40, 60) a la clase 1:'); disp(datos(1).dM); 
disp('distancia de (208, 40, 60) a la clase 2:'); disp(datos(2).dM); 
disp('distancia de (208, 40, 60) a la clase 3:'); disp(datos(3).dM); 







