% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 10.5: Clasificaci�n: reconocimiento estad�stico

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 10.5.3 Clasificador no param�trico: ventana de Parzen 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; close all

% datos 3-dimensionales con 3 clases
% clase c1: 
datos(1).X = [200,160,120;210,170,130;215,172,133;210,165,134;198,177,138;208,170,135;208,40,60];

% clase c2: 
datos(2).X = [90,130,60;92,138,54;87,128,66;91,134,60;85,123,55];

% clase c3: 
datos(3).X = [30,44,178;20,40,180;24,42,184;28,50,176;22,46,181];

c = 3; %numero de clases 

%C�lculo de las medias
for i=1:1:c
    datos(i).m = mean(datos(i).X);
end

%C�lculo de las matrices de covarianza
for i=1:1:c
    datos(i).C = cov(datos(i).X);
end

xk = [208,170, 135]; 

%calculo de las probabilidades
h = 4;
for i=1:1:c
    [n,p] = size(datos(i).X);
    K = (1/n)*(1/((2*pi)^p/2)*det(datos(i).C)^(1/2)*h^p);
    SumaExp = 0;
    for j=1:1:n
      SumaExp = SumaExp + ...
      exp(-1/(2*h^2)*(xk-datos(i).X(j))*inv(datos(i).C)*(xk - datos(i).X(j))');
  
    end
    datos(i).p = SumaExp;
end

disp('distancia de (208, 170, 135) a la clase 1:'); disp(datos(1).p); 
disp('distancia de (208, 170, 135) a la clase 2:'); disp(datos(2).p); 
disp('distancia de (208, 170, 135) a la clase 3:'); disp(datos(3).p); 







