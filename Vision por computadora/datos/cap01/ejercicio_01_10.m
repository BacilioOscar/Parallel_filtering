% ejercicio 1.10: Operaci�n de extensi�n
% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 1.10: Representaci�n de im�genes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1.6.4 Operaciones sobre el contenido de las im�genes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
I = [1 32 119 45 45 160; 4 45 128 56 66 180; 0 20 110 78 76 240; ...
     4 10 120 89 34 124; 5 12 234 24 23 132; 6 11 122 32 77 192];

% Resoluci�n espacial: M: filas y N: columnas 
[M,N] = size(I);
 

p1 = 50; p2 = 150; %umbral
for i=1:1:M
    for j=1:1:N
        if (I(i,j) <= p1) || (I(i,j) >= p2) 
           Extension(i,j) = 0;
        else
           Extension(i,j) = round(((I(i,j) - p1)*255)/(p2-p1));
        end
    end
end

disp('Extension'); disp(Extension);