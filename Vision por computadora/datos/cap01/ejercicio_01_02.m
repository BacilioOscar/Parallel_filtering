% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 1.2: Representaci�n de im�genes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1.6.1 Representaci�n de im�genes digitales
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
% canales espectrales
R  = [2 5 8 0; 2 2 9 1; 2 4 6 3; 2 4 4 4];
G  = [8 4 4 3; 8 3 4 0; 6 9 1 6; 6 9 3 5];
B  = [7 6 7 3; 7 6 8 4; 1 9 7 2; 3 9 9 6];

% Acceso a los valores (1,2) y (3,1)
% debido a la representaci�n de Matlab dado que su origen es (1,1) en lugar
% de (0,0) se realiza un desplazamiento de una unidad en ambos ejes

%Adem�s la representaci�n de Matlab es: filas x columnas
% por lo que hay que expresarlo de forma inversa

% La imagen RGB se obtiene como sigue
I(:,:,1) = R; I(:,:,2) = G; I(:,:,3) = B;

disp('Valor del p�xel I(1,2)'); disp(I(3,2,:));
disp('Valor del p�xel I(3,1)'); disp(I(2,4,:));


