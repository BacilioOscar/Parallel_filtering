% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 1.1: Representaci�n de im�genes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1.6.1 Representaci�n de im�genes digitales
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
I = [1 32 119 45 45 160; 4 45 128 56 66 180; 0 20 110 78 76 240; ...
     4 10 120 89 34 124; 5 12 234 24 23 132; 6 11 122 32 77 192];

% Acceso a los valores (1,2) y (3,1)
% debido a la representaci�n de Matlab dado que su origen es (1,1) en lugar
% de (0,0) se realiza un desplazamiento de una unidad en ambos ejes

% Adem�s la representaci�n de Matlab es: filas x columnas
% por lo que hay que expresarlo de forma inversa

disp('Valor del p�xel I(2,3)'); disp(I(4,3));
disp('Valor del p�xel I(5,2)'); disp(I(3,6));