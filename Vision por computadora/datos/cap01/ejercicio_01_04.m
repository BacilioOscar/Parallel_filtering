% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 1.4: Representaci�n de im�genes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1.6.3 Perfiles de intensidad
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clear all;
I = [1 32 119 45 45 160; 4 45 128 56 66 180; 0 20 110 78 76 240; ...
     4 10 120 89 34 124; 5 12 234 24 23 132; 6 11 122 32 77 192];

% Resoluci�n espacial: M: filas y N: columnas 
[M,N] = size(I);
 
% perfil de intensidad a lo largo de la fila 2 (3 seg�n Matlab) 
P1 = I(3,:);
disp('Perfil P1: '); disp(P1);

[a,b] = size(P1);
for i = 1:1:b
  X(i) = i;
end
figure; bar(X,P1);

% perfil de intensidad a lo largo de la diagonal principal
h = 1;
for i=1:1:M
    for j=1:1:N
      if i == j  
        P2(h) = I(i,j);
        h = h + 1;
      end
    end
end
disp('Perfil P2: '); disp(P2);

clear X
[a,b] = size(P2);
for i = 1:1:b
  X(i) = i;
end
figure; bar(X,P2);
