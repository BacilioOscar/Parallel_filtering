% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 1.3: Representaci�n de im�genes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1.6.2 Resoluci�n espacial y en intensidad
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


I = [1 32 119 45 45 160; 4 45 128 56 66 180; 0 20 110 78 76 240; ...
     4 10 120 89 34 124; 5 12 234 24 23 132; 6 11 122 32 77 192];

% Resoluci�n espacial: M: filas y N: columnas 
[M,N] = size(I);
 
 
disp('Numero de filas: M ='); disp(M);
disp('Numero de columnas: N ='); disp(N);


%reducci�n de la dimensi�n espacial a la mitad
I2 = I(2:2:M,2:2:N);
disp('Reducci�n de las dimensiones a la mitad:'); disp (I2);

%reducci�n de la resoluci�n en intensidad
% Obtenci�n del m�ximo y minimo de la imagen de entrada
IMAX = 255; IMIN = 0;
CMAX = 32; CMIN = 0;
Unos = ones(M,N);

A = round(((CMAX-CMIN)/(IMAX-IMIN))*(I - IMIN*Unos) + CMIN*Unos);
disp('Imagen transformada: A ='); disp (A);

% Ampliaci�n de la resoluci�n de la imagen
In = round(((A - Unos*CMIN)/(CMAX-CMIN))*(IMAX - IMIN) + IMIN*Unos);
disp('Imagen recuperada:  I ='); disp (In);

