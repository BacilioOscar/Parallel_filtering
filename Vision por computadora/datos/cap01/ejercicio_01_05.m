% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 1.5: Representaci�n de im�genes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1.6.4 Operaciones sobre el contenido de las im�genes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
I = [1 32 119 45 45 160; 4 45 128 56 66 180; 0 20 110 78 76 240; ...
     4 10 120 89 34 124; 5 12 234 24 23 132; 6 11 122 32 77 192];

% Resoluci�n espacial: M: filas y N: columnas 
[M,N] = size(I);
 
% perfil de intensidad a lo largo de la fila 2 (3 seg�n Matlab) 
Unos = ones(M,N);;

Inversa = 255*Unos - I;

disp('Imagen inversa'); disp(Inversa);