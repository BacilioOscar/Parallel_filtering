% EJERCICIOS RESUELTOS DE VISIÓN POR COMPUTADOR
% Autores: Gonzalo Pajares y Jesús Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 1.6: Representación de imágenes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1.6.4 Operaciones sobre el contenido de las imágenes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
I = [1 32 119 45 45 160; 4 45 128 56 66 180; 0 20 110 78 76 240; ...
     4 10 120 89 34 124; 5 12 234 24 23 132; 6 11 122 32 77 192];

% Resolución espacial: M: filas y N: columnas 
[M,N] = size(I);
 

p1 = 100; %umbral
for i=1:1:M
    for j=1:1:N
        if I(i,j) <= p1
           Umbral(i,j) = 0;
        else
           Umbral(i,j) = 255;
        end
    end
end

disp('Imagen de Umbral binario'); disp(Umbral);