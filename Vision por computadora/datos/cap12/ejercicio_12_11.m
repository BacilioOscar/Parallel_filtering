% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 12.11: Formas a partir de la intensidad

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 12.7.5 Recuperaci�n de la superficie a partir del gradiente
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Genera superficies integrables a partir de gradientes
%
% C�digo adaptado del algoritmo de Frankot y Chellappa.
% Robert T. Frankot and Rama Chellappa
% A Method for Enforcing Integrability in Shape from Shading
% IEEE PAMI Vol 10, No 4 July 1988. pp 439-451

% Implementado por Peter Kovesi (Copyright(c) 2004)
% School of Computer Science & Software Engineering
% The University of Western Australia
% http://www.csse.uwa.edu.au/
%
% Utilizaci�n:      z = frankotchellappa(p,q)
%
% Argumentos:  p, q  matrices 2D de gradientes de una superficie
%
% Salida:      z     estima de las alturas de una superficie

% Copyright (c) 2004 Peter Kovesi
% School of Computer Science & Software Engineering
% The University of Western Australia
% http://www.csse.uwa.edu.au/
% 

function Ejercicio_12_11
    
  p = [0.0005    0.0020    0.0037         0   -0.0037   -0.0020   -0.0005;
       0.0149    0.0063    0.1120    0.0588   -0.0039   -0.0295   -0.0149;
       0.0965    0.2566    0.0531    0.0216   -0.0512   -0.1686   -0.0965;
       0.1678    0.2000    0.1181    0.0041   -0.0530   -0.1355   -0.1678;
       0.0965    0.2533    0.0529   -0.0037   -0.0282   -0.1071   -0.0965;
       0.0149    0.0361    0.1727    0.0281   -0.0495   -0.1225   -0.0149;
       0.0005    0.0020    0.0037         0   -0.0037   -0.0020   -0.0005];

  q =[0.0005    0.0149    0.0965    0.1678    0.0965    0.0149    0.0005
      0.0020    0.0216    0.1366    0.1393    0.1148    0.0355    0.0020
      0.0037    0.0962    0.0503    0.0900    0.0342   -0.0072    0.0037
           0    0.0323    0.0035    0.0022    0.0073   -0.0249         0
     -0.0037   -0.0038   -0.0466   -0.0978   -0.0390   -0.0421   -0.0037
     -0.0020   -0.0371   -0.0881   -0.1007   -0.1070   -0.0650   -0.0020
     -0.0005   -0.0149   -0.0965   -0.1678   -0.0965   -0.0149   -0.0005];
 
    a = 3;
    z = frankotchellappa(p,q);
    disp('Superficie z:'); disp(z);
    
    X=-a:1:a; Y = X;
    figure; meshz(X,Y,z);
end

function z = frankotchellappa(p,q)
    
    [filas,columnas] = size(p);
    
    % Las siguientes l�neas crean matrices especificando la frecuencia en
    % las direcciones x e y, correspondientes a las transformadas de
    % Fourier. El rango var�a entre -0.5 ciclos/pixel a + 0.5 ciclos/pixel.
    
    [wx, wy] = meshgrid(([1:columnas]-(fix(columnas/2)+1))/(columnas-mod(columnas,2)), ...
			([1:filas]-(fix(filas/2)+1))/(filas-mod(filas,2)));
    
    % Ajuste al cuadrante
    wx = ifftshift(wx); wy = ifftshift(wy);

    p = fft2(p);   % Transformada de Fourier de los gradientes
    q = fft2(q);

    % Integra en el dominio de la frecuencia mediante traslaci�n de fase
    % por pi/2 y promediando los coeficientes de Fourier por sus frecuencias en x e y 
    % para posteriormente dividir por la frecuencia al cuadrado.  
    % eps se a�ade para evitar divisiones por cero.
    
    Z = (-j*wx.*p -j*wy.*q)./(wx.^2 + wy.^2 + eps);  % Equation 21
    
    z = real(ifft2(Z));  % Reconstrucci�n

end