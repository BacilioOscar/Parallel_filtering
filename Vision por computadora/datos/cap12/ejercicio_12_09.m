% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 12.9: Formas a partir de la intensidad

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 12.7.4 M�todo variacional para obtenci�n del gradiente 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Ejercicio_12_09
  lambda = 0.05; %par�metro en el proceso iterativo
  niter  = 3; %numero de iteraciones
  ERROR = 0.001; 
  varianza = 0.001;
  
  a = 3; sg = 0.8; iluminacion = [0.5,0.2,1];
  E  = zeros(2*a+1,2*a+1); R = E;

  i1 = iluminacion(1); i2 = iluminacion(2); i3 = iluminacion(3);
  ro = 0.6;

  [E,p,q] = render(a,sg,iluminacion,ro);

  %%% A�adimos ruido a los datos: esta se utilizar�a normalmente con el fin
  %%% de variar el ruido
  % E = imnoise(E,'gaussian',0,varianza);
  %%%figure; imshow(E);
  
  % generada en un momento determinado con ruido. Esta es la que se utiliza
  % en los c�lculos del ejercicio 12.9
  
  E = [0.4924    0.5318    0.5163    0.4750    0.5583    0.5177    0.5498;
       0.5596    0.4396    0.4520    0.4150    0.5256    0.5574    0.5511;
       0.5044    0.4029    0.4368    0.5233    0.5127    0.5090    0.5785;
       0.4575    0.4535    0.5449    0.5146    0.4898    0.5007    0.6064;
       0.5293    0.4986    0.4583    0.4827    0.5608    0.6379    0.4831;
       0.5171    0.5104    0.4955    0.4991    0.5432    0.5521    0.5362;
       0.5809    0.5286    0.5734    0.5314    0.4657    0.5066    0.5790];
    
  p1 = p; q1 = q;
  for k = 1:1:niter
    for i=2:1:2*a
      for j=2:1:2*a
        pbarra = (p(i+1,j)+ p(i-1,j)+p(i,j+1)+p(i,j-1))/4;
        qbarra = (q(i+1,j)+ q(i-1,j)+q(i,j+1)+q(i,j-1))/4;
        
        Rval = Reflectancia(p(i,j),q(i,j),ro,iluminacion);
        dRp = Rpder(p(i,j),q(i,j),ro,iluminacion);
        dRq = Rqder(p(i,j),q(i,j),ro,iluminacion);
        if abs (dRp) > ERROR           
          p1(i,j) = pbarra + (0.25/lambda)*(E(i,j)-Rval)*dRp;
        end
        if abs (dRq) > ERROR           
          q1(i,j) = qbarra + (0.25/lambda)*(E(i,j)-Rval)*dRq;
        end
      end
    end
    p = p1; q = q1;  
    disp('Iteracion: '); disp(k);
    disp('p:'); disp(p);
    disp('q:'); disp(q);
  end
end % funcion relajacion


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   function Rval = Reflectancia(ps,qs,ro,iluminacion)
     i1 = iluminacion (1);
     i2 = iluminacion (2);
     i3 = iluminacion (3);
     Rval = ro*(-i1*ps - i2*qs + i3)/(sqrt(1+ps^2+qs^2)*sqrt(i1^2+i2^2+i3^2)); 
     %Rval = ro*(-i1*ps - i2*qs + i3)/sqrt(1+ps^2+qs^2); 
     if Rval < 0.0
         Rval = 0.0;
     end
   end

  function Rp = Rpder(ps,qs,ro,iluminacion)
     i1 = iluminacion (1);
     i2 = iluminacion (2);
     i3 = iluminacion (3);
     onep2q2 = 1+ps^2+qs^2;
	 onei = i1^2 + i2^2 + i3^2;
     result = -i1*(1+qs^2) + i2*ps*qs - i3*ps;
	 Rp = (result*ro)/(onep2q2^(3.0/2.0)*onei^0.5);
	 %Rp = result*ro/onep2q2^(3.0/2.0);
  end

  function Rq = Rqder(ps,qs,ro,iluminacion)
     i1 = iluminacion (1);
     i2 = iluminacion (2);
     i3 = iluminacion (3);
     onep2q2 = 1+ps^2+qs^2;
	 onei = i1^2 + i2^2 + i3^2;
	 result = -i2*(1+ps^2) + i1*ps*qs - i3*qs;
     Rq = (result*ro)/(onep2q2^(3.0/2.0)*onei^0.5);
     %Rq = result*ro/onep2q2^(3.0/2.0);
  end
  
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  function [E,p,q] = render(a,sg,iluminacion,ro);
  % render: crea una superficie Z(i,j), sobre una rejilla cuadrada de dimensi�n (x,y) 
  % Entradas: 
  %    a:  n�mero de elementos de la rejilla sobre la que se asienta la superficie 
  %    sg: ancho de la funci�n que define la superficie
  % Output:
  %    E: genera una imagen renderizada

  % generaci�n de la superficie
  A = 1;
  for i=1:1:(2*a+1)
    for j=1:1:(2*a+1)
        x = j-a-1; y = i-a-1;
        Z(i,j) = A*(x^2+y^2)*exp(-(x^2+y^2)/(sg^2*2));
    end
  end

  [p,q] = gradient(Z);

  % obtenci�n de la imagen con ps = iluminaci�n
  ps = iluminacion;
  for i=1:1:(2*a+1)
    for j=1:1:(2*a+1)
        Ea(i,j) = ro*(ps(3)- ps(1)*p(i,j)-ps(2)*q(i,j))/(sqrt(1+p(i,j)^2+q(i,j)^2)*sqrt(ps(1)^2+ps(2)^2+ps(3)^2));
        E(i,j) = max(0,Ea(i,j));
    end
  end
  return
end

