% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 12.8: Formas a partir de la intensidad

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 12.7.3 Obtenci�n del albedo y la direcci�n de iluminaci�n 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Ejercicio_12_08
  clear all; close all;
  iluminacion = [0.5,0.2,1];
  ro1 = 0.6;

  a = 3; sg = 0.8;
  [E,p,q] = render(a,sg,iluminacion,ro1);

  % calculo del albedo mediante la funci�n albedo_ilum
  [ro2,iluminacion2] = albedo_ilumin(E);
  
  disp('iluminaci�n obtenida:'); disp(iluminacion2);

end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  function [E,p,q] = render(a,sg,iluminacion,ro);
  % render: crea una superficie Z(i,j), sobre una rejilla cuadrada de dimensi�n (x,y) 
  % Entradas: 
  %    a:  n�mero de elementos de la rejilla sobre la que se asienta la superficie 
  %    sg: ancho de la funci�n que define la superficie
  % Output:
  %    E: genera una imagen renderizada

  % generaci�n de la superficie
  A = 1;
  for i=1:1:(2*a+1)
    for j=1:1:(2*a+1)
        x = j-a-1; y = i-a-1;
        Z(i,j) = A*(x^2+y^2)*exp(-(x^2+y^2)/(sg^2*2));
    end
  end

  [p,q] = gradient(Z);

  % obtenci�n de la imagen con ps = iluminaci�n
  ps = iluminacion;
  for i=1:1:(2*a+1)
    for j=1:1:(2*a+1)
        Ea(i,j) = ro*(ps(3)- ps(1)*p(i,j)-ps(2)*q(i,j))/(sqrt(1+p(i,j)^2+q(i,j)^2)*sqrt(ps(1)^2+ps(2)^2+ps(3)^2));
        E(i,j) = max(0,Ea(i,j));
    end
  end
  return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ro,iluminacion] = albedo_ilumin(E)

  % a partir de ina imagen de intensidad E, se obtiene el albedo (ro) y la iluminaci�n 
  % niter= numero iteraciones
  % 

  E2 = E.^2;
  [Ex Ey] = gradient(E);
  
  disp('Ex:'); disp(Ex);
  disp('Ey:'); disp(Ey);
  
  Em  = mean2(E); 
  E2m = mean2(E2);
  
  gamma = sqrt(6*pi^2*E2m-48*Em^2);
  
  disp('E media:'); disp(Em);
  disp('E cuadrado media:'); disp(E2m);
  disp('gamma:'); disp(gamma);
  
  ro = gamma/pi;
  disp('ro:'); disp(ro);

  cosSigma = 4*Em/gamma;
  disp('coseno de sigma:'); disp(cosSigma);
  
  sigma = acos(cosSigma);
 
  norm = sqrt(Ex.^2 + Ey.^2);
  
  Exb = norm.*Ex;
  Eyb = norm.*Ey; 
   
  Exbm = mean2(Exb);
  Eybm = mean2(Eyb);
  
  disp('Ex media:'); disp(Exbm);
  disp('Ey media:'); disp(Eybm);
  
  tanTau = Eybm/Exbm;
  tau = atan(tanTau);
  
  disp('tangente de tau:'); disp(tanTau);
  disp('tau:'); disp(tau);
  
  i1= cos(tau)*sin(sigma);
  i2= sin(tau)*sin(sigma);
  i3= cos(sigma);
  
  iluminacion = abs([i1, i2, i3]);
end
 
