% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 12.1: Formas a partir de la intensidad

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 12.7.1 Ecuaci�n de reflectancia 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; close all;
% variaci�n de la intensidad con la distancia al punto de coordenadas (A,B)
R= 128;
a= 256;
b= 256;

% Generaci�n de la figura 12.1(a): iluminaci�n central
A = a; B = b;
for j=1:1:512,
  for i=1:1:512,
    Im1(i,j)=0.5;
    Im2(i,j)=0.5;
    Im3(i,j)=0.5;    
  end   
end

R1 =  115;
Ic = 0.2; 
for j=1:1:512,
  for i=1:1:512,
    if (((j-a)^2+(i-b)^2) <= R^2)
      if (((j-a)^2+(i-b)^2) >= R1^2)
        dAB = sqrt((j-a)^2+(i-a)^2); 
        Intensidad =1-((1-Ic)/(R-R1))*(dAB-R1);
      else 
       Intensidad = 1.0;
      end  
      Im1(i,j) = Intensidad;
    end  
  end   
end

imshow(Im1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Generaci�n de la figura 12.1(b): iluminaci�n NorEste
A= a + 96;
B= b - 48;
h= sqrt((A-a)^2+(B-b)^2);

for j=1:1:512, 
  for i=1:1:512,
    if (((j-a)^2+(i-b)^2) <= R^2) 
      dAB = sqrt((j-A)^2+(i-B)^2); 
      Intensidad = 1.0 - dAB/(h+R);
      Im2(i,j)=Intensidad;
    end  
  end   
end

figure
imshow(Im2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Generaci�n de la figura 12.1(c): iluminaci�n SurOeste
M = 160;
Ic=(1/256)*50;

for j=1:1:512,
  for i=1:1:512,
    if (((j-a)^2+(i-b)^2) <= R^2) 
      dAB = sqrt((j-A)^2+(i-B)^2); 
      if (dAB > M )
        Intensidad = ((1-Ic)/(h+R-M))*(dAB-M)+Ic;
      else
        Intensidad = Ic;
      end
      Im3(i,j)=Intensidad;
    end  
  end   
end

figure
imshow(Im3)