% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 12.10: Formas a partir de la intensidad

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 12.7.4 M�todo variacional para obtenci�n del gradiente 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% C�digo adapatado de 
% Nilesh Heda & Biswarup Choudhury
% Dpt. of Computer Science and Engineering 
% Indian Institute of technology)

dimension  = 64;
irradiacioniacion  = 23;
radiacion   = 24;
lambda = 0.0001;
iteracion  = 1000;
varianza = 0.0001;
Ps = 1;
Qs = 0;

E= zeros(dimension,dimension);
R  = zeros(dimension,dimension);
Z  = zeros(dimension,dimension);
Z1 = zeros(dimension,dimension);
f  = zeros(dimension,dimension);
f1 = zeros(dimension,dimension);
g  = zeros(dimension,dimension);
g1 = zeros(dimension,dimension);
p  = zeros(dimension,dimension);
q  = zeros(dimension,dimension);

Zorigen = zeros(dimension,dimension);
porigen = zeros(dimension,dimension);
qorigen = zeros(dimension,dimension);
forigen = zeros(dimension,dimension);
gorigen = zeros(dimension,dimension);

for i=1:dimension,
   for j=1:dimension,
        x = (dimension/2)-i;
        y = (dimension/2)-j;
       
	    k = 1.0 - (x*x+y*y)/(radiacion*radiacion);
        l = radiacion*radiacion - (x*x + y*y);
	
    	if(l>0)
            Zorigen(i,j)=sqrt(l);
        else
            Zorigen(i,j)= -1/(Zorigen(i,j)+eps);
        end
        porigen(i,j) = -x/Zorigen(i,j);                   %  C�lculo de p
        qorigen(i,j) = -y/Zorigen(i,j);                   %  C�lculo de q 
	    tmp1 = porigen(i,j)*porigen(i,j) + qorigen(i,j)*qorigen(i,j); 
        tmp2 = Ps*Ps + Qs*Qs;         
        
        if(k>0)
            tmp3 = (1+ porigen(i,j)*Ps + qorigen(i,j)*Qs)/(sqrt(tmp1+1)*sqrt(tmp2+1));
            if(tmp3<0)
                E(i,j)=0;
            else
                E(i,j)=tmp3;    
            end
        end     
        
        % C�lculo de f,g a partir de p,q
	    forigen(i,j) = (2*porigen(i,j))/(1+sqrt(1+tmp1));
	    gorigen(i,j) = (2*qorigen(i,j))/(1+sqrt(1+tmp1));
       
	    % B�squeda de condiciones 
        k1 = 1.0 - (x*x+y*y)/(irradiacioniacion*irradiacioniacion);
       	l1 = irradiacioniacion*irradiacioniacion - (x*x + y*y); 
	
    	if(l1>0)
            Z(i,j)=sqrt(l1);
        else
            Z(i,j)= -1/(Z(i,j)+eps);
        end
 
	    p(i,j) = -x/Z(i,j);                   %  C�lculo de p
        q(i,j) = -y/Z(i,j);                   %  C�lculo de q 
	
	    tmp1 = p(i,j)*p(i,j) + q(i,j)*q(i,j);   % C�lculo de f,g a partir de p,q
	    f(i,j) = (2*p(i,j))/(1+sqrt(1+tmp1));
	    g(i,j) = (2*q(i,j))/(1+sqrt(1+tmp1));
    end
end

% Adici�n de ruido blanco gaussiano 
E = imnoise(E,'gaussian',0,varianza);

% Encontrar f & g mediante relajaci�n

for t=1:iteracion,
    for i=2:dimension-1,
        for j=2:dimension-1,
            tmp1 = f(i,j)*f(i,j)+g(i,j)*g(i,j);
            tmp2 = Ps*Ps + Qs*Qs;         
            tmp3 = (4+tmp1)*(4+tmp1)*sqrt(tmp2+1); 
            tmp4 = (4-tmp1-4*f(i,j)*Ps-4*g(i,j)*Qs);
            
            tmp5 = -2*( tmp4*f(i,j) + (4+tmp1)*(2*Ps-f(i,j)));
            tmp6 = -2*( tmp4*g(i,j) + (4+tmp1)*(2*Qs-g(i,j)));            
            
            %tmp5 = -16*f(i,j)+16*Ps- 4*f(i,j)*f(i,j)*Ps + 4*g(i,j)*g(i,j)*Ps + 8*f(i,j)*g(i,j)*Qs;
            R(i,j) = tmp4 / ((4+tmp1)*sqrt(tmp2+1));
            dRf = tmp5 /tmp3;
            dRg = tmp6 /tmp3;
            
            %f1(i,j) = 0.25*(f(i+1,j)+f(i-1,j)+f(i,j+1)+f(i,j-1))+ lambda/4*( E(i,j) - R(i,j) )*dRf;
            %g1(i,j) = 0.25*(g(i+1,j)+g(i-1,j)+g(i,j+1)+g(i,j-1))+ lambda/4*( E(i,j) - R(i,j) )*dRg;
            
            f1(i,j) = f(i,j)+ lambda/4*( E(i,j) - R(i,j) )*dRf;
            g1(i,j) = g(i,j)+ lambda/4*( E(i,j) - R(i,j) )*dRg;
        end
    end
    f = f1;
    g = g1;	
end

% transformaci�n del espacio estereogr�fico al espacio gradiacioniente
for i=1:dimension,
    for j=1:dimension,
        tmp = f(i,j)*f(i,j)+g(i,j)*g(i,j);
        p(i,j) = 4*f(i,j)/(4 - tmp);
        q(i,j) = 4*g(i,j)/(4 - tmp);
    end
end

% construct Depth map from p,q

Depth = ones(dimension,dimension);
for t=1:iteracion,
    for i=2:dimension-1,
        for j=2:dimension-1,          
            Z1(i,j)= 0.25*(Depth(i+1,j) + Depth(i-1,j) + Depth(i,j+1) + Depth(i,j-1)) + p(i,j) - p(i-1,j) + q(i,j) - q(i,j-1);  
        end
    end
    Depth = Z1;
end

surf(Depth);
