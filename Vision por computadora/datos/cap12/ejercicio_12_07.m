% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 12.7: Formas a partir de la intensidad

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 12.7.2 Est�reo fotom�trico 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% se declaran 3 vectores de iluminaci�n: n1, n2 y n3, que proporcionan las
% direcciones de iluminaci�n.
% el presente programa consta de tres partes: 
%   primera parte: se generan tres mapas de reflectancia E1, E2, E3 con
%                  los vectores unitarios mencionados y para una superficie 
%                  dada definida en render con los parametros de entrada y 
%                  determinado valor del albedo. 
%   segunda parte: con dos de esos mapas de reflectancia E1 y E2 por
%                  ejemplo, se obtiene el espacio gradiente a partir del cual 
%                  se trata de reconstruir la superficie original que gener� dichos mapas.
%   tercera parte: se utilizan los tres mapas y los tres vectores de
%                  iluminaci�n para obtener el espacio gradiente.
% NOTA: las reconstrucciones no son perfectas como ser�a deseable

function Ejercicio_12_07
  clear all; close all;
  a = 3; sg = 0.8;

  ro = 0.6; %albedo
  n1 = [0.5,0.2,1];
  n2 = [-0.7,0.8,1];
  n3 = [0.2,-0.9,1];

  % inicio primera parte
  p1 = n1(1); q1 = n1(2);
  p2 = n2(1); q2 = n2(2);

  r1 = sqrt(1+p1^2+q1^2);
  r2 = sqrt(1+p2^2+q2^2);

  E1 = render(a,sg,n1,ro);
  E2 = render(a,sg,n2,ro);
  E3 = render(a,sg,n3,ro);

  disp('E3:'); disp(E3);
  % fin primera parte

  % segunda parte: reconstruccion con dos imagenes
  p = ((E1.*E1*r1-1)*q2-(E2.*E2*r2-1)*q1)./(p1*q2-q1*p2);
  q = ((E2.*E2*r2-1)*p1-(E1.*E1*r1-1)*p2)./(p1*q2-q1*p2);
  % fin de la segunda parte

  %tercera parte: reconstrucci�n con tres im�genes m�s el albedo
  N = [n1; n2; n3];
  
  disp('N:'); disp(N);
  disp('inversa de N:'); disp(inv(N));

  [nc,nf] = size(E1);

  for i=1:1:nc
    for j=1:1:nf
        E = [E1(i,j); E2(i,j); E3(i,j)];
        st = (inv(N)*E)';
        rho(i,j) = sqrt(st(1)^2+st(2)^2+st(3)^2);
        if rho(i,j) ~= 0.0
          p(i,j) = (1/rho(i,j))*st(1);
          q(i,j) = (1/rho(i,j))*st(2);
          m(i,j) = (1/rho(i,j))*st(3);
        else
          p(i,j) = 0;
          q(i,j) = 0;
          m(i,j) = 0;
        end
    end
  end
  disp('rho:'); disp(rho);
      
  disp('n :'); disp(p(4,3)); disp(q(4,3)); disp(m(4,3)); 
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  function [E,p,q] = render(a,sg,iluminacion,ro);
  % render: crea una superficie Z(i,j), sobre una rejilla cuadrada de dimensi�n (x,y) 
  % Entradas: 
  %    a:  n�mero de elementos de la rejilla sobre la que se asienta la superficie 
  %    sg: ancho de la funci�n que define la superficie
  % Output:
  %    E: genera una imagen renderizada
  % Ejemplos de par�metros: a = 125; sg = 55; 
  % Ejemplos de vectores de iluminaci�n son:
  %        iluminaci�n = [0,0,1];
  %        iluminaci�n = [0.94/0.16,0.31/0.16,0.16];
  % Para mostrar la imagen habr� que realizar la siguiente operaci�n
  % I = mat2gray(E);
  % figure; imshow(I);

  % generaci�n de la superficie
  A = 1;
  for i=1:1:(2*a+1)
    for j=1:1:(2*a+1)
        x = j-a-1; y = i-a-1;
        Z(i,j) = A*(x^2+y^2)*exp(-(x^2+y^2)/(sg^2*2));
    end
  end

  [p,q] = gradient(Z);

  % obtenci�n de la imagen con ps = iluminaci�n
  ps = iluminacion;
  for i=1:1:(2*a+1)
    for j=1:1:(2*a+1)
        Ea(i,j) = ro*(ps(3)- ps(1)*p(i,j)-ps(2)*q(i,j))/(sqrt(1+p(i,j)^2+q(i,j)^2)*sqrt(ps(1)^2+ps(2)^2+ps(3)^2));
        E(i,j) = max(0,Ea(i,j));
    end
  end
  return
end


