% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 12.5: Formas a partir de la intensidad

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 12.7.1 Ecuaci�n de reflectancia 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Ejercicio_12_05
  close all; clear all;
  % valores del ejercicio 
  a = 125; sg = 25; ro = 0.96; iluminacion = [0, 0, 1]; 
  [Z,E,p,q] = render(a,sg,iluminacion,ro);
   
  % Para mostrar la imagen habr� que realizar la siguiente operaci�n
  I = mat2gray(E);
  figure; imshow(I);

end

function [Z,E,p,q] = render(a,sg,iluminacion,ro)
% render: crea una superficie Z(i,j), sobre una rejilla cuadrada de dimensi�n (x,y) 
% Entradas: 
%    a:  n�mero de elementos de la rejilla sobre la que se asienta la superficie 
%    sg: ancho de la funci�n que define la superficie
% Output:
%    E: genera una imagen renderizada

% generaci�n de la superficie
 A = 1;
 for i=1:1:(2*a+1)
    for j=1:1:(2*a+1)
        x = j-a-1; y = i-a-1;
        Z(i,j) = A*(x^2+y^2)*exp(-(x^2+y^2)/(sg^2*2));
    end
 end

 [p,q] = gradient(Z);

 % obtenci�n de la imagen con ps = iluminaci�n
 ps = iluminacion;
 X=-a:1:a; Y =-a:1:a;
 meshz(X,Y,Z);

 for i=1:1:(2*a+1)
    for j=1:1:(2*a+1)
        Ea(i,j) = ro*(ps(3)- ps(1)*p(i,j)-ps(2)*q(i,j))/(sqrt(1+p(i,j)^2+q(i,j)^2)*sqrt(ps(1)^2+ps(2)^2+ps(3)^2));
        E(i,j) = max(0,Ea(i,j));
    end
 end
 return
end

