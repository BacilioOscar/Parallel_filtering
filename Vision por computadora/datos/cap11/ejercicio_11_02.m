% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 11.2: Visi�n estereosc�pica

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 11.5.1 Geometr�a del sistema 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; close all;

% angulo que depende de la resolucion del CCD, 
% esto podr�a correspoinderse con que theta = atan(5 micras/10 mm)
% o sea 5 micras que ser�a el ancho de una celda CCD y 10 mm la focal y 
% aproximar�amos el �ngulo calculando esa tangente.

theta = atan(5*10^(-3)/10); 

% parte b) imprecisi�n
x = 0:1:50;  %desde 0 a 50 veces B
dZb = 0.5*tan(atan(2*x)+ theta) - x; 
figure; plot(x,2*dZb,'k','LineWidth',3); grid on;
xlabel('Z/b "numero de l�neas base"');
ylabel('precisi�n: \DeltaZ/b')

% parte a) incertidumbre
b = 16;
Z = 20*10^2;
dZ = 0.5*b*tan(atan(2*Z/b)+ theta) - Z;

disp('imprecisi�n:'); disp(dZ);
disp('incertidumbre:'); disp(dZb);



