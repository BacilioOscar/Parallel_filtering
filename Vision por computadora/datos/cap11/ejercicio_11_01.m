% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 11.1: Visi�n estereosc�pica

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 11.5.1 Geometr�a del sistema 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculo del numero de CCDs
clear all; close all;

% unidades en mil�metros
f = 20;
Z = 60*10^3;
X = 30*10^3;
b = 100;

xI = f/Z*(X+b/2);
xD = f/Z*(X-b/2);

%la anchura de cada celda del CCD en micrometros
anchoCCD = 10;

% numero de celdas 
nI = round(xI*10^3/anchoCCD);
nD = round(xD*10^3/anchoCCD);

disp('Celdas en x:'); disp(nI);
disp('Celdas en y:'); disp(nD);


