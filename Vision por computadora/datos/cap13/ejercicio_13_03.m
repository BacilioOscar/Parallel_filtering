% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 13.3: Movimiento

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 13.5.1 Flujo �ptico
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Ejercicio_13_03
  A = zeros(7,7); 
  B = A;
  A(3:5,3:5) = 200;
  B(2:4,4:6) = 200;

  [u,v] = LucasKanade(A, B, 3);
  disp('U: '); disp(u);
  disp('V: '); disp(v);
  figure, quiver(-u,-v,'linewidth',2);
end


function [u, v] = LucasKanade(im1, im2, windowSize);
  %algoritmo de Lucas-Kanade;

  [fx, fy, ft] = ObtenerDerivadas(im1, im2);

  [m,n] = size(fx);
  FX2 = zeros(m+2,n+2); FY2 = FX2; FXY = FX2; FTX = FX2; FTY = FX2;
  
  u = zeros(size(im1));
  v = zeros(size(im2));
  
  fx2 = fx.*fx; fy2 = fy.*fy;  fxy = fx.*fy;
  ftx = ft.*fx; fty = ft.*fy; 
  
  FX2(2:m+1,2:n+1) = fx2;  
  FY2(2:m+1,2:n+1) = fy2;  
  FXY(2:m+1,2:n+1) = fxy;  
  FTX(2:m+1,2:n+1) = ftx;  
  FTY(2:m+1,2:n+1) = fty;  
  
  for i=2:1:m+1 
     for j=2:1:n+1
         
        a11 = 0; a12 = 0; a22 = 0; b1 = 0; b2 = 0; 
        for ii=-1:1:1
            for jj=-1:1:1
              a11 = a11 + FX2(i+ii,j+jj);
              a12 = a12 + FXY(i+ii,j+jj);
              a22 = a22 + FY2(i+ii,j+jj);
              b1  =  b1 + FTX(i+ii,j+jj);
              b2  =  b2 + FTY(i+ii,j+jj);
            end
        end
        % sumas
        Sfx2a(i,j) = a11; Sfy2a(i,j) = a22; Sfxya(i,j) = a12;
        Sftxa(i,j) = b1; Sftya(i,j) = b2;
     end
  end

  [m1,n1] = size(Sftxa);

  Sfx2 = Sfx2a(2:m1,2:n1);
  Sfy2 = Sfy2a(2:m1,2:n1);
  Sfxy = Sfxya(2:m1,2:n1);
  Sftx = Sftxa(2:m1,2:n1);
  Sfty = Sftya(2:m1,2:n1);
        
  for i=1:1:m 
     for j=1:1:n
       a11 = Sfx2(i,j); a12 = Sfxy(i,j); a22 = Sfy2(i,j);
       b1  = Sftx(i,j); b2  = Sfty(i,j);
       A = [a11 a12; a12 a22];
       b = [b1 b2];
       U = pinv(A'*A)*A'*b';
       u(i,j)=U(1);
       v(i,j)=U(2);
     end
  end
  u(isnan(u))=0;
  v(isnan(v))=0;
end      

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [fx, fy, ft] = ObtenerDerivadas(A0,A1);

  Hx=[-1 0 1];
  Hy=[-1 0 1]';

  fx=imfilter(A0,Hx);
  fy=imfilter(A0,Hy);
  ft=(A1-A0);
end