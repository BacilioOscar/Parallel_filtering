% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 13.8: Movimiento

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 13.5.3 Filtro de Kalman
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Ejercicio_13_08

  % Filtro de Kalman
  % Se supone un punto elemental situado en la posici�n inicial p0 = (1,2), que se
  % desplaza con velocidad v = (2,1), es decir se mueve horizontalmente.

  % La matriz de transic�n de estados es A = [1 0 1 0; 0 1 0 1; 0 0 1 0; 0 0 0 1]
  % La matriz de medida H = [1 0 0 0; 0 1 0 0]

  %Asumiendo que las matrices de covarianza del ruido del sistema y de la medida
  %sean Q = 0.1*[1 1 1 1] y R = [1 1].

  % supondremos inicialmente la incertidumbre P0 = [1, 1, 1, 1] ya que se trata de la incertidumbre inicial.

  % construimos la estructura a pasar al filtro de Kalman
  s.B = 0;   s.u = 0;

  T = input ('introducir el intervalo de tiempo (delta t):  ');
  s.x = input ('introducir la posici�n inicial p.e. [x0;y0;vx;vy]:   ');

  %s.x = [1,2,2,1]';   % estado inicial k = 0
  %s.z = [7;5];       % observaci�n en el instante k = 1
  s.A = [1 0 1*T 0; 0 1 0 1*T; 0 0 1 0; 0 0 0 1];
  s.Q = 0.1*eye(4); % matriz de covaianza de estado 
  s.R = 1*eye(2);
  s.H = [1 0 0 0; 0 1 0 0];
  s.P = ones(4);

  resp = 'si';
  while resp == 'si'
      if resp == 'si'
          s.z = input ('introducir la medida observada z = [x;y]:  ');
      end

      disp('x previa:'); disp(s.x);
      
      s = filtroKalman(s);
      
      disp('x actualizada:'); disp(s.x);
      disp('K:'); disp(s.K);
      disp('P:'); disp(s.P);
      
      resp = input ('�mas actualizaciones?: si o no (entre comillas simples):'); 
  end
end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  function s = filtroKalman(s)
        
     % Implementaci�n del filtro de Kalman:
   
      % Prediction for state vector and covariance:
      %s.x = s.A*s.x + s.B*s.u;
      s.P = s.A * s.P * s.A' + s.Q;

      % Obtenci�n de la ganancia de Kalman:
      s.K = s.P*s.H'*inv(s.H*s.P*s.H'+s.R);

      % Correcci�n basada en la observaci�n:
      s.x = s.A*s.x + s.K*(s.z-s.H*s.A*s.x);
      s.P = s.P - s.K*s.H*s.P; 
   
      % se podr�a incluir aqu� la medida a realizar en la pr�xima iteraci�n
      % como sigue: s.z = s.x + 0.01*randn;

  end