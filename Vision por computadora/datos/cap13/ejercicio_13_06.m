% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 13.6: Movimiento

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 13.5.2 Imagen de diferencias
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% imagen de diferencias acumulativa

secuencia(1).imagen = [30 30 0 0 0; 30 30  0  0 0; 0  0  0  0  0; 0 0 0  0  0; 0 0 0 0 0];
secuencia(2).imagen = [ 0  0 0 0 0;  0 30 30  0 0; 0 30 30  0  0; 0 0 0  0  0; 0 0 0 0 0];
secuencia(3).imagen = [ 0  0 0 0 0;  0  0 30 30 0; 0  0 30 30  0; 0 0 0  0  0; 0 0 0 0 0];
secuencia(4).imagen = [ 0  0 0 0 0;  0  0  0  0 0; 0  0  0 30 30; 0 0 0 30 30; 0 0 0 0 0];

[m1,n1] = size(secuencia(1).imagen);

D = zeros(m1,n1);

n = 4;
for k=2:1:4
  ak = (k-1)/(n-1);  
  D = D + ak*(abs(secuencia(1).imagen - secuencia(k).imagen));
end

disp('d acumulaci�n:'); disp(D);

