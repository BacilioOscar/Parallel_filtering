% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 13.4: Movimiento

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 13.5.1 Flujo �ptico
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Gauss_Seidel

close all; clear all;
% Imagen en t=0
A0=zeros(7,7);
Region=200*ones(3);
A0(3:5,3:5)=Region;

% Siguiente imagen de la secuencia
A1=zeros(7,7);
A1(3:5,4:6)=Region; %horizontal  

% Tama�o de imagen
nf=7; nc=7;

% Fijamos el tama�o de bloque a procesar
bx=1; by=1;

% inicializamos las matrices del flujo �ptico
U = zeros(7,7);
V = U;

% Calculamos las derivadas
Hx=[-1 0 1]; Hy=[-1 0 1]';

fx=imfilter(A0,Hx); fy=imfilter(A0,Hy);
ft=(A1-A0);

nucleo = ones(bx,by);
fx2 = fx.^2; fy2 = fy.^2; ft2 = ft.^2;

%par�metros de entrada solicitados al usuario: 
% k: numero de iteraciones m�ximo por si no se produce la convergencia
% epsilon: cantidad para valorar la energ�a
% lambda: factor de regularizaci�n
K = input('introducir el n�mero de iteraciones m�ximo del algoritmo: ');
epsilon = input ('introducir el valor de epsilon para la energ�a: ');
lambda = input ('introducir el valor del factor de regularizaci�n lambda: ');

D = lambda^2 + fx2 + fy2; 

k = 1;
while k <= K
    % estimar el flujo �ptico medio en un entorno de vecindad 
    u_medio = imfilter(U,nucleo,'same');
    v_medio = imfilter(V,nucleo,'same');

    P = fx.*u_medio + fy.*u_medio + ft;
    U = u_medio - fx.*P./D;
    V = v_medio - fy.*P./D;

    % obtener la energ�a total
    Ux = imfilter(U,Hx); 
    Ux2 = Ux.^2;
    Uy = imfilter(U,Hy); 
    Uy2 = Uy.^2;
    Vx = imfilter(V,Hx); 
    Vx2 = Vx.^2;
    Vy = imfilter(V,Hy); 
    Vy2 = Vy.^2;

    E = (sum(sum((fx.*U + fy.*V + ft)^2 + lambda*(Ux2 + Uy2 + Vx2 + Vy2))))^2;
    
    c1 = 'Iteraci�n '; c2 = num2str(k);
    disp([c1,c2]);
    
    disp('U: '); disp(U);
    disp('V: '); disp(V);
    disp('Energ�a: '); disp(E);
    if E < epsilon
        break
    end
    k = k + 1;
end % bucle que especifica el m�ximo n�mero de iteraciones


figure;
quiver(U,V,0,'k','linewidth',2);
axis equal
axis ij
axis tight
