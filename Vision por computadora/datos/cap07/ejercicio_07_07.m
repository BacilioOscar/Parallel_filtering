% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 7.7: Extracci�n de bordes, regiones y puntos de inter�s
% Operador Laplaciana

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 7.7.7 Bordes: operador Laplaciana
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

A = [1 2 6 6; 1 2 8 6; 8 7 9 6; 8 5 8 9];

c = [-1 -1 -1; -1 8 -1; -1 -1 -1];

[m,n] = size(A);

Bordes = zeros(m,n);

for i=2:1:m-1
    for j=2:1:n-1
      jj = 1;
      g = 0; 
      for h=-1:1:1
          hh = 1;
          for k=-1:1:1
             g = g + c(jj,hh)*A(i+h,j+k); 
             hh = hh + 1; 
          end
          jj = jj + 1;
      end
      Bordes(i,j) = g; 
    end
end
disp('Laplaciana: '); disp(Bordes);
