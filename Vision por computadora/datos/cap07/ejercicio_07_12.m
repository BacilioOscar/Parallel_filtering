% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 7.12: Extracci�n de bordes, regiones y puntos de inter�s

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 7.7.9 Puntos de inter�s
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% puntos de inter�s (Harris)
   
   clear all;
   f = [2 2 2 8 8; 2 2 2 8 8; 2 2 2 8 8; 8 8 8 8 8; 8 8 8 8 8];
   [m,n] = size(f);
   
   %valor del umbral
   U3 = 500;

    % M�scaras de derivaci�n
    dx = [1 0 -1; 1 0 -1; 1 0 -1]; 
    dy = dx'; %dx resulta ser la matriz traspuesta de dy

    % Ix e Iy son las primeras derivadas derivadas horizontal y vertical 
    fX = conv2(f, dx, 'valid');    
    fY = conv2(f, dy, 'valid');
    
    fx = zeros(m,n); fy = fx;
    fx(2:1:m-1,2:1:n-1) = fX; 
    fy(2:1:m-1,2:1:n-1) = fY; 
    
  
    % Aplicar un suavizado Gaussiano (con sigma = 0.5 y dimensi�n 3x3) a las derivadas
    g = fspecial('gaussian');
    fX2 = conv2(fx.^2, g, 'valid'); % Smoothed squared image derivatives
    fY2 = conv2(fy.^2, g, 'valid');
    fXX = conv2(fx.*fx, g, 'valid');
    fYY = conv2(fy.*fy, g, 'valid');
    fXY = conv2(fx.*fy, g, 'valid');
    
    fx2 = zeros(m,n); fy2 = fx2; fxx = fx2; fyy = fx2; fxy = fx2;
    
    fx2(2:1:m-1,2:1:n-1) = fX2; 
    fy2(2:1:m-1,2:1:n-1) = fY2; 
    fxx(2:1:m-1,2:1:n-1) = fXX; 
    fyy(2:1:m-1,2:1:n-1) = fYY; 
    fxy(2:1:m-1,2:1:n-1) = fXY; 

    Autovalores1 = zeros(m,n);  Autovalores2 = Autovalores1;
    
    PInteres = zeros(m,n);
    for i=1+1:1:m-1
        for j=1+1:1:n-1
            % ventana
            a11 = 0; a12 = 0; a21 = 0; a22 = 0;
            for k=-1:1:1
                for h=-1:1:1
                   a11 = a11 + fx2(i+k,j+h);
                   a12 = a12 + fxy(i+k,j+h);
                   a21 = a12;
                   a22 = a22 + fy2(i+k,j+h);
                end
            end
            A = [a11 a12; a21 a22];
            M = eig(A);
            Autovalor1 = M(1,:);
            Autovalor2 = M(2,:);
            Autovalores1(i,j) = Autovalor1;
            Autovalores2(i,j) = Autovalor2;
            if Autovalor1 > U3 & Autovalor2 > U3
                PInteres(i,j) = 1;
            end
        end
    end

    disp('Autovalores lambda 1: '); disp(Autovalores1);
    disp('Autovalores lambda 2: '); disp(Autovalores2);
    disp('Puntos de inter�s: '); disp(PInteres);   
    