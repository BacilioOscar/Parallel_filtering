% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 7.5: Extracci�n de bordes, regiones y puntos de inter�s

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 7.7.5 Bordes: m�scaras de Kirsch
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% M�scaras de Kirsch

clear all;
A = [1 2 6 6; 1 2 8 6; 8 7 9 6; 8 5 8 9];

k0 = [-3 -3 5; -3 0 5; -3 -3 5];  k1 = [-3 5 5; -3 0 5; -3 -3 -3];
k2 = [5  5 5; -3 0 -3; -3 -3 -3]; k3 = [5 5 -3; 5 0 -3; -3 -3 -3];
k4 = [5 -3 -3; 5 0 -3; 5 -3 -3];  k5 = [-3 -3 -3; 5 0 -3; 5 5 -3];
k6 = [-3 -3 -3; -3 0 -3; 5 5 5];  k7 = [-3 -3 -3; -3 0 5; -3 5 5];

Orientaciones = [0 45 90 135 180 225 270 315];

[m,n] = size(A);

G0 = zeros(m,n);
G1 = G0; G2 = G0; G3 = G0; G4 = G0; G5 = G0; G6 = G0; G7 = G0;
G = G0; Angulos = zeros(m,n);

for i=2:1:m-1
    for j=2:1:n-1
      jj = 1;
      g0 = 0; g1 = 0; g2 = 0; g3 = 0; g4 = 0; g5 = 0; g6 = 0; g7 = 0;
      for h=-1:1:1
          hh = 1;
          for k=-1:1:1
             g0 = g0 + k0(jj,hh)*A(i+h,j+k); 
             g1 = g1 + k1(jj,hh)*A(i+h,j+k); 
             g2 = g2 + k2(jj,hh)*A(i+h,j+k); 
             g3 = g3 + k3(jj,hh)*A(i+h,j+k); 
             g4 = g4 + k4(jj,hh)*A(i+h,j+k); 
             g5 = g5 + k5(jj,hh)*A(i+h,j+k);
             g6 = g6 + k6(jj,hh)*A(i+h,j+k); 
             g7 = g7 + k7(jj,hh)*A(i+h,j+k); 
             hh = hh + 1; 
          end
          jj = jj + 1;
      end
      G0(i,j) = abs(g0); G1(i,j) = abs(g1); G2(i,j) = abs(g2); G3(i,j) = abs(g3); 
      G4(i,j) = abs(g4); G5(i,j) = abs(g5); G6(i,j) = abs(g6); G7(i,j) = abs(g7); 
      GR = [G0(i,j),G1(i,j),G2(i,j),G3(i,j),G4(i,j),G5(i,j),G6(i,j),G7(i,j)];
      Maximo = max(GR);
      G(i,j) = Maximo;
      [fila,col] = find(Maximo == GR);
      [a,b] = size(col);
      if b > 1
          columna = col(1,1);
      else
          columna = col;
      end
      Angulos(i,j) = Orientaciones(1,columna);
    end
end


% umbral para la detecci�n de bordes
Umbral = 9;
Bordes = G >= Umbral;

disp('Modulo: '); disp(G);
disp('�ngulos: '); disp(Angulos);

