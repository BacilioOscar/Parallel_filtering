% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 7.2: Extracci�n de bordes, regiones y puntos de inter�s

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 7.7.2 Bordes: operadores de Sobel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Sobel

clear all;
A = [1 2 6 6; 1 2 8 6; 8 7 9 6; 8 5 8 9];

[m,n] = size(A);

% operadores de Sobel, definidos de forma inversa al texto porque en la
% convoluci�n que se aplica para la obtenci�n de los bordes se realiza 
% una operaci�n de reflexi�n 
gx = [-1 0 1; -2 0 2; -1 0 1];
gy = [1 2 1; 0 0 0; -1 -2 -1];

Grx = conv2(A,gx,'valid');
Gry = conv2(A,gy,'valid');

% c�lculo de los valores de los gradientes
Gx = zeros(m,n); Gy = Gx;

Gx(2:1:m-1,2:1:n-1) = Grx; 
Gy(2:1:m-1,2:1:n-1) = Gry;

% m�dulo del gradiente
G = abs(Gx) + abs(Gy);

% umbral para la detecci�n de bordes
Umbral = 25;
Bordes = G >= Umbral;

disp('Gx: '); disp(Gx);
disp('Gy: '); disp(Gy);
disp('Modulo: '); disp(G);
disp('Bordes: '); disp(Bordes);
