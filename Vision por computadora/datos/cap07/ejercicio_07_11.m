% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 7.11: Extracci�n de bordes, regiones y puntos de inter�s

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 7.7.9 Puntos de inter�s
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% puntos de inter�s

   f = [2 2 2 8 8; 2 2 2 8 8; 2 2 2 8 8; 8 8 8 8 8; 8 8 8 8 8];
   [m,n] = size(f);
   
   %valor del umbral
   U2 = 8.0;

    % M�scaras de derivaci�n
    dx = [1 0 -1; 1 0 -1; 1 0 -1]; 
    dy = dx'; %dx resulta ser la matriz traspuesta de dy

    % Ix e Iy son las primeras derivadas derivadas horizontal y vertical 
    fX = conv2(f, dx, 'valid');    
    fY = conv2(f, dy, 'valid');
    
    fx = zeros(m,n); fy = fx;
    fx(2:1:m-1,2:1:n-1) = fX; 
    fy(2:1:m-1,2:1:n-1) = fY; 
    
    % Calculo de las segundas derivadas
    fxx = zeros(m,n); fyy = fxx; fxy = fxx;
    
    fXX = conv2(fx,dx,'valid');
    fYY = conv2(fy,dy,'valid');
    fXY = conv2(fx,dy,'valid');

    fxx(2:1:m-1,2:1:n-1) = fXX; 
    fyy(2:1:m-1,2:1:n-1) = fYY; 
    fxy(2:1:m-1,2:1:n-1) = fXY; 
    
    K = abs((fxx.*fyy - fxy^2)./((fx.^2 + fy.^2)+eps));

    PInteres = K >= U2;
    
    format long e
    disp('K: '); disp(K);
    disp('Puntos de inter�s: '); disp(PInteres);
