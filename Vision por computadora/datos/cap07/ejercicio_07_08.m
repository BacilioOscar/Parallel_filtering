% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 7.8: Extracci�n de bordes, regiones y puntos de inter�s
% Generador de la m�scara LoG

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 7.7.8 Bordes: operador Laplaciana de la Gaussiana
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
K = 128; sigma = 2.0;

m = 7; n = 7;

h = 1;
for x=-m:1:m
    k = 1;
    for y=-n:1:n
       LoG (h,k)= K*(2-(x^2+y^2)/sigma^2)*exp(-(x^2+y^2)/(2*sigma^2));
       k = k + 1;
    end
    h = h + 1;
end

Cuadrante = LoG(1:8,1:8);

[X,Y] = meshgrid(-m:1:m, -n:1:n);

surf(X,Y,LoG)

disp('Cuadrante Superior Izquierdo: '); disp(Cuadrante);
