% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 7.4: Extracci�n de bordes, regiones y puntos de inter�s

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 7.7.4 Bordes: operador de Roberts
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Operador de Roberts

clear all;
A = [1 2 6 6; 1 2 8 6; 8 7 9 6; 8 5 8 9];

[m,n] = size(A);

G = zeros(m,n);

for i=2:1:m-1
    for j=2:1:n-1
      D1 = A(i,j)   - A(i-1,j-1);
      D2 = A(i,j-1) - A(i-1,j);
      G(i,j) = abs(D1) + abs(D2); 
    end
end


% umbral para la detecci�n de bordes
Umbral = 9;
Bordes = G >= Umbral;

disp('Modulo: '); disp(G);
disp('Bordes: '); disp(Bordes);
