% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 7.9: Extracci�n de bordes, regiones y puntos de inter�s
% Representaci�n gr�fica de la m�scara LoG

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 7.7.8 Bordes: operador Laplaciana de la Gaussiana
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; close all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%sigma = 5.0 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
K = 128; sigma = 2.0;
m = 17; n = 17;

h = 1;
for x=-m:1:m
    k = 1;
    for y=-n:1:n
       LoG (h,k)= K*(2-(x^2+y^2)/sigma^2)*exp(-(x^2+y^2)/(2*sigma^2));
       k = k + 1;
    end
    h = h + 1;
end

Cuadrante = LoG(1:m+1,1:n+1);

[X,Y] = meshgrid(-m:1:m, -n:1:n);

colormap(hsv)
surf(X,Y,LoG)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%sigma = 5.0 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sigma = 5.0;
h = 1;
for x=-m:1:m
    k = 1;
    for y=-n:1:n
       LoG (h,k)= K*(2-(x^2+y^2)/sigma^2)*exp(-(x^2+y^2)/(2*sigma^2));
       k = k + 1;
    end
    h = h + 1;
end

Cuadrante = LoG(1:m+1,1:n+1);

[X,Y] = meshgrid(-m:1:m, -n:1:n);

figure; colormap(hsv)
surf(X,Y,LoG)

