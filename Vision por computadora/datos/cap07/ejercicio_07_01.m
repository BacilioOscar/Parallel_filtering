% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 7.1: Extracci�n de bordes, regiones y puntos de inter�s:
% gradiente

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 7.7.1 Bordes: gradiente de una imagen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Gradiente

clear all;
A = [1 2 6 6; 1 2 8 6; 8 7 9 6; 8 5 8 9];

% dimensiones de la imagen
[m,n] = size(A);

% gradiente en la direcci�n X
Gx = zeros(m,n);
for i=1:1:n
    for j=2:m
       Gx(i,j-1) = A(i,j) - A(i,j-1);   
    end
end

% gradiente en la direcci�n Y
Gy = zeros(m,n);
for i=2:1:n
    for j=1:m
       Gy(i-1,j) = A(i,j) - A(i-1,j);   
    end
end


%Calculo del m�dulo y direcci�n del gradiente
% seg�n la ecuaci�n (7.2)
Modulo1 = round(sqrt(Gx.^2+Gy.^2));

Angulos = atan2(Gy,Gx);

% seg�n la ecuaci�n (7.3)
Modulo2 = abs(Gx)+ abs(Gy);

disp('M�dulo del gradiente vers. 1:'); disp(Modulo1);
disp('M�dulo del gradiente vers. 2:'); disp(Modulo2);

Umbral = 4; 
% Puntos de borde ecuaci�n (7.4) 
B1 = (Modulo1 >= Umbral); 
B2 = (Modulo2 >= Umbral); 

disp('Bordes 1: '); disp(B1);
disp('Bordes 2: '); disp(B2);

