% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 7.14: Extracci�n de bordes, regiones y puntos de inter�s

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 7.7.9 Puntos de inter�s
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Operador de Moravec

   clear all;
   f = [2 2 2 8 8; 2 2 2 8 8; 2 2 2 8 8; 8 8 8 8 8; 8 8 8 8 8];
   [m,n] = size(f);
   
   % valor del umbral
   U4 = 50; 
    
   E = zeros(m,n);
   for i=1+1:1:m-1
       for j=1+1:1:n-1
           % ventana
           E(i,j) = (f(i,j)-(f(i-1,j)))^2 + (f(i,j)-(f(i+1,j)))^2+...
               (f(i,j)-(f(i,j-1)))^2 + (f(i,j)-(f(i,j+1)))^2;
       end
   end

   PInteres = (E > U4);              

   disp('VAR: '); disp(E);
   disp('Puntos de Inter�s: '); disp(PInteres);
