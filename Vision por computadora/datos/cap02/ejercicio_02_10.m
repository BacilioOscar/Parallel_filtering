% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 2.10: Transformaci�n del dominio

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2.7.4 Transformada de Wavelets
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Ejercicio_02_10
  clear all;

  f = [1 2 3 4; 5 6 7 8; 9 10 11 12; 13 14 15 16];

  % transformada discreta de wavelets usando el toolbox de wavelets de Matalab
  [I1,I2,I3,I4] = dwt2(f,'Haar');

  % transformada discreta inversa
  PBI=[1,1]*sqrt(1/2);
  PAI=[1,-1]*sqrt(1/2);
  fa=miiddwt2(I1,I2,I3,I4,PBI,PAI);

  disp('Reconstrucci�n original: '); disp(fa);
  
  % Usando el toolbox de wavelets de Matalab
  fa = idwt2(I1,I2,I3,I4,'Haar');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%Funci�n auxiliar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function I=miiddwt2(cA,cH,cV,cD,PBI,PAI)
  % function I=miidwt2(cA,CH,cV,cD,wavelet)
  % Calcula la reconstrucci�n de la matriz o vector de entrada
  % utilizando los filtros que se especifican

  cA=addrowz(cA);
  cH=addrowz(cH);
  cV=addrowz(cV);
  cD=addrowz(cD);

  I1=conv2(PBI,cA')';
  I2=conv2(PAI,cH')';
  I3=conv2(PBI,cV')';
  I4=conv2(PAI,cD')';

  I1=addcolumz(I1);
  I2=addcolumz(I2);
  I3=addcolumz(I3);
  I4=addcolumz(I4);

  I1=conv2(PBI,I1);
  I2=conv2(PBI,I2);
  I3=conv2(PAI,I3);
  I4=conv2(PAI,I4);

  I=I1+I2+I3+I4;
  return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%Funci�n auxiliar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Z=addrowz(A)
  [nf,nc]=size(A);
  Z=zeros(2*nf-1,nc);
  for i=1:nf,
    Z(2*i-1,:)=A(i,:);
  end
  return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%Funci�n auxiliar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Z=addcolumz(A)
  [nf,nc]=size(A);
  Z=zeros(nf,2*nc-1);
  for i=1:nc,
    Z(:,2*i-1)=A(:,i);
  end
end