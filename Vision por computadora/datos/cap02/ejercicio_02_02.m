% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 2.2: Transformaci�n del dominio

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2.7.1 La transformada discreta de Fourier
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Ejercicio_02_02
  clear all;

  f = magic(4);

  % transformada discreta de Fourier (implementaci�n propia)
  [F,Fc] = mifft2(f); % Transformada de Fourier y su traslaci�n al centro

  % transformada inversa de Fourier (implementaci�n propia)
  fi = miifft2(F);

  % transformada inversa de Fourier: implementaci�n de Matlab
  fi = ifft2(F);
  disp('Transformada inversa de Fourier compleja:'); disp(fi);
  disp('Transformada inversa de Fourier real:'); disp(real(fi));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%Funci�n auxiliar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [F,Fc]=mifft2(f)
  % function [F,Fc]=mifft2(f)
  % Calcula la transformada de 2D de Fourier de la imagen f
  % de dimensi�n: M columnas y N filas
  % para las frecuencias u=0,1,2,..M-1,v=0,1,2,..N-1 
  % Fc es la transformada desplazada al centro
  % Equivale a los comandos:
  % F=fft2(f), Fc=fftshift(F)

  j=sqrt(-1);   

  % Para entender el c�digo hay que tener en cuenta que  
  % en Matlab la notaci�n de filas y columnas es distinta a la utilizada en
  % el libro. En una matriz f(i,k)las filas corresponden a la primera
  % coordenada y las columnas a la segunda.

  [N,M]=size(f);

  for u=0:M-1
      for v=0:N-1,
          F(v+1,u+1)=0;
          for x=0:M-1
              for y=0:N-1,
                  F(v+1,u+1)=F(v+1,u+1)+f(y+1,x+1)*exp(-j*2*pi*(u*x/M+v*y/N));
              end
          end
      end
  end

  % Trasladamos al centro
  Fc=fftshift(F);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%Funci�n auxiliar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function fi=miifft2(F)
  % function fi=miifft2(F)
  % Resuelve el ejercicio 2.2
  % Calcula la transformada 2D inversa de Fourier 
  % para  x=0,1,2,..M-1, y=0,1,2,..N-1 
  % donde [N,M]=size(F)
  % Equivale al comando:
  % fi=ifft2(F)

  j=sqrt(-1);   

  % Para entender el c�digo hay que tener en cuenta que  
  % en Matlab la notaci�n de filas y columnas es distinta a la utilizada en
  % el libro. En una matriz fi(i,k)las filas corresponden a la primera
  % coordenada y las columnas a la segunda.

  [N,M]=size(F);

  for x=0:M-1
      for y=0:N-1,
          fi(y+1,x+1)=0;
          for u=0:M-1
              for v=0:N-1,
                  fi(y+1,x+1)=fi(y+1,x+1)+F(v+1,u+1)*exp(j*2*pi*(u*x/M+v*y/N))/M/N;
              end
          end
      end
  end
end