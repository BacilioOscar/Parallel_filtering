% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 2.7: Transformaci�n del dominio

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2.7.3 La transformada del coseno
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Ejercicio_02_07

  clear all;

  f = [2 8 8 2; 2 8 8 2; 2 8 8 2; 2 8 8 2];

  % transformada discreta del coseno: ejercicio 2.7
  CF=midct2(f);
  
  disp('Transformada del Coseno: '); disp(CF);

  % usando el comando de Matlab
  CF1 = dct2(f);
  
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%Funci�n auxiliar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function C=midct2(f)
  % function C=midct2(f)
  % Calcula la transformada de 2D del Coseno de la imagen f
  % de dimensi�n: M columnas y N filas
  % para las frecuencias u=0,1,2,..M-1,v=0,1,2,..N-1 
  % Equivale al comando:
  % C=dct2(f)

  % Para entender el c�digo hay que tener en cuenta que  
  % en Matlab la notaci�n de filas y columnas es distinta a la utilizada en
  % el libro. En una matriz f(i,k)las filas corresponden a la primera
  % coordenada y las columnas a la segunda.

  [N,M]=size(f);
  alfau=[1/sqrt(M),sqrt(2/M)*ones(1,M-1)];
  alfav=[1/sqrt(N),sqrt(2/N)*ones(1,N-1)];
  for u=0:M-1
    for v=0:N-1,
        C(v+1,u+1)=0;
        for x=0:M-1
            for y=0:N-1,
                C(v+1,u+1)=C(v+1,u+1)+alfau(u+1)*alfav(v+1)*...
                                      f(y+1,x+1)*cos(pi*u*(2*x+1)/M/2)*cos(pi*v*(2*y+1)/N/2);
            end
        end
    end
  end
end
