% EJERCICIOS RESUELTOS DE VISIÓN POR COMPUTADOR
% Autores: Gonzalo Pajares y Jesús Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 2.4: Transformación del dominio
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2.7.2 Filtrado espacial de imágenes digitales
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Ejercicio_02_04
  clear all;

  f = magic(3);

  % nucleo de convolución
  h = [1,2,3;4,5,6;7,8,9];

  % convolución de f*h (implementación propia)
  f0 = miconv2(f,h);

  disp('Valor del elemento central f0(2,2):');
  disp(f0(3,3));

end


 %%%%%%%%%%%%%%%%%%%%%%%%%%%%Función auxiliar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 function f=miconv2(A,B,shape)
  %function f=miconv2(A,B,shape)
  % Realiza la convolución de las matrices A y B
  % Si shape='same' devuelve una matriz de la dimensión de A
  % En cualquier otro caso devuelve la convolución completa


  [N,M]=size(A);
  [L,K]=size(B);
  f=zeros(N+L-1,M+K-1);
  for m=1:M,
      for n=1:N
          I=A(n,m)*B;
          f(n:n+L-1,m:m+K-1)=f(n:n+L-1,m:m+K-1)+I;
      end
  end

  if nargin==3,
      if shape=='same',
          nf=floor(L/2);
          nc=floor(K/2);
          f=f(nf+1:nf+N,nc+1:nc+M);
      end
  end
end