% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 2.12: Transformaci�n del dominio

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2.7.4 Transformada de componentes principales
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  clear all;

  X = [1 2 5 9 6; 4 6 7 1 9; 5 8 5 4 2; 3 2 4 8 6; 2 6 2 9 8;...
       6 8 6 3 1; 4 5 3 3 8; 1 7 3 6 8; 7 9 7 2 6]';

  %reducci�n de la dimensionalidad, n�mero de autovectores a utilizar
  d = 2;  

  [N,M]=size(X);

  % calcular la matriz de Covarianza de X
  mx = mean(X')';  % Valor medio de X
  for i=1:M, 
    Xm(:,i) = X(:,i) - mx; 
  end  
  Cx=Xm*Xm'/(M-1);
  
    
  % autovectores, ordenados de acuerdo a sus autovalores y normalizados
  [V,S]   = eig(Cx);
  eigval  = diag(S);
  [Y,ind] = sort(abs(eigval)); 
  eigval  = eigval(flipud(ind));  % Autovalores en orden decreciente
  V       = V(:,flipud(ind));     % Matriz de autovectores ordenada
  % autovectores normalizados
  for i=1:d
     Vn(:,i) = (V(:,i) / norm(V(:,i)));
  end
 
  % considerar s�lo los d primeros autovectores
  Vd = Vn(:,1:d);
  D = abs(eigval)/sum(abs(eigval));
  Dd = D(1:d); 

  % proyectar los datos utilizando los primeros q autovectores
  A=Vd';
  y = A*Xm;

  %calcular la matriz de covarianza de y
  Cy = cov(y');
  
  disp('Proyecci�n de los datos en y: '); disp(y);
  disp('Matriz de covarianza Cy: '); disp(Cy);

  
  %reconstrucci�n de los datos originales
  Mx = repmat(mx,1,M);
  XX = A'*y+Mx;

  disp('Reconstrucci�n de los datos originales X: '); disp(XX);


  