% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 2.6: Transformaci�n del dominio

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2.7.2 Filtrado espacial de im�genes digitales
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Ejercicio_02_06
  clear all;

  A = [2 2 2; 2 100 2; 2 2 2]; %imagen original
  h = (1/9)*ones(3);           %n�cleo del filtro paso bajo
  g = [-1 -1 -1; -1 8 -1; -1 -1 -1]; %n�cleo del filtro paso alto

  % parte correspondiente a la convoluci�n.
  Ah = miconv2(A,h,'same'); 
  Ag = miconv2(A,g,'same');
  
  disp('Convoluci�n A0h: '); disp(Ah);
  disp('Convoluci�n A0g: '); disp(Ag);
  
  % parte correspondiente a la transformada en frecuencia.
  Aa=[A,zeros(3,2);zeros(2,5)]; % ampliamos A con ceros
  ha=[h,zeros(3,2);zeros(2,5)]; % ampliamos h con ceros
  ga=[g,zeros(3,2);zeros(2,5)]; % ampliamos g con ceros
  FA = abs(mifft2(Aa));
  FH = abs(mifft2(ha));
  FG = abs(mifft2(ga));

  disp('Transformada en frecuencia de A:'); disp(FA);
  disp('Transformada en frecuencia de h:'); disp(FH);
  disp('Transformada en frecuencia de g:'); disp(FG);
  
  
  AH = abs(FA.*FH);
  AG = abs(FA.*FG);

  disp('Filtrado en frecuencia AH (m�dulo): '); disp(AH);
  disp('Filtrado en frecuencia AG (m�dulo): '); disp(AG);
  
  Aih = miifft2(AH);
  Aig = miifft2(AG);
    
  %Parte central
  Aihc=Aih(2:4,2:4);
  Aigc=Aig(2:4,2:4);

  disp('Parte central del resultado del filtrado Ah: '); disp(real(Aihc));
  disp('Parte central del resultado del filtrado Ag: '); disp(real(Aigc));
  
  % Realizamos los mismos c�lculos usando las funciones de Matlab

  % parte correspondiente a la convoluci�n.
  Ah = conv2(A,h,'same'); 
  Ag = conv2(A,g,'same');

  % parte correspondiente a la transformada en frecuencia.
  FA = fft2(A,5,5);
  FH = fft2(h,5,5);
  FG = fft2(g,5,5);

  AH = FA.*FH;
  AG = FA.*FG;

  Aih = ifft2(AH);
  Aig = ifft2(AG);
  %Parte central
  Aihc=Aih(2:4,2:4);
  Aigc=Aig(2:4,2:4);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%Funci�n auxiliar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [F,Fc]=mifft2(f)
    % function [F,Fc]=mifft2(f)
    % Calcula la transformada de 2D de Fourier de la imagen f
    % de dimensi�n: M columnas y N filas
    % para las frecuencias u=0,1,2,..M-1,v=0,1,2,..N-1 
    % Fc es la transformada desplazada al centro
    % Equivale a los comandos:
    % F=fft2(f), Fc=fftshift(F)

    j=sqrt(-1);   

    % Para entender el c�digo hay que tener en cuenta que  
    % en Matlab la notaci�n de filas y columnas es distinta a la utilizada en
    % el libro. En una matriz f(i,k)las filas corresponden a la primera
    % coordenada y las columnas a la segunda.

    [N,M]=size(f);

    for u=0:M-1
      for v=0:N-1,
          F(v+1,u+1)=0;
          for x=0:M-1
              for y=0:N-1,
                  F(v+1,u+1)=F(v+1,u+1)+f(y+1,x+1)*exp(-j*2*pi*(u*x/M+v*y/N));
              end
          end
      end
    end

    % Trasladamos al centro
    Fc=fftshift(F);
end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%Funci�n auxiliar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function fi=miifft2(F)
  % function fi=miifft2(F)
  % Resuelve el ejercicio 2.2
  % Calcula la transformada 2D inversa de Fourier 
  % para  x=0,1,2,..M-1, y=0,1,2,..N-1 
  % donde [N,M]=size(F)
  % Equivale al comando:
  % fi=ifft2(F)

  j=sqrt(-1);   

  % Para entender el c�digo hay que tener en cuenta que  
  % en Matlab la notaci�n de filas y columnas es distinta a la utilizada en
  % el libro. En una matriz fi(i,k)las filas corresponden a la primera
  % coordenada y las columnas a la segunda.

  [N,M]=size(F);

  for x=0:M-1
      for y=0:N-1,
          fi(y+1,x+1)=0;
          for u=0:M-1
              for v=0:N-1,
                  fi(y+1,x+1)=fi(y+1,x+1)+F(v+1,u+1)*exp(j*2*pi*(u*x/M+v*y/N))/M/N;
              end
          end
      end
  end
end

 %%%%%%%%%%%%%%%%%%%%%%%%%%%%Funci�n auxiliar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 function f=miconv2(A,B,shape)
  %function f=miconv2(A,B,shape)
  % Realiza la convoluci�n de las matrices A y B
  % Si shape='same' devuelve una matriz de la dimensi�n de A
  % En cualquier otro caso devuelve la convoluci�n completa


  [N,M]=size(A);
  [L,K]=size(B);
  f=zeros(N+L-1,M+K-1);
  for m=1:M,
      for n=1:N
          I=A(n,m)*B;
          f(n:n+L-1,m:m+K-1)=f(n:n+L-1,m:m+K-1)+I;
      end
  end

  if nargin==3,
      if shape=='same',
          nf=floor(L/2);
          nc=floor(K/2);
          f=f(nf+1:nf+N,nc+1:nc+M);
      end
  end
end