% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 2.1: Transformaci�n del dominio

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2.7.1 La transformada discreta de Fourier
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Ejercicio_02_01

  clear all;

  f = magic(4);

  % transformada discreta de Fourier (implementaci�n propia)
  [F,Fc] = mifft2(f); % Transformada de Fourier y su traslaci�n al centro

  % transformada discreta de Fourier mediante la funci�n de Matlab
  F = fft2(f); 

  % traslaci�n al centro
  Fc = fftshift(F);

  disp('Transformada discreta de Fourier (sin trasladar al centro):'); disp(F);
  disp('Transformada discreta de Fourier (trasladada al centro):'); disp(Fc);

end


function [F,Fc]=mifft2(f)
  % function [F,Fc]=mifft2(f)
  % Calcula la transformada de 2D de Fourier de la imagen f
  % de dimensi�n: M columnas y N filas
  % para las frecuencias u=0,1,2,..M-1,v=0,1,2,..N-1 
  % Fc es la transformada desplazada al centro
  % Equivale a los comandos:
  % F=fft2(f), Fc=fftshift(F)

  j=sqrt(-1);   

  % Para entender el c�digo hay que tener en cuenta que  
  % en Matlab la notaci�n de filas y columnas es distinta a la utilizada en
  % el libro. En una matriz f(i,k)las filas corresponden a la primera
  % coordenada y las columnas a la segunda.

  [N,M]=size(f);

  for u=0:M-1
      for v=0:N-1,
          F(v+1,u+1)=0;
          for x=0:M-1
              for y=0:N-1,
                  F(v+1,u+1)=F(v+1,u+1)+f(y+1,x+1)*exp(-j*2*pi*(u*x/M+v*y/N));
              end
          end
      end
  end

  % Trasladamos al centro
  Fc=fftshift(F);
end
