% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 2.9: Transformaci�n del dominio

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2.7.4 Transformada de Wavelets
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function Ejercicio_02_09
  % transformada discreta de wavelets
  f = [1 2 3 4; 5 6 7 8; 9 10 11 12; 13 14 15 16];

  PBD=[1,1]*sqrt(1/2);
  PAD=[-1,1]*sqrt(1/2);
  [I1,I2,I3,I4]=middwt2(f,PBD,PAD);

  disp('I1 :'); disp(I1);
  disp('I2 :'); disp(I2);
  disp('I3 :'); disp(I3);
  disp('I4 :'); disp(I4);

  % Usando el toolbox de wavelets de Matalab
  [I1,I2,I3,I4] = dwt2(f,'Haar');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%Funci�n auxiliar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [I1,I2,I3,I4]=middwt2(I,PBD,PAD)
  % function [I1,I2,I3,I4]=middwt2(I,PBD,PAD)
  % Calcula la descomposici�n de la matriz o vector de entrada
  % utilizando los filtros que se especifican

  II=conv2(I,PBD);
  II=quitacol(II);

  I1=conv2(PBD,II')';
  I1=quitarow(I1);

  I2=conv2(PAD,II')';
  I2=quitarow(I2);

  II=conv2(I,PAD);
  II=quitacol(II);

  I3=conv2(PBD,II')';
  I3=quitarow(I3);

  I4=conv2(PAD,II')';
  I4=quitarow(I4);
end

function A=quitarow(A)
  nf=size(A,1);
  if nf >= 2,
    A=A([2:2:nf],:);
  end
  return
end

function A=quitacol(A)
  nc=size(A,2);
  if nc >= 2,
    A=A(:,[2:2:nc]);
  end
  return
end
