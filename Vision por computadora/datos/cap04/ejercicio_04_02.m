% EJERCICIOS RESUELTOS DE VISIÓN POR COMPUTADOR
% Autores: Gonzalo Pajares y Jesús Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 4.2: Operaciones aritméticas, lógicas y morfológicas

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4.4.1 Operaciones Aritméticas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
% Operaciones aritméticas
A = [5 2 3; 4 5 6; 7 8 9];
B = [6 4 4; 6 9 8; 5 8 0];

% Multiplicación
Tp = A.*B;
M = max(max(Tp)); 
m = min(min(Tp)); 

P = floor(((Tp - m)./(M-m))*(9-0));

% División
epsilon = 0.2;
Td = A./(B+epsilon);
M = max(max(Td)); 
m = min(min(Td)); 

D = floor(((Td - m)./(M-m))*(9-0));

disp('Multiplicación: ');  disp(P);
disp('División: '); disp(D);


