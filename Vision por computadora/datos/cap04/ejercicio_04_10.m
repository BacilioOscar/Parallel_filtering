% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 4.10: Operaciones aritm�ticas, l�gicas y morfol�gicas

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4.4.3 Operaciones morfol�gicas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
% Operaciones morfologicas: cierre

A = [0 1 0 0 0; 0 0 1 0 0; 1 1 0 0 0; 1 1 0 0 0; 0 0 0 0 0];
B = [1 1];

CierreA = imclose(A,B);

disp('Cierre: ');  disp(CierreA);
