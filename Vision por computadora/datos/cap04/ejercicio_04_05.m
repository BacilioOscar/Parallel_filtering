% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 4.5: Operaciones aritm�ticas, l�gicas y morfol�gicas

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4.4.2 Operaciones L�gicas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
% Operaciones logicas
A = [1 0 1; 0 1 0; 1 1 1];
B = [1 0 1; 1 1 1; 1 0 1];

% Operaciones or, and y xor 
OR = or(A,B);
AND = and(A,B);
XOR = xor(A,B);

disp('Or: ');   disp(OR);
disp('And: ');  disp(AND);
disp('Xor: ');  disp(XOR);
