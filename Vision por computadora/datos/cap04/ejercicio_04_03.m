% EJERCICIOS RESUELTOS DE VISIÓN POR COMPUTADOR
% Autores: Gonzalo Pajares y Jesús Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 4.3: Operaciones aritméticas, lógicas y morfológicas

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4.4.1 Operaciones Aritméticas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
% Operaciones aritméticas
A = [5 2 3; 4 5 6; 7 8 9];
B = [6 4 4; 6 9 8; 5 8 0];

% multiplicación por una constante
K = 3;
Tk = K*A;
M = max(max(Tk)); 
m = min(min(Tk)); 

Pk = floor(((Tk - m)./(M-m))*(9-0));

% División por una constante
Tk = A/K;
M = max(max(Tk)); 
m = min(min(Tk)); 

Dk = floor(((Tk - m)./(M-m))*(9-0));

disp('Multiplicación por una constante: ');  disp(Pk);
disp('División por una constante: '); disp(Dk);

