% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 4.9: Operaciones aritm�ticas, l�gicas y morfol�gicas

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4.4.3 Operaciones morfol�gicas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
% Operaciones morfologicas: apertura

A = [0 1 0 0 0; 0 0 1 0 0; 1 1 0 0 0; 1 1 0 0 0; 0 0 0 0 0];
B = [1 1];

AperturaA = imopen(A,B);

disp('Apertura: ');  disp(AperturaA);
