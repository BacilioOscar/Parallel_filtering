% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 4.13: Operaciones aritm�ticas, l�gicas y morfol�gicas

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4.4.3 Operaciones morfol�gicas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
% Operaciones morfologicas: dilataci�n

X = [0 1 0 0 0; 0 0 1 0 0; 0 1 1 0 0; 0 1 1 0 1; 0 0 0 0 1];
B = [1 0; 1 1];

DilatacionX2 = imdilate(X,B,'full');

disp('Dilataci�n: ');  disp(DilatacionX2);
