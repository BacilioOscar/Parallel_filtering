% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 4.1: Operaciones aritm�ticas, l�gicas y morfol�gicas

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4.4.1 Operaciones Aritm�ticas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
% Operaciones aritm�ticas
A = [5 2 3; 4 5 6; 7 8 9];
B = [6 4 4; 6 9 8; 5 8 0];

% Suma
S = floor((A + B)/2);

% Resta
T = A - B;
M = max(max(T)); 
m = min(min(T)); 

R = round(((T - m)./(M-m))*(9-0));

disp('Suma: ');  disp(S);
disp('Resta: '); disp(R);
