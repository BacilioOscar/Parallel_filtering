% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 4.6: Operaciones aritm�ticas, l�gicas y morfol�gicas

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4.4.2 Operaciones L�gicas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
% Operaciones logicas
A = [1 0 1; 0 1 0; 1 1 1];
B = [1 0 1; 1 1 1; 1 0 1];

% Operaci�n de negaci�n 
NO = ~A;

disp('Negaci�n: ');  disp(NO);
