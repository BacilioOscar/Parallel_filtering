% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 14.8: Fusi�n de im�genes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 14.4.8 Mezcla de coeficientes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; close all;
%imagen de entrada de dimensi�n 8x8
I1=  [2     4     4     2     8     8     6     8;
      6     6     6     5     5     5     6     5;
      2     4     4     2     8     8     6     8;
      2     4     4     2     8     8     6     8;
      6     6     6     5     5     5     6     5;
      2     4     4     2     8     8     6     8;
      6     6     6     5     5     5     6     5;
      2     4     4     2     8     8     6     8];
  
%imagen de entrada de dimensi�n 2x2
I2 = [1 1; 9 9]; 

%nivel de descomposici�n
nivel = 2;

% definir el filtro 
h  = [1 2 1; 2 4 2; 1 2 1]/16;

Y = cell(1,nivel);

% An�lisis
for i = 1:1:nivel 
  
  % Filtrado con el nucleo h 
  X = conv2(I1,h,'same');
 
  % submuestrear la imagen resultante
  [a b] = size(I1);
  Xs2 = I1(1:2:a, 1:2:b);
    
  % sobremuestrear 
  [z s] = size(Xs2);
  XS2   = zeros(2*z, 2*s); 
  XS2(1:2:2*z,1:2:2*s) = Xs2;
  
  %convoluci�n con el n�cleo 4h (interpolaci�n)
  M = conv2(XS2, 4*h, 'same');
 
  % selecconar coeficientes y almacenarlos
  Y(i) = {I1 - M};
  
  % preparacion para el siguinete nivel 
  I1 = Xs2;
end;
disp('Resultado del An�lisis: aproximaci�n:'); x = I1; disp(x);
disp('Resultado del An�lisis: detalles:');             disp(Y{i});

%En este momento se intercambia x por I2
x = I2;

% S�ntesis
for i = nivel:-1:1
  % sobremuestrear 
  [z s] = size(x);
  XS2   = zeros(2*z, 2*s); 
  XS2(1:2:2*z,1:2:2*s) = x;

  % convoluci�n del resulatado anterior con el n�cleo 4*h 
  M = conv2(XS2, 4*h, 'same');;
  
  % ecuaci�n (14.5)
  x  = M + Y{i};

end;
disp('Resultado de la S�ntesis'); disp(x);
