% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 14.5: Fusi�n de im�genes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 14.4.5 Descomposici�n piramidal del gradiente
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; close all;
%imagen de entrada

A =  [2     4     4     2     8     8 ;
      2     4     4     2     8     8 ;
      2     4     4     2     8     8 ;
      2     4     4     2     8     8 ;
      2     4     4     2     8     8 ;
      2     4     4     2     8     8 ];
  
%nivel de descomposici�n
nivel = 1;

% definir el n�cleo del filtro 
h  = [1 2 1]/4;
% definir los n�cleos de derivadas direccionales
d1 = [1 -1];
d2 = [0 -1; 1 0] / sqrt(2);
d3 = [-1 1];
d4 = [-1 0; 0 1] / sqrt(2);

% obtener las derivadas gp
d1 = conv2(d1,d1); 
d1 = [zeros(1,3); d1; zeros(1,3)];
d2 = conv2(d2,d2);
d3 = d1';
d4 = conv2(d4,d4);

Y = cell(1,nivel);

% An�lisis
for i = 1:1:nivel 
  
  % Filtrado con el nucleo h 
  X = conv2(conv2(A,h,'same'),h','same');
  XG = A+conv2(conv2(A,h,'same'),h','same');
 
  % submuestrear la imagen resultante
  [a b] = size(X);
  Xs2 = X(1:2:a, 1:2:b);
    
  % obtener las derivadas direccionales
  B  = zeros(size(A));
  D1 = conv2(XG, d1, 'same');
  D2 = conv2(XG, d2, 'same');
  D3 = conv2(XG, d3, 'same');
  D4 = conv2(XG, d4, 'same');
 
  B = B + D1 + D2 + D3 + D4;
    
  % selecconar coeficientes y almacenarlos
  Y(i) = {-B/8};
  
  % preparacion para el siguinete nivel 
  A = Xs2;
end;
disp('Resultado del An�lisis: aproximaci�n:'); x = A; disp(x);
disp('Resultado del An�lisis: detalles:');            disp(Y{i}); 

% S�ntesis
for i = nivel:-1:1
  % sobremuestrear 
  [z s] = size(x);
  XS2   = zeros(2*z, 2*s); 
  XS2(1:2:2*z,1:2:2*s) = x;

  % convoluci�n del resulatado anterior con el n�cleo 4*h 
  M = conv2(conv2(XS2, 2*h, 'same'),2*h','same');
  
  % ecuaci�n (14.5)
  x  = M + Y{i};

end;
disp('Resultado de la S�ntesis'); disp(x);