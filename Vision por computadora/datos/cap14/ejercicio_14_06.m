% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 14.6: Fusi�n de im�genes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 14.4.7 Medidas de actividad
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; close all;
%imagen de descomposici�n

y =  [0.3  0.1 1.1 4.1;
      0.5  0.3 1.5 5.5;
      0.1 -0.5 0.4 4.9;
      1.1  1.5 3.4 6.5];
  
%actividad basada en coeficientes
ya = abs(y);   % valor absoluto

yc = y.^2;     % cuadrados
disp('Actividad basada en los coeficientes');
disp ('Valor absoluto'); 
ya
disp ('Cuadrado'); 
yc

%actividad basada en ventanas
% coeficientes de la ventana
w = (1/12)*[1 2 1; 2 2 2; 1 2 1];

[m,n] = size(y);

% m�todos de la media ponderada (V1) y filtrado de rango (V2)
V1 = zeros(m,n); 
V2 = V1;
for i=2:1:m-1
    for j=2:1:n-1
        a = 0; v = zeros(3,3); kk = -1; 
        for k=1:1:3
           hh = -1;
           for h=1:1:3
             a = a + w(k,h)*abs(y(i+kk,j+hh)); 
             v(k,h) = abs(y(i+kk,j+hh));     
             hh = hh + 1;
           end
           kk = kk + 1;   
        end
        V1(i,j) = a;
        V2(i,j) = max(max(v));
    end
end
disp('Actividad basada en una vecindad');
disp ('Media ponderada'); 
disp(V1);  %m�todo de la media ponderada
disp ('Filtrado de rango'); 
disp(V2);  %m�todo del filtrado de rango

% m�todo de la frecuencia espacial
V3 = zeros(m,n); 
for i=2:1:m-1
    for j=2:1:n-1
        v = zeros(3,3); kk = -1; 
        for k=1:1:3
           hh = -1;
           for h=1:1:3
             v(k,h) = y(i+kk,j+hh);     
             hh = hh + 1;
           end
           kk = kk + 1;   
        end
        R  = (1/9)*((v(2,1)-v(1,1))^2+(v(3,1)-v(2,1))^2+...   %primera columna
                    (v(2,2)-v(1,2))^2+(v(3,2)-v(2,2))^2+...   %segunda columna                    
                    (v(2,3)-v(1,3))^2+(v(3,3)-v(2,3))^2);     %tercera columna                    

        C  = (1/9)*((v(1,2)-v(1,1))^2+(v(1,3)-v(1,2))^2+...   %primera fila
                    (v(2,2)-v(2,1))^2+(v(2,3)-v(2,2))^2+...   %segunda fila                    
                    (v(3,2)-v(3,1))^2+(v(3,3)-v(3,2))^2);     %tercera fila                    
        V3(i,j) = sqrt(R^2+C^2);
    end
end
disp('Actividad basada en una vecindad: Frecuencia espacial');
disp(V3); %m�todo de la frecuencia espacial
