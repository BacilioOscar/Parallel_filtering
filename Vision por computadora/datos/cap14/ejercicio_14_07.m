% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 14.7: Fusi�n de im�genes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 14.4.8 Mezcla de coeficientes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; close all;
%imagen de entrada
I1=  [2     4     4     2     8     8     6     8;
      6     6     6     5     5     5     6     5;
      2     4     4     2     8     8     6     8;
      2     4     4     2     8     8     6     8;
      6     6     6     5     5     5     6     5;
      2     4     4     2     8     8     6     8;
      6     6     6     5     5     5     6     5;
      2     4     4     2     8     8     6     8];

I2=  [7     7     8     4     7     8     2     2;
      3     6     8     4     7     5     2     4;
      7     5     8     6     7     8     2     3;
      7     5     8     6     6     6     2     4;
      3     4     8     6     6     6     2     4;
      3     2     8     5     4     8     2     2;
      7     2     8     5     5     8     2     4;
      7     2     8     3     5     8     2     4];  

%descomposici�n wavelets al primer nivel  
[A1,H1,V1,D1] = dwt2(I1,'db1');
[A2,H2,V2,D2] = dwt2(I2,'db1');


% fusi�n de los coeficientes de aproximaci�n mediante la media ponderada general
umbral = 0.4;
% obtener las actividades Ax^2 y Ay^2 seg�n la ecuaci�n (14.17) 
% con un n�cleo w = (1/9)*[1 1 1; 1 1 1; 1 1 1];

[m,n] = size(V1);
Ax = zeros(m,n); Ay = Ax; Mxy = Ax; 

w = (1/9)*[1 1 1; 1 1 1; 1 1 1];
A = A1;
for i=2:1:m-1
    for j=2:1:n-1
        a1 = 0; a2 = 0; a = 0; kk = -1;  
        for k=1:1:3
           hh = -1;
           for h=1:1:3
             a1 = a1 + abs(A1(i+kk,j+hh)); 
             a2 = a2 + abs(A2(i+kk,j+hh));
             a  = a  + w(k,h)*A1(i+kk,j+hh)*A2(i+kk,j+hh);
             hh = hh + 1;
           end
           kk = kk + 1;   
        end
        Ax(i,j)  = a1/9; Ay(i,j) = a2/9; 
        M(i,j)   = a;
        Mxy (i,j) = M(i,j)/(Ax(i,j)^2+Ay(i,j)^2+eps);
        if Mxy(i,j) < umbral
            wx = 0.0; wy = 1.0;
        else
            wx = 0.5-0.5*((1-Mxy(i,j))/(1-umbral));
            wy = 1- wx;
        end
        A(i,j) = wx*A1(i,j) + wy*A2(i,j);
    end
end
disp('Mezcla de los coeficinetes de aproximaci�n:')
disp(A);

%Fusi�n de los coeficientes horizontales mediante componentes principales
% obtener los autovalores y autovectores y normalizar 
H = H1;
[v, d] = eig(cov([H1(:) H2(:)]));
if (d(1,1) > d(2,2))
  w = v(:,1)./sum(v(:,1));
else  
  w = v(:,2)./sum(v(:,2));
end;

% fus�n
disp('Mezcla de los coeficinetes de detalle horizontales:')
H = w(1)*H1+w(2)*H2;
disp(H);

% los coeficientes verticales se seleccionan por m�ximos
[m,n] = size(A1);

V = zeros(m,n);
for i=1:1:m
    for j=1:1:n
      ma = max([abs(V1(i,j)),abs(V2(i,j))]);
      if ma == abs(V1(i,j))
          V(i,j) = V1(i,j);
      else
          V(i,j) = V2(i,j);
      end
    end
end
disp('Mezcla de los coeficinetes de detalle verticales:')
disp(V);

% a continuaci�n procedemos a obtener la imagen fusinada por reconstrucci�n
% de los coeficientes fusionados

X = idwt2(A,H,V,D1,'haar');
disp('Imagen resultante fusionada:')
disp(X);



  

