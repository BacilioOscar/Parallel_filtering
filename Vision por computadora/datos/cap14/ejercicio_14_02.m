% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 14.2: Fusi�n de im�genes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 14.4.2 Desomposici�n en pir�mide FSD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; close all;
%imagen de entrada
A =  [2     4     4     2     8     8     8     8;
      2     4     4     2     8     8     8     8;
      2     4     4     2     8     8     8     8;
      2     4     4     2     8     8     8     8;
      2     4     4     2     8     8     8     8;
      2     4     4     2     8     8     8     8;
      2     4     4     2     8     8     8     8;
      2     4     4     2     8     8     8     8];

%nivel de descomposici�n
nivel = 1;

% definir el filtro 
h  = [1 2 1; 2 4 2; 1 2 1]/16;

Y = cell(1,nivel);

% An�lisis
for i = 1:1:nivel 
  
  % Filtrado con el nucleo h 
  X = conv2(A,h,'same');
 
  % submuestrear la imagen resultante
  [a b] = size(X);
  Xs2 = X(1:2:a, 1:2:b);
    
  % select coefficients and store them
  Y(i) = {A - X};
  
  % preparacion para el siguiente nivel 
  A = Xs2;
end;
disp('Resultado del An�lisis: aproximaci�n:'); x = A; disp(x);
disp('Resultado del An�lisis: detalles:');            disp(Y{i}); 


% S�ntesis
for i = nivel:-1:1
  % sobremuestrear 
  [z s] = size(x);
  XS2   = zeros(2*z, 2*s); 
  XS2(1:2:2*z,1:2:2*s) = x;

  % convoluci�n del resulatado anterior con el n�cleo 4*h 
  M = conv2(XS2, 4*h, 'same');;
  
  % ecuaci�n (14.5)
  x  = M + Y{i};

end;
disp('Resultado de la S�ntesis'); disp(x);
