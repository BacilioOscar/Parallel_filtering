% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 14.4: Fusi�n de im�genes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 14.4.4 Descomposici�n piramidal morfol�gica
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; close all;
%imagen de entrada
A =  [2     4     4     2     8     8     8     8;
      2     4     4     2     8     8     8     8;
      2     4     4     2     8     8     8     8;
      2     4     4     2     8     8     8     8;
      2     4     4     2     8     8     8     8;
      2     4     4     2     8     8     8     8;
      2     4     4     2     8     8     8     8;
      2     4     4     2     8     8     8     8];

%nivel de descomposici�n
nivel = 2;

% definir el elemento estructural B 
B = strel('square',3);

Y = cell(1,nivel);

% An�lisis
for i = 1:1:nivel 
  
  % Apertura morfol�gica de la imagen original 
  X = imopen(A,B);
  % Cierre morfol�gico de la imagen resultante anterior 
  X = imclose(X,B);
  
  % submuestrear la imagen resultante
  [a b] = size(X);
  Xs2 = X(1:2:a, 1:2:b);
    
  % sobremuestrear 
  [z s] = size(Xs2);
  XS2   = zeros(2*z, 2*s); 
  XS2(1:2:2*z,1:2:2*s) = Xs2;
  
  %dilatar con elelemento estructural B
  M = imdilate(XS2,B);
   
  % selecconar coeficientes y almacenarlos
  Y(i) = {A - M};
  
  % preparacion para el siguinete nivel 
  A = Xs2;
end;
disp('Resultado del An�lisis: aproximaci�n:'); x = A; disp(x);
disp('Resultado del An�lisis: detalles:');            disp(Y{i}); 

% S�ntesis
for i = nivel:-1:1
  % sobremuestrear 
  [z s] = size(x);
  XS2   = zeros(2*z, 2*s); 
  XS2(1:2:2*z,1:2:2*s) = x;

  %dilatar
  M = imdilate(XS2,B);
  
  % ecuaci�n (14.11)
  x  = M + Y{i};

end;
disp('Resultado de la S�ntesis'); disp(x);