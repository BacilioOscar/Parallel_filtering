% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 9.4: Clasificaci�n: redes neuronales y m�quinas de vectores
% soporte

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 9.9.1 El perceptr�n simple y combinaci�n de perceptrones 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% El perceptr�n multicapa: funci�n XOR

 % topolog�a del perceptr�n multicapa para resolver el XOR
 
 % numero de capas ocultas 
 NcapasOcultas = 1;
 
 % N�mero neuronas entrada/salida
 NumeroEntradas = 2; 
 NumeroSalidas  = 1; 
 
 % n�mero de neuronas en la capa oculta
 neuronasCapaOculta = 2;  %numero de neuronas en la capa oculta
     
 % valores de los bias de las neuronas de la capa oculta
 biasOculta = [-1.5 -0.5];

 % valor del bias de las neuronas de la capa de salida
 biasSalida = -0.5;

%pesos de conexi�n entre la capa de entrada y la oculta
W1 = [1 1; 1 1];
     
%pesos de conexi�n entre la capa de oculta y la de salida
W2 = [-2 1];

% Ejecuci�n de la red para las entradas dadas

% Entradas:  x1  x2
%            0   0
%            0   1
%            1   0
%            1   1
X = [0 0; 0 1; 1 0; 1 1];

[nentradas, dim] = size(X);

for i=1:nentradas
  x = X(i,:);
  a = W1(1,:)*x' + biasOculta(1);  % neurona 1
  b = W1(2,:)*x' + biasOculta(2);  % neurona 2
  if a <= 0
    y1 = 0;
  else
    y1 = 1;
  end
  s1 = 'Entrada '; s2 = num2str(i); s3 = '  y1 ='; 
  disp([s1 s2 s3]); disp(y1);
  
  if b <= 0
    y2 = 0;
  else
    y2 = 1;
  end
  s3 = '  y2 ='; 
  disp([s1 s2 s3]); disp(y2);

  % Salida de la capa final
  y = [y1 y2];
  c = W2(1,:)*y' + biasSalida;
  if c <= 0
    y3 = 0;
  else
    y3 = +1;
  end
  disp('Salida y3= '); disp(y3);
end   
 