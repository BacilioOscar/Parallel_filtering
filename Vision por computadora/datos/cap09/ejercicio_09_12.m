% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 9.12: Clasificaci�n: redes neuronales y m�quinas de vectores
% soporte
%
%  adaptado de Steve Gunn: http://www.isis.ecs.soton.ac.uk/resources/svminfo/
%  Southampton University 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 9.9.5 M�quinas de vectores soporte 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Ejercicio_09_12
  % patrones para la funci�n XOR
  X = [-1 -1; 1 1; -1 1; 1 -1];
  Y = [-1;-1;1;1];

  d = 2; %grado del polinomio
  %entrenamiento
  [nsv alpha bias] = svc(X,Y,'poly',2000,d,0);
  
  disp('Vectores soporte w:'); disp(alpha);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [nsv, alpha, b0] = svc(X,Y,ker,C,p1,p2)
%Clasificaci�n de M�quinas de Vectores Soporte (SVM)
%
%  Parameters: X      - Entradas de entrenamiento
%              Y      - Objetivos de entrenamiento
%              ker    - Funci�n del n�cleo
%              C      - L�mite superior
%              p1     - grado del polinomio
%              nsv    - n�mero de vectores soporte
%              alpha  - Multiplicadores de LAgrange
%              b0     - t�rmino bias
%
%  Tomado de: Steve Gunn (srg@ecs.soton.ac.uk)

    fprintf('Clasificaci�n de Vectores soporte\n')
    fprintf('_____________________________\n')
    n = size(X,1);
    if (nargin<4) C=Inf;, end
    if (nargin<3) ker='linear';, end

    % tolerancia
    epsilon = svtol(C);
    
    % Construir la matriz Kernel
    fprintf('Generando la matriz n�cleo ...\n');
    H = zeros(n,n);  
    for i=1:n
       for j=1:n
          H(i,j) = Y(i)*Y(j)*svkernel(ker,X(i,:),X(j,:),p1,p2);
          H1(i,j) = svkernel(ker,X(i,:),X(j,:),p1,p2);
       end
     end
    c = -ones(n,1);  

    % Se a�ade un �psilon para evitar problemas con el Hessiano. 
    H = H+1e-10*eye(size(H));
    
    % Asignar los par�metros para el problema de optimizaci�n

    vlb = zeros(n,1);      % Fijar l�mites:  alphas >= 0
    vub = C*ones(n,1);     %                 alphas <= C
    x0 = zeros(n,1);       % Punto inicial [0 0 0   0]
    neqcstr = nobias(ker); % N�mero de restricciones de igualdad (1 or 0)  
    if neqcstr
       A = Y';, b = 0;     % Restricci�n Ax = b
    else
       A = [];, b = [];
    end

    % Resolver el problema de optimizaci�n
    fprintf('Optimizando ...\n');
    st = cputime;
    
    [alpha lambda how] = qp(H, c, A, b, vlb, vub, x0, neqcstr);
    
    fprintf('tiempo de ejecuci�n: %4.1f segundos\n',cputime - st);
    fprintf('Estado : %s\n',how);
    w2 = alpha'*H*alpha;
    fprintf('|w0|^2    : %f\n',w2);
    fprintf('Margen    : %f\n',2/sqrt(w2));
    fprintf('Suma alfas : %f\n',sum(alpha));
    
        
    % Obtener el n�mero de vectores soporte
    svi = find( alpha > epsilon);
    nsv = length(svi);
    fprintf('Vectores soporte : %d (%3.1f%%)\n',nsv,100*nsv/n);

    % bias impl�cito, b0
    b0 = 0;

    if nobias(ker) ~= 0
      % b�squeda de b0 a partir de los vectores soporte con: 0 < alpha < C
      svii = find( alpha > epsilon & alpha < (C - epsilon));
      if length(svii) > 0
        b0 =  (1/length(svii))*sum(Y(svii) - H(svii,svi)*alpha(svi).*Y(svii));
      else 
        fprintf('No existen vectores soporte en ese margen.\n');
      end
    end
    
end
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function k = svkernel(ker,u,v,p1,p2)
%
%  Par�metros: ker - tipo del n�cleo
%              u,v - argumentos del n�cleo
%
%  Values for ker: 'linear'  -
%                  'poly'    - p1 es el grado del polinomio
%                  'rbf'     - p1 es el ancho de la rbfs (sigma)
%                  'sigmoid' - p1 es la escala, p2 is el offset
%                  'spline'  -
%                  'bspline' - p1 grado del bspline
%                  'fourier' - p1 grado
%                  'erfb'    - p1 ancho de la rbfs (sigma)
%                  'anova'   - p1 es el m�ximo orden de t�rminos
%              
    switch lower(ker)
      case 'linear'
        k = u*v';
      case 'poly'
        k = (u*v' + 1)^p1;
      case 'rbf'
        k = exp(-(u-v)*(u-v)'/(2*p1^2));
      case 'erbf'
        k = exp(-sqrt((u-v)*(u-v)')/(2*p1^2));
      case 'sigmoid'
        k = tanh(p1*u*v'/length(u) + p2);
      case 'fourier'
        z = sin(p1 + 1/2)*2*ones(length(u),1);
        i = find(u-v);
        z(i) = sin(p1 + 1/2)*(u(i)-v(i))./sin((u(i)-v(i))/2);
        k = prod(z);
      case 'spline'
        z = 1 + u.*v + (1/2)*u.*v.*min(u,v) - (1/6)*(min(u,v)).^3;
        k = prod(z);
      case 'bspline'
        z = 0;
        for r = 0: 2*(p1+1)
          z = z + (-1)^r*binomial(2*(p1+1),r)*(max(0,u-v + p1+1 - r)).^(2*p1 + 1);
        end
        k = prod(z);
      case 'anovaspline1'
        z = 1 + u.*v + u.*v.*min(u,v) - ((u+v)/2).*(min(u,v)).^2 + (1/3)*(min(u,v)).^3;
        k = prod(z); 
      case 'anovaspline2'
        z = 1 + u.*v + (u.*v).^2 + (u.*v).^2.*min(u,v) - u.*v.*(u+v).*(min(u,v)).^2 + (1/3)*(u.^2 + 4*u.*v + v.^2).*(min(u,v)).^3 - (1/2)*(u+v).*(min(u,v)).^4 + (1/5)*(min(u,v)).^5;
        k = prod(z);
      case 'anovaspline3'
        z = 1 + u.*v + (u.*v).^2 + (u.*v).^3 + (u.*v).^3.*min(u,v) - (3/2)*(u.*v).^2.*(u+v).*(min(u,v)).^2 + u.*v.*(u.^2 + 3*u.*v + v.^2).*(min(u,v)).^3 - (1/4)*(u.^3 + 9*u.^2.*v + 9*u.*v.^2 + v.^3).*(min(u,v)).^4 + (3/5)*(u.^2 + 3*u.*v + v.^2).*(min(u,v)).^5 - (1/2)*(u+v).*(min(u,v)).^6 + (1/7)*(min(u,v)).^7;
        k = prod(z);
      case 'anovabspline'
        z = 0;
        for r = 0: 2*(p1+1)
          z = z + (-1)^r*binomial(2*(p1+1),r)*(max(0,u-v + p1+1 - r)).^(2*p1 + 1);
        end
        k = prod(1 + z);
      otherwise
        k = u*v';
    end
end

function tol = svtol(C)
%SVTOL Tolerance for Support Vectors
%
%  Usage: tol = svtol(C)
%
%  Parameters: C      - upper bound 
%
%  Author: Steve Gunn (srg@ecs.soton.ac.uk)


  if (nargin ~= 1) % check correct number of arguments
    help svtol
  else

    % tolerance for Support Vector Detection
    if C==Inf 
      tol = 1e-5;
    else
      tol = C*1e-6;
    end
    
  end
end

function nb = nobias(ker)
%NOBIAS returns true if SVM kernel has no implicit bias
%
%  Usage: nb = nobias(ker)
%
%  Parameters: ker - kernel type
%              
%  Author: Steve Gunn (srg@ecs.soton.ac.uk)

  if (nargin ~= 1) % check correct number of arguments
     help nobias
  else
     
    switch lower(ker)
      case {'linear','sigmoid'}
%,'anovaspline1','anovaspline2','anovaspline3'}
        nb = 1;
      otherwise
        nb = 0;
    end

  end
end
