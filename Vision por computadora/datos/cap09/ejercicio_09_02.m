% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 9.2: Clasificaci�n: redes neuronales y m�quinas de vectores
% soporte

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 9.9.1 El perceptr�n simple y combinaci�n de perceptrones 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% El perceptr�n: funci�n OR

clear all; close all

% datos 2-dimensionales con 2 clases. 
% Se utiliza bias para todos los datos de 0.5
% clase c1: 
datos(1).X = [0, 1, 1; 1 0 1; 1 1 1];

% clase c2: 
datos(2).X = [0, 0, 1];

c = 2;      % numero de clases
e = 0.0;    % tolerancia en la convergencia 
alfa = 1.0; %raz�n de aprendizaje

% clase 1: fd = +1
% clase 2: fd =  0

% n1: numero de muestras de la clase 1
% n2: numero de muestras de la clase 2
[n1, p] = size(datos(1).X);
[n2, p] = size(datos(2).X);

%inicializaci�n de los pesos
w = zeros(p,1);
w(1) = 0.5; w(2) = 1.5; w(3) = 1.5;

dif = 1000;
iteraciones = 0;

while dif > e
  w_anterior = w;
 % entrenamiento para las clases
 for i=1:1:c 
   if i==1
      n=n1; fdi = +1;
   else
      n=n2; fdi = -1;
   end
   for j=1:1:n
     Salida = datos(i).X(j,:)*w;
     if Salida > 0
        O = +1; 
     else
        O = -1; 
     end
     error = fdi - O;
     
     w = w + alfa*error*datos(i).X(j,:)';
   end
 end
 dif = norm(w - w_anterior);
 iteraciones = iteraciones + 1;
end

disp('Pesos aprendidos:'); disp(w);
disp('Numero de iteraciones: '); disp(iteraciones);

%Representaci�n gr�fica: se toman dos valores representativos de x: 
%   x1 = 0
%   x2 = 0.5
x(1) = 0.0; x(2) = 0.5;

y = -(w(1)*x + w(3))./(w(2));

% representaci�n de la recta de separaci�n
plot(x,y,'b','LineWidth',3);
    
% representaci�n de los datos de la clase 1
for i=1:1:n1
   hold on
   plot (datos(1).X(i,1),datos(1).X(i,2),'g*','LineWidth',3);
end

% representaci�n de los datos de la clase 2
for i=1:1:n2
  hold on
  plot (datos(2).X(i,1),datos(2).X(i,2),'bo','LineWidth',3);
end