% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 9.9: Clasificaci�n: redes neuronales y m�quinas de vectores
% soporte

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 9.9.3 Mapas Autoorganizativos 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; close all;

X = [1,3;1,5;2,2;6,3;6,4;7,3];    

[N,n] = size(X);

c = 2; %n�mero de agrupamientos

% proporcionamos los centros iniciales
v = zeros(c,n);

if n == 2
  v(1,:) = [1,4];  v(2,:) = [7,2]; 
  A = [6,2]; 
else
  v(1,:) = [205, 165, 125];  v(2,:) = [85, 131, 58]; 
  A = [120,110,70];
end

ai = 1; af = 0.8; km = 5;
T = 0.2; 
for k = 1:1:km   %numero de iteraciones
  g = 1/(10+k); %razon de aprendizaje variable
  a(k) = ai*(af/ai).^(k/km);

  for j=1:1:N    %numero de patrones
    for h=1:1:c  %numero de clases
       d = exp(-(sum((v(h,:)-X(j,:)).^2))/(2*a(k).^2));
       if d > T  % se considera que c1 pertenece a la region definida por x
         v(h,:) = v(h,:) + g*d*(X(j,:)-v(h,:));    
       end
    end
  end
  c1 = 'Iteraci�n #'; c2 = num2str(k);
  disp([c1,c2]);
  disp('centro c1: '); disp(v(1,:));
  disp('centro c2: '); disp(v(2,:));
end

dAc1 = norm (A-v(1,:));
dAc2 = norm (A-v(2,:));

if dAc1 < dAc2 
  disp('A pertenece a la clase 1: '); disp(dAc1);
else
  disp('A pertenece a la clase 2: '); disp(dAc2);  
end



