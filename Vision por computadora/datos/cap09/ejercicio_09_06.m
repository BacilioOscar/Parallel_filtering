% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 9.6: Clasificaci�n: redes neuronales y m�quinas de vectores
% soporte

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 9.9.1 El perceptr�n simple y combinaci�n de perceptrones 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Clasificaci�n de texturas naturales: retropropagaci�n

%creaci�n de una red neuronal del tipo backpropagation
% primer par�metro:  valores m�ximo y m�nimo de los dos vectores de entrada
% segundo par�metro: 2 neuronas en la capa oculta y una neurona en la capa
% de salida
% tercer par�metro: funciones de activaci�n (tangente hiperb�lica, log-sigmoid)
% en la capa oculta y en la de salida respectivamente
% cuarto par�metro: steepest descent

%patrones de entrada
p = [208 210 212  90  92  87 160 154 148;...
     165 170 172 130 138 128 143 146 137;...
     128 130 133  60  59  62 113 115 110]./255;

%objetivo
t = [1 1 1 0 0 0 0 0 0; 0 0 0 1 1 1 0 0 0; 0 0 0 0 0 0 1 1 1];

red = newff(minmax(p),[3 2 3],{'logsig','logsig','logsig'},'traingd');
%red = newff(minmax(p),[3 3],{'logsig','logsig'},'traingd');

%asignar valores a los pesos y bias iniciales
% entre la capa de entrada y la primera capa oculta
red.IW{1,1}(1,1)=-2.8895; red.IW{1,1}(1,2)=22.0190; red.IW{1,1}(1,3)=-24.0699;
red.IW{1,1}(2,1)=12.3436; red.IW{1,1}(2,2)=-13.8256; red.IW{1,1}(2,3)=-16.5004;
red.IW{1,1}(3,1)=9.3127; red.IW{1,1}(3,2)=-37.6128; red.IW{1,1}(3,3)=-5.1931;

% entre las dos capas ocultas
red.LW{2,1}(1,1)=5.2559; red.LW{2,1}(1,2)= 4.6647; red.LW{2,1}(1,3)= -0.6297;
red.LW{2,1}(2,1)=-1.0789;red.LW{2,1}(2,2)= -4.3410; red.LW{2,1}(2,3)=-5.4565;

% entre la segunda capa oculta y la de salida
red.LW{3,2}(1,1)= -3.8562; red.LW{3,2}(1,2)= -8.9000;
red.LW{3,2}(2,1)= 6.4517;  red.LW{3,2}(2,2)= -7.2426;
red.LW{3,2}(3,1)= -9.0344; red.LW{3,2}(3,2)=  3.5299;
 
% bias
red.b{1}(1)= 1.8415;  red.b{1}(2)= 7.1078; red.b{1}(3)= 22.6587;
red.b{2}(1)= -8.1733; red.b{2}(2)= 1.9104;
red.b{3}(1)= 11.2278; red.b{3}(2)= 0.3954; red.b{3}(3)= -2.0975;

% dejamos que la inicializaci�n de los pesos y los bias se haga de forma
% aleatoria
A = [209; 169; 131]./255;
B = [89; 133; 60]./255;
C = [152; 140; 111]./255;

salidaA = sim(red,A)
salidaB = sim(red,B)
salidaC = sim(red,C)

%parametros para el aprendizaje
red.trainParam.show = 20000; %mostrar resultados cada valor de ese intervalo de entrenamiento
red.trainParam.lr = 0.2; %raz�n de aprendizaje
red.trainParam.epochs = 1500000; %n�mero m�ximo de iteraciones
red.trainParam.goal = 1e-1;  %tolerancia

%entrenar la red
[red,tr] = train(red,p,t);

%Generar salida
salidaA = sim(red,A)
salidaB = sim(red,B)
salidaC = sim(red,C)





