% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 9.7: Clasificaci�n: redes neuronales y m�quinas de vectores
% soporte

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 9.9.2 Algoritmo de Lloyd 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


X = [1,1;1,3;1,5; 2,2; 2,3;6,3;6,4;7,1;7,3;7,5];    

[N,n] = size(X);

c = 2; %n�mero de agrupamientos

% inicializaci�n de los centros iniciales
v = zeros(c,n);
v(1,:) = [1,4];  v(2,:) = [7,2]; 

gamma = 0.1;

iteraciones = 2;
for i=1:1:iteraciones
  for j=1:1:N
    for h=1:1:c
       d(h) = norm(v(h,:)-X(j,:));
    end
    m = min(d);
    
    for h=1:1:c
       if m == d(h)
          %se actualiza el centro correspondiente
          f = X(j,:) - v(h,:);
          v(h,:) = v(h,:) + gamma*(f);    
       end
    end
  end
  c1 = 'Iteraci�n #'; c2 = num2str(i);
  disp([c1,c2]);
  disp('centro c1: '); disp(v(1,:));
  disp('centro c2: '); disp(v(2,:));
end

% Dato a clasificar
A = [6,2]; 
dAc1 = norm (A-v(1,:));
dAc2 = norm (A-v(2,:));

if dAc1 < dAc2 
  disp('A pertenece a la clase 1: '); disp(dAc1);
else
  disp('A pertenece a la clase 2: '); disp(dAc2);  
end



