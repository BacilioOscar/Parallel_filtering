% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 9.3: Clasificaci�n: redes neuronales y m�quinas de vectores
% soporte

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 9.9.1 El perceptr�n simple y combinaci�n de perceptrones 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% El perceptr�n en clasificaci�n de im�genes

clear all; close all

% datos 3-dimensionales con 2 clases
% clase c1: 
datos(1).X = [200,160,120;210,170,130;215,172,133];

% clase c2: 
datos(2).X = [90,130,60;92,138,54;87,128,66];

c = 2;   % numero de clases
e = 0.0001; % tolerancia en la convergencia 
alfa = 0.0001; %raz�n de aprendizaje

% clase 1: fd = +1
% clase 2: fd = -1

[n1, p] = size(datos(1).X);
[n2, p] = size(datos(2).X);

%inicializaci�n de los pesos
w = zeros(p,1);
dif = 1000;
iteraciones = 0;

while dif > e
  w_a = w;
 % entrenamiento para las clases
 for i=1:1:c 
   if i==1
      n=n1; fdi = +1;
   else
      n=n2; fdi = -1;
   end
   for j=1:1:n
     Salida = datos(i).X(j,:)*w;
     if Salida > 0
        O = +1; 
     else
        O = -1; 
     end
     error = fdi - O;
     
     w = w + alfa*error*datos(i).X(j,:)';
   end
 end
 dif = abs(w - w_a);
 iteraciones = iteraciones + 1;
end
disp('Pesos aprendidos:'); disp(w);

%realizado el entrenamiento procedemos a clasificar las muestras
A = [208 170 135];
B = [89 130 60];

fdA = w'*A'; fdB = w'*B';

if fdA > 0 
  disp('A pertenece a la clase 1: ');
else
  disp('A pertenece a la clase 2: ');
end
disp(fdA);

if fdB > 0 
  disp('B pertenece a la clase 1: ');
else
  disp('B pertenece a la clase 2: ');
end
disp(fdB);




