% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 9.5: Clasificaci�n: redes neuronales y m�quinas de vectores
% soporte

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 9.9.1 El perceptr�n simple y combinaci�n de perceptrones 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% problema del XOR

%creaci�n de una red neuronal del tipo backpropagation
% primer par�metro:  valores m�ximo y m�nimo de los dos vectores de entrada
% segundo par�metro: 2 neuronas en la capa oculta y una neurona en la capa
% de salida
% tercer par�metro: funciones de activaci�n (tangente hiperb�lica, log-sigmoid)
% en la capa oculta y en la de salida respectivamente
% cuarto par�metro: steepest descent
red = newff([0 1; 0 1],[2 1],{'logsig','logsig'},'traingd');

% inicializaci�n de los pesos 
% pesos de entrada
red.IW{1,1}(1,1) = 11;
red.IW{1,1}(1,2) = -9;
red.IW{1,1}(2,1) = 16;
red.IW{1,1}(2,2) = -17;

%pesos de la capa oculta
red.LW{2,1}(1,1) = 26;
red.LW{2,1}(1,2) = -25;

% inicializaci�n de los bias
red.b{1}(1) = 1; %capa oculta
red.b{1}(2) = 1; %capa oculta
red.b{2}(1) = 1; %capa de salida

%parametros para el aprendizaje
red.trainParam.show = 10000; %mostrar resultados cada valor de ese intervalo de entrenamiento
red.trainParam.lr = 0.05; %raz�n de aprendizaje
red.trainParam.epochs = 1200000; %n�mero m�ximo de iteraciones
red.trainParam.goal = 1e-5;  %tolerancia

%patrones de entrada
p = [1 1 0 0; 1 0 1 0];

%objetivo
t = [0 1 1 0];

%simular la red antes del entrenamiento
salida = sim(red,p);

%entrenar la red
[red,tr] = train(red,p,t);

%simular de nuevo la red despu�s del entrenamiento
salida = sim(red,p)





