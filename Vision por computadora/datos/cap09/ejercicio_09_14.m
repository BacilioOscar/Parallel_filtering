% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 9.14: Clasificaci�n: redes neuronales y m�quinas de vectores
% soporte

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 9.9.6 La red neuronal de Hopfield 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Ejercicio_09_14

   clear all; close all

   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   %%%%%    FASE DE ENTRENAMIENTO         %%%%%%%%%%%%%%%%%%%%%
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   % patrones de entrada
   letras(1).matriz = [1 0; 1 0];
   letras(2).matriz = [0 1; 0 1];
   letras(3).matriz = [1 0; 0 1];
      
   numeroPatrones = size(letras,2);
   
   [M,N] = size(letras(1).matriz);
 
   %paso de matrices a vectores
   for i=1:1:numeroPatrones
      letras(i).vector = reshape(letras(i).matriz',1,M*N);
   end
   
   % Se normalizan al rango -1/+1 cuando son 0/+1 
   for i=1:1:numeroPatrones
     Minimo = min(letras(i).vector);
     if Minimo == 0
       letras(i).vector =  2*letras(i).vector - 1;
     end
   end
   
   % aprendizaje: alamacenar pesos en la memoria
   T = AprenderPesos(letras,numeroPatrones);
   
   
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   %%%%%    FASE DE CLASIFICACI�N         %%%%%%%%%%%%%%%%%%%%%
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
   % M�ximo n�mero de iteraciones por si no se consigue la convergencia
   MaxNIter = 10; 
   
   % patr�n de entrada
   C = [1 0; 0 1]; %muestra C
   disp('Patron C: '); disp(C);
   
   % Cambio al rango -1/+1
   C = 2*C -1;
   
   [M,N] = size(C);
   Entrada = reshape(C,1,M*N);
   [Salida, Energia] = EjecutarHopfield(T,Entrada',MaxNIter);

   Salida = reshape(Salida,M,N);
   
   % Cambio al rango 0/+1
   Salida = (Salida + 1)./2;
   disp('Salida para C: '); disp(Salida);

   % patr�n de entrada
   D = [0 1; 1 0]; %muestra D
   disp('Patron D: '); disp(D);

   % Cambio al rango -1/+1
   D = 2*D -1;

   [M,N] = size(D);
   Entrada = reshape(D,1,M*N);
   [Salida, Energia] = EjecutarHopfield(T,Entrada',MaxNIter);
 
   Salida = reshape(Salida,M,N);

   % Cambio al rango 0/+1
   Salida = (Salida + 1)./2;
   disp('Salida para D: '); disp(Salida);
end
   
   
% Entrenamiento de Hopfield: almacenar la informaci�n de los patrones 
% en la memoria. 
% Obtener los pesos

function T = AprenderPesos(patrones,numeroPatrones)
   T = zeros(size(patrones(1).vector,2),size(patrones(1).vector,2));

   for i=1:1:numeroPatrones
      T = CrearPesos(T,patrones(i).vector);
   end;
end

% Actualizar los pesos para cada patr�n (X)
function T = CrearPesos(T,X)
  a = size(X,2); 
  I = eye(a); % matriz identidad
  T = T + X'*X - I;
end


function [Salida,Energia] = EjecutarHopfield(T,X,MaxNumeroIter)
%
%   EjecutarHopfield: permite calcular la salida de una memoria de Hopfield
%
%   Energia: devuelve la secuencia de valores de la energia del 
%   sistema durante el proceso de resonancia

   figure;
   NEURONAS = length(T);
   cambios=1;
   Energia=-0.5*X'*T*X;
   iteraciones = 0;
   while (cambios~=0) & (iteraciones <= MaxNumeroIter)
         iteraciones = iteraciones + 1;
         XPrevia = X;
         X = hardlims(T*X); % funci�n de transferencia escal�n
         cambios=sum((XPrevia-X).^2);
         Energia = [Energia -0.5*X'*T*X];
   end

   x = length(Energia);
   k = 1:1:x;
   plot(k,Energia,'ko-','linewidth',3);
   title('Variaci�n de la energ�a');
   xlabel('Iteraciones');
   ylabel('Energ�a');
   
   if iteraciones == MaxNumeroIter
       disp ('Convergencia no lograda');
   else
       disp('Convergencia lograda');
   end
   Salida = X;
end



