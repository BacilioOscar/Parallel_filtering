% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 9.10: Clasificaci�n: redes neuronales y m�quinas de vectores
% soporte

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 9.9.4 Cuantizaci�n vectorial 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; close all;

X = [200,160,120;90,130,60;210,170,130;35,23,44;215,172,133;92,138,54;87,128,66;41,22,37];

[N,n] = size(X);

nc = 0; %numero de centroides, inicialmente cero

T = 20; %umbral

% inicializaci�n
clase(1).centro = X(1,:);
clase(1).patrones(1,:) = X(1,:);
clase(1).np = 1; %numero de patrones
nc = 1;  %numero de clases

for j=2:1:N
  % se computa la distancia del nuevo patron a los centros
  nueva_clase = false;
  for h=1:1:nc
     d(h) = norm(X(j,:)-clase(h).centro);
  end

  %calcular la distancia m�nima a los centros de cada clase, con indicaci�n de la clase (h) 
  dmin = min(d);
  h = find(d == dmin);

  % si esa distancia m�nima es inferior a un umbral determinado, a�adir el
  % patr�n a la clase, de otro modo crear una nueva clase
  if dmin < T
       clase(h).np = clase(h).np + 1;
       clase(h).patrones(clase(h).np,:) = X(j,:);
  else % hay que crear una nueva clase 
       nueva_clase = true;
  end

 if nueva_clase
   nc = nc + 1;
   clase(nc).np = 1;
   clase(nc).patrones(1,:) = X(j,:);
   clase(nc).centro = X(j,:);
 end

 %para todas las clases existentes calcular los centros
 for h=1:1:nc
   if clase(h).np ~= 1
     clase(h).centro = mean(clase(h).patrones);
   end
 end
  
end

disp('Numero de clases: '); disp(nc);
for h=1:1:nc
  if clase(h).np ~= 1
    clase(h).centro = mean(clase(h).patrones);
    c1 = 'Clase #'; c2 = num2str(h);
    disp([c1,c2]);
    disp('centro: '); disp(clase(h).centro);
  end
end


%%%%%%%%%%% Clasificaci�n de un nuevo patr�n %%%%%
A = [93, 120, 70];

%calculo de las distancias de un nuevo patr�n a los centros
disp('Pertenecia de A a las clases:'); c1 = 'clase #'; 
for h=1:1:nc
  dAc(h) = norm (A-clase(h).centro);
  c2 = num2str(h);
  disp([c1,c2]); disp(dAc(h));
end