% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 8.14: Descripci�n de contornos, regiones y superficies 3D

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8.5.7 Superficies 3D: curvatura de superficies 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Curvatura normal de una superficie

   f = [2 2 2 8 8 9 9; 2 2 6 6 8 9 9; 2 6 6 6 8 9 9; 2 6 6 8 9 9 9; 2 2 6 6 8 9 9; 2 2 6 6 9 9 9; 2 2 6 6 9 9 9];
   [m,n] = size(f);
   
    % M�scaras de derivaci�n: como se utiliza la convoluci�n en realidad 
    % las matrices definidas son la reflexi�n de las definidas en el propio
    % libro
    d00  = [0 0 0 0 0; 0 1 0 -1 0; 0 1 0 -1 0; 0 1 0 -1 0; 0 0 0 0 0]; 
    d45  = [0 0 -1 0 0; 0 0 0 -1 0; 1 0 0 0 -1; 0 1 0 0 0; 0 0 1 0 0]; 
    d90  = -d00'; 
    d135 = [0 0 -1 0 0; 0 -1 0 0 0; -1 0 0 0 1; 0 0 0 1 0; 0 0 1 0 0]; 
    

    % primeras derivadas  
    fp00 = conv2(f, d00, 'valid');    
    fp45 = conv2(f, d45, 'valid');
    fp90 = conv2(f, d90, 'valid');
    fp135 = conv2(f, d135, 'valid');
    
    f00 = zeros(m,n); f45 = f00; f90 = f00; f135 = f00;
    f00 (3:1:m-2,3:1:n-2)  = fp00; 
    f45 (3:1:m-2,3:1:n-2)  = fp45; 
    f90 (3:1:m-2,3:1:n-2)  = fp90; 
    f135(3:1:m-2,3:1:n-2)  = fp135; 
    
    % Calculo de las segundas derivadas
    ff00 = zeros(m,n); ff45 = ff00; ff90 = ff00; ff135 = ff00;
    
    fs00  = conv2(f00, d00,'valid');
    fs45  = conv2(f45, d45,'valid');
    fs90  = conv2(f90, d90,'valid');
    fs135 = conv2(f135,d135,'valid');

    ff00 (3:1:m-2,3:1:n-2) = fs00; 
    ff45 (3:1:m-2,3:1:n-2) = fs45; 
    ff90 (3:1:m-2,3:1:n-2) = fs90; 
    ff135(3:1:m-2,3:1:n-2) = fs135; 
    
    % Derivadas horizontal y vertical
    p = fp00; q = fp90;
    
    % direcci�n a = 0�;
    phi = pi*0/180;
    KN00 = abs((fs00./(1+fp00.^2)^(3/2))*sqrt((1+(p.*cos(phi)-q.*sin(phi))^2)/(1+p.^2+q.^2)));
    
    % direcci�n a = 45�;
    phi = pi*45/180;
    KN45 = abs((fs45./(1+fp45.^2)^(3/2))*sqrt((1+(p.*cos(phi)-q.*sin(phi))^2)/(1+p.^2+q.^2)));
    
    % direcci�n a = 90�;
    phi = pi*90/180;
    KN90 = abs((fs90./(1+fp90.^2)^(3/2))*sqrt((1+(p.*cos(phi)-q.*sin(phi))^2)/(1+p.^2+q.^2)));

    % direcci�n a = 135�;
    phi = pi*135/180;
    KN135 = abs((fs135./(1+fp135.^2)^(3/2))*sqrt((1+(p.*cos(phi)-q.*sin(phi))^2)/(1+p.^2+q.^2)));
    
    
    disp('K0: '); disp(KN00);
    disp('K45: '); disp(KN45);
    disp('K90: '); disp(KN90);
    disp('K135: '); disp(KN135);
    
    