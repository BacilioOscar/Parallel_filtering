% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 8.3: Descripci�n de contornos, regiones y superficies 3D

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8.5.2 Bordes: Ajuste de l�neas mediante m�nimos cuadrados
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all, close all;

PuntosBorde.x = [0 0 1 2 2 3 3];
PuntosBorde.y = [0 1 1 2 3 3 4];

[m,n] = size(PuntosBorde.x);

%ajuste mediante m�nimos cuadrados
% construcci�n de las matrices Y y b
Y = ones(2,n); b = ones(n,1);

for i=1:1:n
    Y(2,i) = PuntosBorde.x(i);
    b(i)   = PuntosBorde.y(i);
end

Y = Y';

% resoluci�n de la ecuaci�n
c = pinv(Y)*b;

figure
for i=1:1:n
    hold on
    plot(PuntosBorde.x(i),PuntosBorde.y(i),'ko','LineWidth',3)
end

x1 = 0:0.1:5;
y1 = c(2)*x1 + c(1);

hold on
plot(x1,y1,'k','LineWidth',3);

