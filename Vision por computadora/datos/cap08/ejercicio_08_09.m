% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 8.9: Descripci�n de contornos, regiones y superficies 3D

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8.5.5 Regiones: texturas 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Matriz de coocurrencia

clear all, close all;
f = [0 1 0 2 0; 0 1 1 2 1; 0 1 0 0 2; 1 0 2 1 2; 0 0 1 2 1];

[m,n] = size(f);

% construcci�n de la matriz A
A = zeros(3);
for i=2:1:m     %dese j = 2 por lo de la derecha
    for j=1:1:n-1 %hasta m-1 porque la �ltima fila no se explora (por encima)
        val1 = f(i,j);
        val2 = f(i-1,j+1); % un pixel por encima y a la derecha del dado
        switch val1 
            case 0
                switch val2
                    case  0
                       A(1,1) = A(1,1) + 1;
                    case  1
                       A(2,1) = A(2,1) + 1;
                    case  2
                       A(3,1) = A(3,1) + 1;
                end
            case 1
                switch val2
                    case  0
                       A(1,2) = A(1,2) + 1;
                    case  1
                       A(2,2) = A(2,2) + 1;
                    case  2
                       A(3,2) = A(3,2) + 1;
                end
            case 2
                switch val2
                    case  0
                       A(1,3) = A(1,3) + 1;
                    case  1
                       A(2,3) = A(2,3) + 1;
                    case  2
                       A(3,3) = A(3,3) + 1;
                end
        end
    end
end

c = sum(sum(A));

%matriz de coocurrencia
C = A./c;

disp('matriz de co-ocurrencia:'); disp(C);