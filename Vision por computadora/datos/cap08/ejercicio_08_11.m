% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 8.11: Descripci�n de contornos, regiones y superficies 3D

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8.5.6 Regiones: momentos invariantes de Hu 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Momentos ordinarios

f = [0 0 0 0 0 0 0; 0 0 1 1 1 0 0; 0 0 1 1 1 0 0;  0 0 1 1 1 0 0;...
     0 0 0 0 0 0 0; 0 0 0 0 0 0 0; 0 0 0 0 0 0 0];

[M,N] = size(f);
[x,y] = meshgrid(1:N,1:M);
x = x(:);
y = y(:);
f = f(:);
m00 = sum(f);
if (m00 == 0)
   m00 = eps;
end

m10 = sum(x.*f);
m01 = sum(y.*f);
m11 = sum(x.*y.*f);
m20 = sum(x.^2.*f);
m02 = sum(y.^2.*f);
m30 = sum(x.^3.*f);
m03 = sum(y.^3.*f);
m12 = sum(x.*y.^2.*f);
m21 = sum(x.^2.*y.*f);

disp('m00: '); disp(m00);
disp('m10: '); disp(m10);
disp('m01: '); disp(m01);
disp('m11: '); disp(m11);
disp('m20: '); disp(m20);
disp('m02: '); disp(m02);
disp('m30: '); disp(m30);
disp('m03: '); disp(m03);
disp('m12: '); disp(m12);
disp('m21: '); disp(m21);
