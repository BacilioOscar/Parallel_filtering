% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 8.5: Descripci�n de contornos, regiones y superficies 3D

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8.5.3 Bordes: la transformada de Hough 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all, close all;
f = [0 0 0 0 1; 1 0 1 0 0; 0 1 0 0 0; 0 0 1 0 0; 1 0 0 1 0];

[m,n] = size(f);

%creamos el acumulador de dimensi�n 7x7
d1 = 7; d2 = 7;
Acumulador = zeros(d1,d2);
idx = [-3 -2 -1 0 1 2 3];
idy = [-3 -2 -1 0 1 2 3];

for y=1:1:m
    for x=1:1:n
       if f(y,x) == 1
           % recorremos el acumulador 
           for a =-3:1:3
              b = -a*x + y;
              [a1,a2] = find(idx == a);
              [b1,b2] = find(idy == b);
              Acumulador(b2,a2) = Acumulador(b2,a2) + 1;
           end
       end
    end
end

disp('Acumulador: '); disp(Acumulador);
