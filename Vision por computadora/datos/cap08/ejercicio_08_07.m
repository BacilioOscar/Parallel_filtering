% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 8.7: Descripci�n de contornos, regiones y superficies 3D

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8.5.4 Regiones: propiedades m�tricas 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all, close all;
f = [0 0 0 0 0; 0 0 6 3 0; 0 4 5 0 0; 0 6 0 0 0; 0 0 0 0 0];

%centro de gravedad
[y,x] = find(f ~= 0);
[a,b] = size(x);

%C�lculo de los valores intermedios
Area = a;

Sx = sum(x); Sy = sum(y); Sxx = sum(x.*x); Syy = sum(y.*y); Sxy = sum(x.*y);

Mxx = Sxx - Sx^2/Area;  Myy = Syy - Sy^2/Area;  Mxy = Sxy - Sx*Sy/Area;

num = Mxx - Myy + sqrt((Mxx-Myy)^2+4*Mxy^2);
den = 2*Mxy;

%calculo en radianes
phir = atan2(num,den);

%calculo en grados
phig = phir*180/pi;

disp('Mxx: '); disp(Mxx);
disp('Myy: '); disp(Myy);
disp('Mxy: '); disp(Mxy);

disp('Orientaci�n en �:'); disp(phig);


