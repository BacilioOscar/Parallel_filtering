% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 8.15: Descripci�n de contornos, regiones y superficies 3D

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8.5.8 Superficies 3D: forma local de superficies 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Curvatura de superficies

   f = [2 2 2 8 8 9 9; 2 2 6 6 8 9 9; 2 6 6 6 8 9 9; 2 6 6 8 9 9 9; 2 2 6 6 8 9 9; 2 2 6 6 9 9 9; 2 2 6 6 9 9 9];
   [m,n] = size(f);
   
    % M�scaras de derivaci�n
    dx = [1 0 -1; 1 0 -1; 1 0 -1]; 
    dy = dx'; %dx resulta ser la matriz traspuesta de dy

    % fx e fy son las primeras derivadas derivadas horizontal y vertical 
    fx = conv2(f, dx, 'valid');    
    fy = conv2(f, dy, 'valid');
    
    Fx = zeros(m,n); Fy = Fx;
    Fx(2:1:m-1,2:1:n-1) = fx; 
    Fy(2:1:m-1,2:1:n-1) = fy; 
    
    % Calculo de las segundas derivadas
   
    fxx = conv2(Fx,dx,'valid');
    fyy = conv2(Fy,dy,'valid');
    fxy = conv2(Fx,dy,'valid');

   
    K = (fxx.*fyy - fxy^2)./(1+ fx.^2 + fy.^2);
    
    H = ((1 + fx.^2)*fyy -2*fx.*fy.*fxy +(1 + fy^2).*fxx)./(1+ fx.^2 + fy.^2).^1.5;
    
    H = H/2;
    
    [m,n] = size(K);
    KPosNeg = zeros(m,n); HPosNeg = KPosNeg;
    for i=1:1:m
        for j=1:1:n
           if K(i,j) > 0
               KPosNeg(i,j) = 1;
           elseif K(i,j) < 0
               KPosNeg(i,j) = -1;
           end

           if H(i,j) > 0
               HPosNeg(i,j) = 1;
           elseif H(i,j) < 0
               HPosNeg(i,j) = -1;
           end
        end
    end
    
    disp('Signo K: '); disp(KPosNeg);
    disp('Signo H: '); disp(HPosNeg);
       