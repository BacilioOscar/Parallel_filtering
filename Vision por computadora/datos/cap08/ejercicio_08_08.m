% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 8.8: Descripci�n de contornos, regiones y superficies 3D

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8.5.5 Regiones: texturas 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all, close all;
f = [1 2 1 3 1; 1 2 2 3 2; 1 2 1 1 3; 2 1 3 2 3; 1 1 2 3 2];

media = mean2(f);

dst = std2(f);

R = 1 - 1/(1 + dst^2);

disp('coeficiente de suavidad:'); disp(R);
