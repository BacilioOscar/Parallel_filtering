% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 8.6: Descripci�n de contornos, regiones y superficies 3D

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8.5.4 Regiones: propiedades m�tricas 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all, close all;
f = [0 0 0 0 0; 0 4 6 3 0; 0 4 5 4 0; 0 6 3 5 6; 0 0 0 0 0];

%obtenci�n del per�metro
Perimetro = bwperim(f);

%centro de gravedad
[y,x] = find(f ~= 0);
[a,b] = size(x);


%Area
Area = a;

xm = sum(x)/Area;
ym = sum(y)/Area;

%centro de gravedad promediado por la intensidad de la imagen
[s1,s2] = size(f);

F = 0; xM = 0; yM = 0;
for i=1:1:s1
    for j=1:1:s2
      if f(i,j) ~= 0
          F = F + f(i,j);
          xM = xM + j*f(i,j);
          yM = yM + i*f(i,j);
      end
    end
end

xM = xM/F;  yM = yM/F;

disp('centro de gravedad');
disp('x:'); disp(xM);
disp('y:'); disp(yM);
