% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 8.13: Descripci�n de contornos, regiones y superficies 3D

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 8.5.6 Regiones: momentos invariantes de Hu 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Momentos de Hu

f = [0 0 0 0 0 0 0; 0 1 1 1 0 0 0; 0 1 1 1 1 0 0;  0 1 1 0 1 0 0;...
     0 0 0 0 0 0 0; 0 0 0 0 0 0 0; 0 0 0 0 0 0 0]; 
 
%f = [0 0 0 0 0 0 0; 0 0 0 0 0 0 0; 0 0 0 0 0 0 0;  0 0 0 0 0 0 0;...
%     0 0 0 0 1 1 0; 0 0 0 0 1 1 1; 0 0 0 0 1 0 1];  


[M,N] = size(f);
[x,y] = meshgrid(1:N,1:M);
x = x(:);
y = y(:);
f = f(:);

% calculo de los momentos de orden p + q
m00 = sum(f);
if (m00 == 0)
   m00 = eps;
end

m10 = sum(x.*f);
m01 = sum(y.*f);
m11 = sum(x.*y.*f);
m20 = sum(x.^2.*f);
m02 = sum(y.^2.*f);
m30 = sum(x.^3.*f);
m03 = sum(y.^3.*f);
m12 = sum(x.*y.^2.*f);
m21 = sum(x.^2.*y.*f);

% calculo de los momentos centrales normalizados
xmedia = m10/m00;
ymedia = m01/m00;

eta11 = (m11 - ymedia*m10)/m00^2;
eta20 = (m20 - xmedia*m10)/m00^2;
eta02 = (m02 - ymedia*m01)/m00^2;
eta30 = (m30 - 3*xmedia*m20 + 2*xmedia^2*m10)/m00^2.5;
eta03 = (m03 - 3*ymedia*m02 + 2*ymedia^2*m01)/m00^2.5;
eta21 = (m21 - 2*xmedia*m11 - ymedia*m20+2*xmedia^2*m01)/m00^2.5;
eta12 = (m12 - 2*ymedia*m11 - xmedia*m02+2*ymedia^2*m10)/m00^2.5;

%calculo de los momentos invariantes de Hu
phi(1) = eta20 + eta02;
phi(2) = (eta20 - eta02)^2 + 4*eta11^2;
phi(3) = (eta30 - 3*eta12)^2 + (3*eta21 - eta03)^2;
phi(4) = (eta30 + eta12)^2 + (eta21 + eta03)^2;
phi(5) = (eta30 - 3*eta12)*(eta30+eta12)*...
         ( (eta30 + eta12)^2 - 3*(eta21 + eta03)^2)+...
         (3*eta21 - eta03)*(eta21 + eta03)*...
         (3*(eta30 + eta12)^2 - (eta21 + eta03)^2); 
phi(6) = (eta20 - eta02)*((eta30+eta12)^2 -...
                              (eta21 + eta03)^2) +...
         4*eta11*(eta30 + eta12)*(eta21 + eta03); 
phi(7) = (3*eta21 - eta03)*(eta30 + eta12)*...
         ((eta30 + eta12)^2 - 3*(eta21 + eta03)^2) + ...
         (3*eta12 - eta30) * (eta21 + eta03) * ...
         (3*(eta30 + eta12)^2 - (eta21 + eta03)^2); 

phip = abs(log10(abs(phi)));

disp('phi: '); disp(phi);
disp('phi prima: '); disp(phip);
     