% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 5.14: Suavizado y Realzado: contracci�n histograma

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.11.4 Histogramas: modificaci�n
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Histograma para 10 niveles de gris
clear all;
B = [
     0     0     2     3     9
     2     5     5     2     5
     3     3     6     5     5
     3     6     8     6     5
     2     8     8     6     6];

L = 10; %numero de niveles de gris
%creamos un vector para contar los valores
H = zeros(1,L);

[m,n] = size(B);

fMAX = max(max(B));  fMIN = min(min(B));
CMAX = 6; CMIN = 3;

%Contracci�n
for i=1:1:m
    for j=1:1:n
      BCont(i,j) = round(((CMAX - CMIN)/(fMAX - fMIN))*(B(i,j)- fMIN)+ CMIN);  
    end
end


%obtenci�n del nuevo histograma
for i=1:1:m
    for j=1:1:n
      H(BCont(i,j)+1) = H(BCont(i,j)+1) + 1;  
    end
end

disp('Contracci�n de histograma :'); disp(H);

x = 0:1:L-1;

bar(x,H,'k','LineWidth',2)
