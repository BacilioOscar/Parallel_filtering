% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 5.7: Suavizado y Realzado: filtrado Gaussiano 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.11.2 Suavizado: filtros de medias
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  % Salida:
  %   ma9: media gaussiana para la vecindad que rodea al valor 9
  %   AMa: matriz resultante de aplicar el filtro gaussiano

  A = [1 1 1 2 7; 8 3 2 1 1; 1 3 9 2 1; 1 2 2 3 1; 1 1 1 2 1];
  
  %dimensiones del entorno de vecindad
  m = 3; n = 3;

  %extraemos la submatriz central (vecindad-8) del pixel dado en la posici�n
  %(x,y) = (3,3)
  x = 3; y = 3;
  h = 1;
  for i=x-1:1:x+1
    k = 1;  
    for j=y-1:1:y+1
        S(h,k) = A(i,j);
        k = k + 1;
    end
    h = h + 1;
  end

  %obtenci�n del nuecleo gaussiano
  Ventana = fspecial('gaussian', [m,n]);

  Ma9 = sum(sum(S.*Ventana));
  
  AMa = imfilter (A,Ventana);

  disp('Filtrado gaussianao para el valor central:'); disp(Ma9);
  disp('Imagen resultante del filtrado Gaussiano:'); disp(AMa);
