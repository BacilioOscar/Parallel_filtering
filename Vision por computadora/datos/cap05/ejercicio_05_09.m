% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 5.9: Suavizado y Realzado: media arm�nica y contra-arm�nica

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.11.2 Suavizado: filtros de medias
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function mediaCArmonica 

  % Salida:
  %   Ma9:  media arm�nica para la vecindad que rodea al valor 9
  %   Mca9: media contra-arm�nica para la vecindad que rodea al valor 9

  A = [1 1 1 2 7; 8 3 2 1 1; 1 3 9 2 1; 1 2 2 3 1; 1 1 1 2 1];

  %dimensiones del entorno de vecindad
  m = 3; n = 3;

  %extraemos la submatriz central (vecindad-8) del pixel dado en la posici�n
  %(x,y) = (3,3)
  x = 3; y = 3;
  h = 1;
  for i=x-1:1:x+1
    k = 1;  
    for j=y-1:1:y+1
        S(h,k) = A(i,j);
        k = k + 1;
    end
    h = h + 1;
  end

  % media arm�nica 
  Sa = 1./S;
  Ma9 = (m*n)/sum(sum(Sa));

  % media contrarm�nica con par�metro R = 2
  R = -2;
  SR1 = sum(sum(S.^(R+1)));
  SR  = sum(sum(S.^R));
  Mca9 = SR1/SR;
  
  disp('Filtrado de la media arm�nica para el valor central:'); disp(Ma9);
  disp('Filtrado de la media contra-arm�nica para el valor central:'); disp(Mca9);


