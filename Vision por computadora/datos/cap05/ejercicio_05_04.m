% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 5.4: Suavizado y Realzado: filtrado del punto medio

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.11.1 Suavizados: filtros de orden
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Salida:
%   pm9: punto medio para la vecindad que rodea al valor 9
%   Apm: matriz resultante de aplicar el punto medio

clear all;
A = [1 1 1 2 7; 8 3 2 1 1; 1 3 9 2 1; 1 2 2 3 1; 1 1 1 2 1];

%dimensiones del entorno de vecindad
m = 3; n = 3;

%extraemos la submatriz central (vecindad-8) del pixel dado en la posici�n
%(x,y) = (3,3)
x = 3; y = 3;
h = 1;
for i=x-1:1:x+1
    for j=y-1:1:y+1
        S(h) = A(i,j);
        h = h + 1;
    end
end

SO = sort(S);
m9 = min(SO);
M9 = max(SO);

% punto medio
pm9 = (m9 + M9)/2;
Amin = ordfilt2(A,1,ones(m,n),'symmetric'); 
AMax = ordfilt2(A,m*n,ones(m,n),'symmetric'); 
Apm = imlincomb(0.5, Amin, 0.5, AMax);

disp('Filtrado del punto medio para el valor central:'); disp(pm9);
disp('Imagen resultante del filtrado del punto medio:'); disp(Apm);

