% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 5.2: Suavizado y Realzado: filtro de la moda
% Salida:
%   mo: moda de la vecindad que rodea el valor central del 9

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.11.1 Suavizados: filtros de orden
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
A = [1 1 1 2 7; 8 3 2 1 1; 1 3 9 2 1; 1 2 2 3 1; 1 1 1 2 1];

%dimensiones del entorno de vecindad
m = 3; n = 3;

%extraemos la submatriz central (vecindad-8) del pixel dado en la posici�n
%(x,y) = (3,3)
x = 3; y = 3;
h = 1;
for i=x-1:1:x+1
    for j=y-1:1:y+1
        S(h) = A(i,j);
        h = h + 1;
    end
end

SO = sort(S);
mo = mode(SO);

disp('Valor de la moda del p�xel central: '); disp(mo);
