% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 5.24: Suavizado y Realzado: filtrado speckle Oddy

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.11.8 Filtrado: ruido "speckle"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Filtro Oddy

% M�todo para generar una imagen ruidosa a partir de una dada
A = [2 2 2 8 8; 2 2 2 8 8; 2 2 2 8 8; 4 4 4 6 6; 4 4 4 6 6];
imagen = A;
% generaci�n de una imagen ruidosa con ruido de speckle
%Imruidosa = imnoise(imagen,'speckle', 0.04) + imagen;


% Copia directa de la imagen del ejercicio
B =[3.0000    2.8018    2.9390    9.0000    8.7971
    3.0000    3.0000    3.0000    8.8009    9.0000
    3.0000    2.8753    2.8392    9.0000    9.0000
    4.9575    5.0000    4.9584    7.0000    6.9103
    4.9988    5.0000    5.0000    6.7463    7.0000];

Imruidosa = B;
[filas,columnas, s] = size(imagen);

alpha = 0.5; %par�metro del filtro

imagen_filtrada = Imruidosa;

for i=2:1:filas-1
    for j=2:1:columnas-1
      %valores del filtro de dimensi�n 3x3
      a = i-1; b = i + 1; c = j - 1; d = j + 1; 
      ventana = Imruidosa(a:b,c:d);
      ym = mean2(ventana); y = ventana(2,2);
      for h=1:1:3
          for k=1:1:3
              mv(h,k) = abs(ventana(h,k)-y);
          end
      end
      m = sum(mv(:))/8;
      W = zeros(3,3);
      for h=1:1:3
          for k=1:1:3
              if abs(ventana(h,k)-y) < m
                  W(h,k) = 1;
              end
          end
      end
      if (i==3) & (j==3)
         parar = 1;
      end
      if m <= alpha*ym
          imagen_filtrada (i,j) = ym;
      else
          imagen_filtrada (i,j) = sum(W(:)'*ventana(:))/sum(W(:));
      end
    end
end
disp('Filtrado de Speckle (Oddy): '); disp(imagen_filtrada);
