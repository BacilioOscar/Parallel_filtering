% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 5.15: Suavizado y Realzado: desplazamiento de histograma

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.11.4 Histogramas: modificaci�n
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Histograma para 10 niveles de gris

clear all;
B = [
     3     3     4     4     6
     4     5     5     4     5
     4     4     5     5     5
     4     5     6     5     5
     4     6     6     5     5];

L = 10; %numero de niveles de gris
%creamos un vector para contar los valores
H = zeros(1,L);

[m,n] = size(B);

DES = 2;

%Desplazamiento
for i=1:1:m
    for j=1:1:n
      BDes(i,j) = B(i,j) + DES;  
    end
end


%obtenci�n del nuevo histograma
for i=1:1:m
    for j=1:1:n
      H(BDes(i,j)+1) = H(BDes(i,j)+1) + 1;  
    end
end

disp('Desplazamiento de histograma:'); disp(H);

x = 0:1:L-1;

bar(x,H,'k','LineWidth',2)
