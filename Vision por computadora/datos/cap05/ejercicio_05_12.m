% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 5.12: Suavizado y Realzado: histograma

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.11.4 Histogramas: modificaci�n
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

B = [0 0 1 2 6; 1 3 3 1 3; 2 2 4 3 3; 2 4 5 4 3; 1 5 5 4 4];

L = 10; %numero de niveles de gris
%creamos un vector para contar los valores
H = zeros(1,L);

[m,n] = size(B);

%calculo del histograma
for i=1:1:m
    for j=1:1:n
      H(B(i,j)+1) = H(B(i,j)+1) + 1;  
    end
end

disp('Histograma:'); disp(H);

x = 0:1:L-1;
figure; bar(x,H,'k','LineWidth',2)

