% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 5.3: Suavizado y Realzado: filtrado de m�ximos y m�nimos

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.11.1 Suavizados: filtros de orden
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Salida:
%   m9: m�nimo para la vecindad que rodea al valor 9
%   M9: m�ximo para la vecindad que rodea al valor 9
%   Am: matriz resultante de m�nimos
%   AM: matriz resultante de m�ximos

clear all;
A = [1 1 1 2 7; 8 3 2 1 1; 1 3 9 2 1; 1 2 2 3 1; 1 1 1 2 1];

%dimensiones del entorno de vecindad
m = 3; n = 3;

%extraemos la submatriz central (vecindad-8) del pixel dado en la posici�n
%(x,y) = (3,3)
x = 3; y = 3;
h = 1;
for i=x-1:1:x+1
    for j=y-1:1:y+1
        S(h) = A(i,j);
        h = h + 1;
    end
end

SO = sort(S);
m9 = min(SO);
M9 = max(SO);
disp('Valor del m�nimo del p�xel central:'); disp(m9);
disp('Valor del m�ximo del p�xel central:'); disp(M9);

Am = ordfilt2(A,1,ones(m,n));
AM = ordfilt2(A,m*n,ones(m,n));

disp('Filtrado de m�nimos:'); disp(Am);
disp('Filtrado de m�ximos:'); disp(AM);


