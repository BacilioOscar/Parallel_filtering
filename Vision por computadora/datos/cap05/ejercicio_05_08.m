% EJERCICIOS RESUELTOS DE VISIÓN POR COMPUTADOR
% Autores: Gonzalo Pajares y Jesús Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 5.8: Suavizado y Realzado: Media geométrica

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.11.2 Suavizado: filtros de medias
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function mediaGeometrica

  % Salida:
  %   ma9: media geométrica para la vecindad que rodea al valor 9
  %   AMa: matriz resultante de aplicar el filtro media geométrica

  A = [1 1 1 2 7; 8 3 2 1 1; 1 3 9 2 1; 1 2 2 3 1; 1 1 1 2 1];

  %dimensiones del entorno de vecindad
  m = 3; n = 3;

  %extraemos la submatriz central (vecindad-8) del pixel dado en la posición
  %(x,y) = (3,3)
  x = 3; y = 3;
  h = 1;
  for i=x-1:1:x+1
    k = 1;  
    for j=y-1:1:y+1
        S(h,k) = A(i,j);
        k = k + 1;
    end
    h = h + 1;
  end

  Ma9 = prod(prod(S.^(1/(m*n))));
  
  disp('Filtrado de la media geométrica para el valor central:'); disp(Ma9);