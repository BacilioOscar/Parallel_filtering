% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 5.23: Suavizado y Realzado: filtro speckle Frost

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.11.8 Filtrado: ruido "speckle"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Filtro Frost

% M�todo para generar una imagen ruidosa a partir de una dada
A = [2 2 2 8 8; 2 2 2 8 8; 2 2 2 8 8; 4 4 4 6 6; 4 4 4 6 6];
imagen = A;
% generaci�n de una imagen ruidosa con ruido de speckle
%Imruidosa = imnoise(imagen,'speckle', 0.04) + imagen;


% Copia directa de la imagen del ejercicio
B =[3.0000    2.8018    2.9390    9.0000    8.7971
    3.0000    3.0000    3.0000    8.8009    9.0000
    3.0000    2.8753    2.8392    9.0000    9.0000
    4.9575    5.0000    4.9584    7.0000    6.9103
    4.9988    5.0000    5.0000    6.7463    7.0000];

Imruidosa = B;

[filas,columnas, s] = size(imagen);

imagen_filtrada = Imruidosa;

K = 2.0; %constante del filtro

% matriz de distancias de las localizaciones de la m�scara al p�xel central
r2 = sqrt(2);
t = [r2,1,r2; 1 0 1; r2 1 r2];

for i=2:1:filas-1
    for j=2:1:columnas-1
      %valores del filtro de dimensi�n 3x3
      a = i-1; b = i + 1; c = j - 1; d = j + 1; 
      ventana = Imruidosa(a:b,c:d);
      ym = mean2(ventana); sigy = std2(ventana);
      Cy = sigy/ym;
      for h=1:1:3
          for k=1:1:3
              m(h,k) = exp(-K*Cy*t(h,k));
          end
      end
      imagen_filtrada (i,j) = ventana(:)'*m(:)/sum(m(:));
    end
end
disp('Filtrado de Speckle (Frost): '); disp(imagen_filtrada);