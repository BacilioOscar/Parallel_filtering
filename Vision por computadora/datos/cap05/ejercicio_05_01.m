% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 5.1: Suavizado y Realzado: filtro de la mediana

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.11.1 Suavizados: filtros de orden
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Resultado:
%   med: mediana de la vecindad que rodea al valor central del 9
%   B  : matriz resultante de aplicar la meidana a A, teniendo en cuenta 
%        que en los bordes se completa con ceros 

clear all;
A = [1 1 1 2 7; 8 3 2 1 1; 1 3 9 2 1; 1 2 2 3 1; 1 1 1 2 1];

%dimensiones del entorno de vecindad
m = 3; n = 3;

%extraemos la submatriz central (vecindad-8) del pixel dado en la posici�n
%(x,y) = (3,3)
x = 3; y = 3;
h = 1;
for i=x-1:1:x+1
    for j=y-1:1:y+1
        S(h) = A(i,j);
        h = h + 1;
    end
end

SO = sort(S);
med = median(SO);
disp('Valor de la mediana del p�xel central: '); disp(med);

B = medfilt2(A,[m,n]);
disp('Filtro de la mediana: '); disp(B);




