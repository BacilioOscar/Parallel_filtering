% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 5.20: Suavizado y Realzado: transformaciones radiom�tricas ra�z
% cuadrada

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.11.7 Transformaciones radiom�tricas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Transformaci�n radiom�trica del tipo ra�z cuadrada para 10 niveles de gris
close all; clear all;
B = [3 3 5 6 3; 3 3 5 6 3; 4 3 5 6 4; 3 3 5 6 5; 3 4 5 5 5];

L = 10; %numero de niveles de gris

%Aplicamos la transformaci�n cuadrada (m = 2)
m = 1/2;
Bt = floor(L^(1-m).*B.^m);

%creamos un vector para contar los valores
H = zeros(1,L);
Ht = zeros(1,L);
[M,N] = size(B);

%calculo del histograma de la imagen original
for i=1:1:M
    for j=1:1:N
      H(B(i,j)+1) = H(B(i,j)+1) + 1;  
      Ht(Bt(i,j)+1) = Ht(Bt(i,j)+1) + 1;  
    end
end
disp('Imagen transformada ra�z cuadrada Bt: '); disp(Bt);

x = 0:1:L-1;
figure; bar(x,H,'k','LineWidth',2)
figure; bar(x,Ht,'k','LineWidth',2)

