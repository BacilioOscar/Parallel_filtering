% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 5.11: Suavizado y Realzado: filtrado adaptativo

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.11.3 Suavizado: filtro adaptativo
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Adaptativo 

  % Salida:
  %   Madap9:  filtrado adaptativo para la vecindad que rodea al valor 9

  A = [1 1 1 2 7; 8 3 2 1 1; 1 3 9 2 1; 1 2 2 3 1; 1 1 1 2 1];
  
  Aruidosa = [1.9835    1.9405    1.8741    2.7062    7.9065;
              9.0000    4.0000    3.0000    1.9797    2.0000;
              1.9291    3.8643    9.9666    3.0000    1.9458;
              1.9003    2.9793    2.9893    4.0000    1.9253;
              2.0000    2.0000    1.9493    2.9467    2.0000];

  %dimensiones del entorno de vecindad
  m = 3; n = 3;

  %extraemos la submatriz central (vecindad-8) del pixel dado en la posici�n
  %(x,y) = (3,3)
  x = 3; y = 3;
  h = 1;
  for i=x-1:1:x+1
    k = 1;  
    for j=y-1:1:y+1
        S(h,k) = Aruidosa(i,j);
        k = k + 1;
    end
    h = h + 1;
  end

  % filtrado adaptativo
  fxy = S(2,2);
  
  % varianza de la imagen
  vA = sqrt(std2(Aruidosa));
  
  % media de la vecindad local
  ml = mean2(S);

  % varianza de la vecindad local
  vl = sqrt(std2(S));
  
  Madap9 = fxy - (vA/vl)*(fxy - ml);
  
  disp('Filtrado adaptivo para el p�xel central:'); disp(Madap9);

