% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 5.5: Suavizado y Realzado: medio alpha recortado 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.11.1 Suavizados: filtros de orden
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  % Salida:
  %   ma9: medio alpha recortado para la vecindad que rodea al valor 9
  %   AMa: matriz resultante de aplicar el medio alpha recortado

  function medioalfa; 
  clear all;
  A = [1 1 1 2 7; 8 3 2 1 1; 1 3 9 2 1; 1 2 2 3 1; 1 1 1 2 1];

  %dimensiones del entorno de vecindad
  m = 3; n = 3;

  %extraemos la submatriz central (vecindad-8) del pixel dado en la posici�n
  %(x,y) = (3,3)
  x = 3; y = 3;
  h = 1;
  for i=x-1:1:x+1
    for j=y-1:1:y+1
        S(h) = A(i,j);
        h = h + 1;
    end
  end

  SO = sort(S);
  % alpha recortado eliminando los dos valores extremos
  SOe = SO(3:7);
  Ma9 = mean(SOe);

  % valor del recorte 
  T = 2;
  AMa = alphatrim (A,m,n,T);

  disp('Filtrado medio alpha recortado del valor central: '); disp(Ma9);
  disp('Filtrado medio alpha recortado: '); disp(AMa);
end

function s = alphatrim (t,m,n,T)
  % implementa el recorte alfa

  t = im2double(t);
  s = imfilter(t,ones(m,n),'symmetric');
  for k = 1:T
    s = imsubtract(s,ordfilt2(t,k,ones(m,n),'symmetric'));
  end

  for k = (m*n - (T) + 1):m*n
    s = imsubtract(s,ordfilt2(t,k,ones(m,n),'symmetric'));
  end

  s = s/(m*n - 2*T);
end

