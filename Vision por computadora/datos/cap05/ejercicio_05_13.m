% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 5.13: Suavizado y Realzado: expansi�n de histograma

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5.11.4 Histogramas: modificaci�n
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Histograma para 10 niveles de gris
clear all;
B = [0 0 1 2 6; 1 3 3 1 3; 2 2 4 3 3; 2 4 5 4 3; 1 5 5 4 4];

L = 10; %numero de niveles de gris
%creamos un vector para contar los valores
H = zeros(1,L);

[m,n] = size(B);

fMAX = max(max(B));  fMIN = min(min(B));
MAX = L-1; MIN = 0;

%Expansi�n
for i=1:1:m
    for j=1:1:n
      BExp(i,j) = round(((B(i,j)-fMIN)/(fMAX-fMIN))*(MAX-MIN)+MIN);  
    end
end


%obtenci�n del nuevo histograma
for i=1:1:m
    for j=1:1:n
      H(BExp(i,j)+1) = H(BExp(i,j)+1) + 1;  
    end
end

disp('Expansi�n de histograma:'); disp(H);

x = 0:1:L-1;

bar(x,H,'k','LineWidth',2)
