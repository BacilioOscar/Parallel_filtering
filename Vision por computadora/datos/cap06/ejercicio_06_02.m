% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 6.2: Fundamentos del color: modelo CMY

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 6.4.2 El modelo CMY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all; clear all;
%dimensiones de la imagen

ARoja  = [1 0 0; 1 0 1; 0.2    0  0.5];
AVerde = [0 1 0; 1 1 0; 0.2  0.2  0.5];
AAzul  = [0 0 1; 0 1 1;   0  0.2  0.5];

RGB(:,:,1) = ARoja; RGB(:,:,2) = AVerde; RGB(:,:,3) = AAzul; 
imagesc(RGB); title('Imagen en formato RGB');

%transformaci�n al fromato CMY
AC = 1-ARoja; AM = 1-AVerde; AY = 1-AAzul;

disp('Componentes C:'); disp(AC);
disp('Componentes M:'); disp(AM);
disp('Componentes Y:'); disp(AY);

figure;
CMY(:,:,1) = AC; CMY(:,:,2) = AM; CMY(:,:,3) = AY; 
imagesc(CMY); title('Imagen en formato CMY');



 

