% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 6.1: Fundamentos del color: modelo RGB

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 6.4.1 El modelo RGB
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all; clear all;
%dimensiones de la imagen
N = 32; 
M = 32;

%Seg�n el modelo RGB crear una imagen de color:
% 1) rojo
% 2) verde
% 3) azul
% 4) amarillo
% 5) cyan
% 6) magenta
% 7) gris
% 8) blanco

for i=1:1:N
    for j=1:1:M
      Rojo(i,j,1)    =   1;  Rojo(i,j,2)    =   0; Rojo(i,j,3)    =   0; 
      Verde(i,j,1)   =   0;  Verde(i,j,2)   =   1; Verde(i,j,3)   =   0; 
      Azul(i,j,1)    =   0;  Azul(i,j,2)    =   0; Azul(i,j,3)    =   1; 
      Amarillo(i,j,1)=   1;  Amarillo(i,j,2)=   1; Amarillo(i,j,3)=   0;    
      Magenta(i,j,1) =   1;  Magenta(i,j,2) =   0; Magenta(i,j,3) =   1;
      Cyan(i,j,1)    =   0;  Cyan(i,j,2)    =   1; Cyan(i,j,3)    =   1;
      Gris(i,j,1)    = 0.5;  Gris(i,j,2)    = 0.5; Gris(i,j,3)    = 0.5;
      Blanco(i,j,1)  =   1;  Blanco(i,j,2)  =   1; Blanco(i,j,3)  =   1;
   end
end

subplot(4,2,1); imshow(Rojo);     title('Rojo','LineWidth',3);
subplot(4,2,2); imshow(Verde);    title('Verde','LineWidth',3);
subplot(4,2,3); imshow(Azul);     title('Azul','LineWidth',3);
subplot(4,2,4); imshow(Amarillo); title('Amarillo','LineWidth',3);

subplot(4,2,5); imshow(Magenta);  title('Magenta','LineWidth',3);
subplot(4,2,6); imshow(Cyan);     title('Cyan','LineWidth',3);
subplot(4,2,7); imshow(Gris);     title('Gris','LineWidth',3);
subplot(4,2,8); imshow(Blanco);   title('Blanco','LineWidth',3);


