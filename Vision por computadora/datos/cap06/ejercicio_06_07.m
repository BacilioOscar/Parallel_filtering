% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 6.7: Fundamentos del color: pseudocolor

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 6.4.7 Pseudocolor: transformaci�n
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all; clear all;

A  = [0.9 0.0 0.6; 0.1 0.5 0.8; 0.7 0.2 0.3];

% obtener las dimensiones de la matriz de entrada
[m,n] = size(A);

alpha = 0.2;
for i=1:1:m
    for j=1:1:n
      % conversion
      R(i,j) = (cos(alpha*A(i,j)+pi/2))^2; 
      G(i,j) = (cos(alpha*A(i,j)))^2;
      B(i,j) = (sin(alpha*A(i,j)))^2;
    end
end

RGB = cat(3,R,G,B);
RGB = max(min(RGB,1),0);

disp('Componentes R:'); disp(R);
disp('Componentes G:'); disp(G);
disp('Componentes B:'); disp(B);
