% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 6.3: Fundamentos del color: modelo YIQ

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 6.4.3 El modelo YIQ
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all; clear all;
%dimensiones de la imagen

ARoja  = [1 0 0; 1 0 1; 0.2    0  0.5];
AVerde = [0 1 0; 1 1 0; 0.2  0.2  0.5];
AAzul  = [0 0 1; 0 1 1;   0  0.2  0.5];

% matriz de transformaci�n
T = [0.299 0.587 0.114; 0.596 -0.274 -0.322; 0.211 -0.523 0.312];

% obtener las dimensiones de la matriz de entrada
[m,n] = size(ARoja);

%transformaci�n de RGB al fromato YIQ
for i=1:1:m
    for j=1:1:n
      rgb = [ARoja(i,j), AVerde(i,j), AAzul(i,j)]';
      Y(i,j) = T(1,:)*rgb;  % matriz Y
      I(i,j) = T(2,:)*rgb;  % matriz I
      Q(i,j) = T(3,:)*rgb;  % matriz Q
    end
end

% imagen resultante en formato YIQ
YIQ(:,:,1) = Y; YIQ(:,:,2) = I; YIQ(:,:,3) = Q;

disp('Transformaci�n RGB -> YIQ')
disp('Componentes Y:'); disp(Y);
disp('Componentes I:'); disp(I);
disp('Componentes Q:'); disp(Q);

% matriz inversa de T
TI = inv(T);

% conversi�n del formato YIQ a RGB
for i=1:1:m
    for j=1:1:n
      yiq = [Y(i,j), I(i,j), Q(i,j)]';
      R(i,j) = TI(1,:)*yiq;  % matriz R
      G(i,j) = TI(2,:)*yiq;  % matriz G
      B(i,j) = TI(3,:)*yiq;  % matriz B
    end
end
 
RGB(:,:,1) = R; RGB(:,:,2) = G; RGB(:,:,3) = B;

disp('Transformaci�n YIQ -> RGB')
disp('Componentes R:'); disp(R);
disp('Componentes G:'); disp(G);
disp('Componentes B:'); disp(B);



