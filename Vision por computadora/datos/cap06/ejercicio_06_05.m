% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 6.5: Fundamentos del color: conversi�n HSI a RGB

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 6.4.5 Conversi�n de HSI a RGB
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all; clear all;
%dimensiones de la imagen

H  = [0.92 0.04 0.55; 0.17 0.50 0.83; 0.29 0.29 0.83];
S  = [0.33 0.45 0.83; 0.50 0.50 0.22; 0.62 0.45 0.14];
I  = [0.60 0.37 0.60; 0.80 0.80 0.90; 0.27 0.37 0.47];

% obtener las dimensiones de la matriz de entrada
[m,n] = size(H);

epsilon = 10^-8;

%transformaci�n de HSI al fromato RGB

% normalizacion de H
H = H*360; %multiplicaci�n por 360�
for i=1:1:m
    for j=1:1:n
      % b�squeda de sectores
      
      if 0 < H(i,j) & H(i,j) <= 120 % sector RG
         b = (1-S(i,j))/3;
         r = ((1+((S(i,j)*cosd(H(i,j)))/(cosd(60-H(i,j))))))/3;
         g = 1 -(r+b); 
      elseif 120 < H(i,j) & H(i,j) <= 240 % sector GB
         H(i,j) = H(i,j) - 120;
         r = (1-S(i,j))/3;
         g = ((1+((S(i,j)*cosd(H(i,j)))/(cosd(60-H(i,j))))))/3;
         b = 1 -(r+g); 
      elseif 240 < H(i,j) & H(i,j) <= 360 % sector BR
         H(i,j) = H(i,j) - 240;
         g = (1-S(i,j))/3;
         b = ((1+((S(i,j)*cosd(H(i,j)))/(cosd(60-H(i,j))))))/3;
         r = 1 -(g+b); 
      end
      R(i,j) = 3*r*I(i,j); 
      G(i,j) = 3*g*I(i,j); 
      B(i,j) = 3*b*I(i,j); 
      
    end
end

RGB = cat(3,R,G,B);
RGB = max(min(RGB,1),0);

disp('Componentes R:'); disp(R);
disp('Componentes G:'); disp(G);
disp('Componentes B:'); disp(B);

