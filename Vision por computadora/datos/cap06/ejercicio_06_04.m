% EJERCICIOS RESUELTOS DE VISI�N POR COMPUTADOR
% Autores: Gonzalo Pajares y Jes�s Manuel de la Cruz
% Copyright RA-MA, 2007
% Ejercicio 6.4: Fundamentos del color: conversi�n RGB a HSI

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 6.4.4 Conversi�n de RGB a HSI
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all; clear all;
%dimensiones de la imagen

Rc  = [0.8 0.6 0.1; 1.0 0.4 1.0; 0.2 0.3 0.5];
Gc  = [0.4 0.3 0.7; 1.0 1.0 0.7; 0.5 0.6 0.4];
Bc  = [0.6 0.2 1.0; 0.4 1.0 1.0; 0.1 0.2 0.5];

% obtener las dimensiones de la matriz de entrada
[m,n] = size(Rc);

epsilon = 10^-8;

%transformaci�n de RGB al fromato YIQ
for i=1:1:m
    for j=1:1:n
      R = Rc(i,j); G = Gc(i,j); B = Bc(i,j);      
      I(i,j) = (R+B+G)/3;  % matriz I
      
      % para normalizar H al rango [0,1]
      numerador   = 0.5*((R-G)+(R-B));
      denominador = ((R-G)^2+(R-B)*(G-B))^0.5 + epsilon;
      angulo = acos(numerador/denominador); 
      if B > G 
        angulo = 2*pi - angulo;
      end
      H(i,j) = angulo/(2*pi);  % matriz H
      
      S(i,j) = 1 - (3/(R+G+B+epsilon))*(min([R,G,B]));  % matriz S
    end
end

HSI = cat(3,H,S,I);

disp('Componentes I:'); disp(I);
disp('Componentes H:'); disp(H);
disp('Componentes S:'); disp(S);


