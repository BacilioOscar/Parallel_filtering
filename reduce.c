#include <omp.h>
#include <stdio.h>
int main() {
int t;
#pragma omp parallel reduction(+:t)
{
 t = omp_get_thread_num() + 1;
 printf("local %d\n", t);
}
printf("reduction %d\n", t); 
return 0;
}
