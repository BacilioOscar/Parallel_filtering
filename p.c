#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <omp.h>
#include <math.h>
#include <malloc.h>
#include <time.h>
#define n 3

typedef struct //Estructura para el color
{
  unsigned char b, g, r, a;
} Color;

int media(int arreglo[],int elementos);
float norma(int b,int g,int r);
float traspuesta(int r1,int r2,int g1,int g2,int b1,int b2);
bool peer(int b[],int g[],int r[],float d,int m,int t);
double filtro(char* original, char* nuevo,int di, int m);
long int width,height,p,bp;
int b3[9],g3[9],r3[9];
int b2[6],g2[6],r2[6];
int b1[4],g1[4],r1[4];
double start=0,finish=1;

int main(){
	int di = 905;
	float d = 2;
	int m = 2;
  int veces = 10;
  int a = 0,i;
  double sum = 0;

  //char nombre[30]="BaboonDi10.bmp";
  //char original[120] = "C:/Users/cars_/Desktop/bacilio/images/todo/lenna_r0i10.bmp";
  //char nuevo[120] = "C:/Users/cars_/Desktop/bacilio/images/todo/resultados r0 10/resultado.bmp";
	char* original="/home/msanchez/Documentos/investigacionParalela/todo/imagen0i20.bmp";
	char* nuevo=   "/home/msanchez/Documentos/investigacionParalela/todo/FilteringImages/resultados/imagen0i20.bmp";
	//strcat(original,nombre);
  //strcat(nuevo,nombre);

for (i = 2; i <= 32; i*=2) {
    omp_set_num_threads(i);
    sum=0;
    printf(" \n");
    for(a = 0; a < veces; a++){
      sum += filtro(original, nuevo, di, m);
    }
    printf("%f", (sum / veces)*10);
}
}
double filtro(char* original,char* nuevo,int di,int m){
	float d = di/1000.0f;
	int i;
  FILE *f = fopen(original, "rb"); //Imagen original
  FILE *fa;
 	fa = fopen (nuevo, "a+b" ); //Imagen generada, falta corregir el comando para que si el archivo existe lo actualice

  Color col;
  fseek(f, 10, SEEK_SET);    /* Posición 10: donde comienza el cuerpo (pixeles)*/
  fread(&p, 1,4, f);
  fseek(f, 18, SEEK_SET);    /* Posición 18: ancho */
  fread(&width, 1,4, f);
  fread(&height,1,4, f);
  fseek(f, 28, SEEK_SET);    /* Posición 18: alto*/
  fread(&bp, 1,4, f);
  int k=0,o=0;
//GENERANDO EL PUNTERO BIDIMENSIONAL PARA GUARDAR LOS PIXELES
  long int **pixeles;
	pixeles=(long int **)malloc(height*sizeof(long int *)); //numero de renglones tamaño de columas por enteros
	for (i = 0; i < height; i++){
    pixeles[i] = (long int *)malloc(width*4*sizeof(long int *));  //numero de columnas
	}

  long int **pixelesF;
	pixelesF=(long int **)malloc(height*sizeof(long int *)); //numero de renglones tamaño de columas por enteros
	for (i = 0;i < height; i++){
    pixelesF[i] = (long int *)malloc(width*4*sizeof(long int *));  //numero de columnas
  }//VARIABLE PARA GUARDAR LA CABECERA Y PASARLOS AL ARCHIVO NUEVO
  unsigned char cabe;
//leer cabecera
  fseek(f, 0, SEEK_SET);
	for(o = 0; o < 54; o++) {
    fread(&cabe,1,1,f);
    fputc(cabe, fa);
  }
  //PONER EL BUFFER DE LA IMAGEN EN LA POSICIÓN DONDE COMIENZAN LOS PIXELES
  fseek(f, 54, SEEK_SET);
	for(o = 0; o < height; o++){ //ciclo del tamaño del alto
    for (k = 0;k < width*4;k += 4){//Cargando la paleta de colores
      fread(&col,1,4,f); //leemos 4 bytes, ya que los pixeles esta en formato B G R A, solo nos interesa RGB
      //PASAR LOS PIXELES AL ARREGLO HACIENDO LA CONVERCIÓN
      pixeles[o][k] = (char)col.b;
	 		pixeles[o][k+1] = (char)col.g;
	 		pixeles[o][k+2] = (char)col.r;
			pixeles[o][k+3] = (char)col.a;
    }
  }
    start = omp_get_wtime();
		//int hebras = omp_get_num_procs();
	  //printf("total=  %d\n",hebras);
   #pragma omp parallel
   {
    #pragma omp for private (o,k) 
    for( o = 0; o<height; o++){ //ciclo del tamaño del alto
      for (k=0;k<width*4;k+=4){//Cargando la paleta de colores
        int b=pixeles[o][k],g=pixeles[o][k+1],r=pixeles[o][k+2],a=pixeles[o][k+3];
        if(o>0&&o<(height-1)&&k>0&&k<(width-1)*4){
           b3[0]=(unsigned char)pixeles [o-1][k-4];
           b3[1]=(unsigned char)pixeles [o]  [k-4];
           b3[2]=(unsigned char)pixeles [o+1][k-4];
           b3[3]=(unsigned char)pixeles [o-1][k];
           b3[4]=(unsigned char)pixeles [o]  [k];
           b3[5]=(unsigned char)pixeles [o+1][k];
           b3[6]=(unsigned char)pixeles [o-1][k+4];
           b3[7]=(unsigned char)pixeles [o]  [k+4];
           b3[8]=(unsigned char)pixeles [o+1][k+4];

           g3[0]=(unsigned char)pixeles [o-1][k+1-4];
           g3[1]=(unsigned char)pixeles [o]  [k+1-4];
           g3[2]=(unsigned char)pixeles [o+1][k+1-4];
           g3[3]=(unsigned char)pixeles [o-1][k+1];
           g3[3]=(unsigned char)pixeles [o-1][k+1];
           g3[4]=(unsigned char)pixeles [o][k+1];
           g3[5]=(unsigned char)pixeles [o+1][k+1];
           g3[6]=(unsigned char)pixeles [o-1][k+1+4];
           g3[7]=(unsigned char)pixeles [o]  [k+1+4];
           g3[8]=(unsigned char)pixeles [o+1][k+1+4];

           r3[0]=(unsigned char)pixeles [o-1][k+2-4];
           r3[1]=(unsigned char)pixeles [o]  [k+2-4];
           r3[2]=(unsigned char)pixeles [o+1][k+2-4];
           r3[3]=(unsigned char)pixeles [o-1][k+2];
           r3[4]=(unsigned char)pixeles [o]  [k+2];
           r3[5]=(unsigned char)pixeles [o+1][k+2];
           r3[6]=(unsigned char)pixeles [o-1][k+2+4];
           r3[7]=(unsigned char)pixeles [o]  [k+2+4];
           r3[8]=(unsigned char)pixeles [o+1][k+2+4];
           if(peer(b3,g3,r3,d,m,9)==false){
              b=mediana(b3,8);
              g=mediana(g3,8);
              r=mediana(r3,8);
              a=1;
           }
        }
        else if(o==0&&k==0){//inferior izquierda
           b1[0]=(unsigned char)pixeles [o+1][k];
           b1[1]=(unsigned char)pixeles [o+1][k+4];
           b1[2]=(unsigned char)pixeles [o]  [k];
           b1[3]=(unsigned char)pixeles [o]  [k+4];

           g1[0]=(unsigned char)pixeles [o+1][k+1];
           g1[1]=(unsigned char)pixeles [o+1][k+1+4];
           g1[2]=(unsigned char)pixeles [o]  [k+1];
           g1[3]=(unsigned char)pixeles [o]  [k+1+4];

           r1[0]=(unsigned char)pixeles [o+1][k+2];
           r1[1]=(unsigned char)pixeles [o+1][k+2+4];
           r1[2]=(unsigned char)pixeles [o]  [k+2];
           r1[3]=(unsigned char)pixeles [o]  [k+2+4];

           if(peer(b1,g1,r1,d,1,4)){
              b=mediana(b1,4);
              g=mediana(g1,4);
              r=mediana(r1,4);
              a=0;
           }
        }
        else if(o==0&&k==(width-1)*4){//inferior derecha
           b1[0]=(unsigned char)pixeles [o+1][k];
           b1[1]=(unsigned char)pixeles [o+1][k-4];
           b1[2]=(unsigned char)pixeles [o]  [k];
           b1[3]=(unsigned char)pixeles [o]  [k-4];

           g1[0]=(unsigned char)pixeles [o+1][k+1];
           g1[1]=(unsigned char)pixeles [o+1][k+1-4];
           g1[2]=(unsigned char)pixeles [o]  [k+1];
           g1[3]=(unsigned char)pixeles [o]  [k+1-4];

           r1[0]=(unsigned char)pixeles [o+1][k+2];
           r1[1]=(unsigned char)pixeles [o+1][k+2-4];
           r1[2]=(unsigned char)pixeles [o]  [k+2];
           r1[3]=(unsigned char)pixeles [o]  [k+2-4];

           if(peer(b1,g1,r1,d,1,4)){
              b=mediana(b1,4);
              g=mediana(g1,4);
              r=mediana(r1,4);
              a=0;
           }
        }
        else if(o==height-1&&k==0){//superior izquierda
           b1[0]=(unsigned char)pixeles [o-1][k];
           b1[1]=(unsigned char)pixeles [o-1][k+4];
           b1[2]=(unsigned char)pixeles [o]  [k];
           b1[3]=(unsigned char)pixeles [o]  [k+4];

           g1[0]=(unsigned char)pixeles [o-1][k+1];
           g1[1]=(unsigned char)pixeles [o-1][k+1+4];
           g1[2]=(unsigned char)pixeles [o]  [k+1];
           g1[3]=(unsigned char)pixeles [o]  [k+1+4];

           r1[0]=(unsigned char)pixeles [o-1][k+2];
           r1[1]=(unsigned char)pixeles [o-1][k+2+4];
           r1[2]=(unsigned char)pixeles [o]  [k+2];
           r1[3]=(unsigned char)pixeles [o]  [k+2+4];

           if(peer(b1,g1,r1,d,1,4)){
              b=mediana(b1,4);
              g=mediana(g1,4);
              r=mediana(r1,4);
              a=0;
           }
        }
        else if(o==height-1&&k==(width-1)*4){//superior derecha
           b1[0]=(unsigned char)pixeles [o-1][k];
           b1[1]=(unsigned char)pixeles [o-1][k-4];
           b1[2]=(unsigned char)pixeles [o]  [k];
           b1[3]=(unsigned char)pixeles [o]  [k-4];

           g1[0]=(unsigned char)pixeles [o-1][k+1];
           g1[1]=(unsigned char)pixeles [o-1][k+1-4];
           g1[2]=(unsigned char)pixeles [o]  [k+1];
           g1[3]=(unsigned char)pixeles [o]  [k+1-4];

           r1[0]=(unsigned char)pixeles [o-1][k+2];
           r1[1]=(unsigned char)pixeles [o-1][k+2-4];
           r1[2]=(unsigned char)pixeles [o]  [k+2];
           r1[3]=(unsigned char)pixeles [o]  [k+2-4];

           if(peer(b1,g1,r1,d,1,4)){
              b=mediana(b1,4);
              g=mediana(g1,4);
              r=mediana(r1,4);
              a=0;
           }
        }
        else if(o==0){//fila inferior
           b2[0]=(unsigned char)pixeles [o]  [k-4];
           b2[1]=(unsigned char)pixeles [o+1][k-4];
           b2[2]=(unsigned char)pixeles [o]  [k];
           b2[3]=(unsigned char)pixeles [o+1][k];
           b2[4]=(unsigned char)pixeles [o]  [k+4];
           b2[5]=(unsigned char)pixeles [o+1][k+4];

           g2[0]=(unsigned char)pixeles [o]  [k+1-4];
           g2[1]=(unsigned char)pixeles [o+1][k+1-4];
           g2[2]=(unsigned char)pixeles [o]  [k+1];
           g2[3]=(unsigned char)pixeles [o+1][k+1];
           g2[4]=(unsigned char)pixeles [o]  [k+1+4];
           g2[5]=(unsigned char)pixeles [o+1][k+1+4];

           r2[0]=(unsigned char)pixeles [o]  [k+2-4];
           r2[1]=(unsigned char)pixeles [o+1][k+2-4];
           r2[2]=(unsigned char)pixeles [o]  [k+2];
           r2[3]=(unsigned char)pixeles [o+1][k+2];
           r2[4]=(unsigned char)pixeles [o]  [k+2+4];
           r2[5]=(unsigned char)pixeles [o+1][k+2+4];

           if(peer(b2,g2,r2,d,2,6)==false){
              b=mediana(b2,6);
              g=mediana(g2,6);
              r=mediana(r2,6);
              a=0;
           }
        }
        else if(o==height-1){//fila superior
           b2[0]=(unsigned char)pixeles [o]  [k-4];
           b2[1]=(unsigned char)pixeles [o-1][k-4];
           b2[2]=(unsigned char)pixeles [o]  [k];
           b2[3]=(unsigned char)pixeles [o-1][k];
           b2[4]=(unsigned char)pixeles [o]  [k+4];
           b2[5]=(unsigned char)pixeles [o-1][k+4];

           g2[0]=(unsigned char)pixeles [o]  [k+1-4];
           g2[1]=(unsigned char)pixeles [o-1][k+1-4];
           g2[2]=(unsigned char)pixeles [o]  [k+1];
           g2[3]=(unsigned char)pixeles [o-1][k+1];
           g2[4]=(unsigned char)pixeles [o]  [k+1+4];
           g2[5]=(unsigned char)pixeles [o-1][k+1+4];

           r2[0]=(unsigned char)pixeles [o]  [k+2-4];
           r2[1]=(unsigned char)pixeles [o-1][k+2-4];
           r2[2]=(unsigned char)pixeles [o]  [k+2];
           r2[3]=(unsigned char)pixeles [o-1][k+2];
           r2[4]=(unsigned char)pixeles [o]  [k+2+4];
           r2[5]=(unsigned char)pixeles [o-1][k+2+4];

           if(peer(b2,g2,r2,d,2,6)==false){
              b=mediana(b2,6);
              g=mediana(g2,6);
              r=mediana(r2,6);
              a=0;
           }
        }
        else if(k==0){//columna izquierda
           b2[0]=(unsigned char)pixeles [o-1][k];
           b2[1]=(unsigned char)pixeles [o-1][k+4];
           b2[2]=(unsigned char)pixeles [o]  [k];
           b2[3]=(unsigned char)pixeles [o]  [k+4];
           b2[4]=(unsigned char)pixeles [o+1][k];
           b2[5]=(unsigned char)pixeles [o+1][k+4];

           g2[0]=(unsigned char)pixeles [o-1][k+1];
           g2[1]=(unsigned char)pixeles [o-1][k+1+4];
           g2[2]=(unsigned char)pixeles [o]  [k+1];
           g2[3]=(unsigned char)pixeles [o]  [k+1+4];
           g2[4]=(unsigned char)pixeles [o+1][k+1];
           g2[5]=(unsigned char)pixeles [o+1][k+1+4];

           r2[0]=(unsigned char)pixeles [o-1][k+2];
           r2[1]=(unsigned char)pixeles [o-1][k+2+4];
           r2[2]=(unsigned char)pixeles [o]  [k+2];
           r2[3]=(unsigned char)pixeles [o]  [k+2+4];
           r2[4]=(unsigned char)pixeles [o+1][k+2];
           r2[5]=(unsigned char)pixeles [o+1][k+2+4];

           if(peer(b2,g2,r2,d,2,6)==false){
              b=mediana(b2,6);
              g=mediana(g2,6);
              r=mediana(r2,6);
              a=0;
           }
        }
        else if(k==(width-1)*4){//columna derecha
           b2[0]=(unsigned char)pixeles [o-1][k];
           b2[1]=(unsigned char)pixeles [o-1][k-4];
           b2[2]=(unsigned char)pixeles [o]  [k];
           b2[3]=(unsigned char)pixeles [o]  [k-4];
           b2[4]=(unsigned char)pixeles [o+1][k];
           b2[5]=(unsigned char)pixeles [o+1][k-4];

           g2[0]=(unsigned char)pixeles [o-1][k+1];
           g2[1]=(unsigned char)pixeles [o-1][k+1-4];
           g2[2]=(unsigned char)pixeles [o]  [k+1];
           g2[3]=(unsigned char)pixeles [o]  [k+1-4];
           g2[4]=(unsigned char)pixeles [o+1][k+1];
           g2[5]=(unsigned char)pixeles [o+1][k+1-4];

           r2[0]=(unsigned char)pixeles [o-1][k+2];
           r2[1]=(unsigned char)pixeles [o-1][k+2-4];
           r2[2]=(unsigned char)pixeles [o]  [k+2];
           r2[3]=(unsigned char)pixeles [o]  [k+2-4];
           r2[4]=(unsigned char)pixeles [o+1][k+2];
           r2[5]=(unsigned char)pixeles [o+1][k+2-4];

           if(peer(b2,g2,r2,d,2,6)==false){
              b=mediana(b2,6);
              g=mediana(g2,6);
              r=mediana(r2,6);
              a=0;
           }
        }
          pixelesF[o][k]=b;
          pixelesF[o][k+1]=g;
          pixelesF[o][k+2]=r;
          pixelesF[o][k+3]=a;
      }
    }
}
    finish = omp_get_wtime();

      for(o = 0; o < height; o++){ //ciclo del tamaño del alto
        for(k = 0; k < width*4;k += 4){
          fputc(pixelesF[o][k], fa);
          fputc(pixelesF[o][k+1], fa);
          fputc(pixelesF[o][k+2], fa);
          fputc(pixelesF[o][k+3], fa);
        }
      }
      fclose (fa);
      fclose(f);
		  free(pixeles);
return(finish-start);
}
int media(int arreglo[],int elementos) {	//recibir los valores en el arreglo y el total de valores
	int suma_valores = 0;
	int p;
	for (p = 0; p < elementos; ++p) {
		suma_valores = suma_valores +  arreglo[p];
	}
	return suma_valores/elementos;
}
int mediana(int array[],int elementos){
	int i,j,k;
	int aux_elem;
	int movimientos =0;
	  for ( i = 0; i < elementos - 1; i++)
	 {
	     for ( j = 1; j < elementos; j++)
	     {
	         if (array[j] < array[j-1])
	         {   // si el elemento anterior es mayor, hacemos el cambio
	             aux_elem = array[j];
	             array[j] = array[j-1];
	             array[j-1] = aux_elem;
	             movimientos++;
	         }
	     }
	 }
	 if(elementos%2==0)
		return (array[elementos/2-1]+array[elementos/2])/2;
	return array[elementos/2-1];
}
bool peer(int b[],int g[],int r[],float d,int m,int t){
	int i,cardinalidad=0,c=t==9?4:2;
	float dis=0;
	for(i=0;i<t;i++){
		if(i!=c){
			dis=(b[c]-b[i])*(b[c]-b[i])+(g[c]-g[i])*(g[c]-g[i])+(r[c]-r[i])*(r[c]-r[i]);
			dis=sqrtf(dis);
			dis=1-dis/441.672955;
			if(dis>d){//entra si el pixel examinado es acceptado
				cardinalidad++;
			}
	    }
	}
	return cardinalidad>=m+1;
}

