#include <omp.h>
#include <stdio.h>
int main() {
 int t, j, i;
	 #pragma omp parallel private(t, i)
	 {
		 t = omp_get_thread_num();
		 if(t==0){
		 	printf("hilo 0\n");
		 }
		 else if(t==1){
		 	printf("hilo 1\n");
		 }
		 else if(t==2){
		 	printf("hilo 2\n");
		 }
	 }
}
